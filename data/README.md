﻿# Data

This directory contains raw and processed data files.

## raw

Raw data files.

```
|   # Air quality data from https://api.atmo-aura.fr/
├── atmo-aura
|   ├── atmo_aq_journaliere_2017-01-01_2021-12-31.csv  # daily air quality data
|   └── atmo_stations.geojson                          # station lat/lon
|
|   # ERA5 and ERA5-Land from https://cds.climate.copernicus.eu/
|   # To download: `src/workflow/get_copernicus_data.py`
├── copernicus
|   ├── era5_1h_2017.nc  # boundary layer height
|   ├── era5-land_1h_2017_2m_dewpoint_temperature.nc
|   ├── era5-land_1h_2017_2m_temperature.nc
|   ├── era5-land_1h_2017_10m_u_component_of_wind.nc
|   ├── era5-land_1h_2017_10m_v_component_of_wind.nc
|   ├── era5-land_1h_2017_surface_pressure.nc
|   └── ...  # files for 2018-2021
|
|   # PlanetScope satellite images from https://www.planet.com/
|   # To download: `src/workflow/get_planet_data.py`
├── planet
|   ├── RGB
|   |   └── PS2
|   |       ├── GRE-cb
|   |       |   ├── composite
|   |       |   |   └── <Order ID>
|   |       |   |       ├── 2017-03-17_strip_436701_composite.tif
|   |       |   |       └── ...  # other images and metadata files
|   |       |   └── scene
|   |       |       └── <Order ID>
|   |       |           └── PSScene
|   |       |               ├── 20170112_094417_0e19_3B_Visual_clip.tif
|   |       |               └── ...  # other images and metadata files
|   |       └── ... # other stations
|   └── TOAR
|       └── PS2
|           └── ... # same directory structure as RGB data
|
|   # Oxidative potential measures
└── pmall/
    ├── pmall_pm10-op_qamecs_2023-10-05.xlsx
    └── pmall_stations_atmo_sites.csv
```

## processed

Processed data created by `src/workflow/prepare_data.py`.

```
├── images
|   ├── RGB_all.pt         # all RGB images
|   ├── RGB_all.csv        # labels (air quality + meteorology) for all RGB images
|   ├── RGB_PS2_clear.pt   # clear (not cloudy) RGB images
|   ├── RGB_PS2_clear.csv  # labels (air quality + meteorology) for clear RGB images
|   ├── TOAR_all.pt        # all TOAR images
|   ├── TOAR_all.csv       # labels (air quality + meteorology) for all TOAR images
|   ├── TOAR_PS2_clear.pt  # clear (not cloudy) TOAR images
|   └── TOAR_PS2_clear.csv # labels (air quality + meteorology) for clear TOAR images
|
├── db.csv                 # air quality and meteorology
├── db_imputed.csv         # air quality and meteorology with missing PM10 imputed
├── met-embed_d3-n256.csv  # high-dimensional representation of meteorology
└── splits.csv             # training / validation / test splits
```

**RGB_PS2_clear.pt** is a pickled `torch.Tensor` of dtype `torch.float32` and shape `torch.Size([1465, 3, 334, 334])` (Images, Channels, Height, Width). The values range from 0 to 255.

**RGB_PS2_clear.csv** lists the metadata (Planet data type, instrument, station, date, etc.), air quality measures, and meteorological parameters corresponding to each image in `RGB_PS2_clear.pt`. Example:

```
file_idx data_type instrument station        date  ...   t2m    rh    sp   wd    ws
       0       RGB        PS2  GRE-cb  2017-01-19  ...  -9.7  0.73  93.5  114  1.00
       1       RGB        PS2  GRE-cb  2017-01-26  ...  -3.7  0.66  93.0  123  1.27
       2       RGB        PS2  GRE-cb  2017-02-20  ...   3.5  0.76  93.4   20  0.80
```

**TOAR_PS2_clear.pt** is a pickled `torch.Tensor` of dtype `torch.float32` and shape `torch.Size([1436, 4, 334, 334])` (Images, Channels, Height, Width). The values range from 0 to 1.

**TOAR_PS2_clear.csv** lists the metadata (Planet data type, instrument, station, date, etc.), air quality measures, and meteorological parameters corresponding to each image in `RGB_PS2_clear.pt`.

**db_imputed.csv** lists the air quality measures and meteorological variables, with missing PM~10~ observations imputed. Example:

```
      date station  pm10  pm10_imp  op_aa25_m3  ...  blh    rh    sp   wd   ws
2017-02-28  GRE-cb   4.0       4.0         NaN  ...  622  0.90  91.6  201  1.2
2017-02-28  GRE-fr   NaN       4.8        0.48  ...  605  0.90  91.5  201  1.2
2017-02-28     VIF   6.0       6.0         NaN  ...  582  0.91  89.9  196  1.1
```

**met-embed_d3-n256.csv** lists a high-dimensional representation of the meteorological parameters. Example:

```
      date station met0000 met0001 met0002 ... met1971 met1972 met1973 met1974 met1975
2017-01-01  GRE-cb       0       0       0 ...       0       0       0       0       0
2017-01-01  GRE-fr       0       0       0 ...       0       0       0       0       0
2017-01-01     VIF       0       1       0 ...       0       0       0       0       0
```

**splits.csv** lists the training / validation / test assignment of each station-day. Example:

```
      date station    set
2017-01-01  GRE-cb  train
2017-01-16  GRE-fr    val
2017-01-21     VIF   test
```
