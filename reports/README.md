# Reports

Exploratory analyses and experiment results.

## data_aq-meteo.ipynb

Exploratory analyses of air quality measures and meteorological variables

## data_planet.ipynb

Exploratory analyses of PlanetScope satellite images

## dev_cloud-filter.ipynb

Analyses developping a filter to exclude cloudy PlanetScope images

## dev_splits.ipynb

Analyses developping train / val / test splits

## results_contrastive.ipynb

Results of contrastive pretraining experiments. Contrastive pretraining uses SimSiam, an unsupervised contrastive learning framerwork, to learn features that distinguish between PlanetScope images. The hypothesis is that these features may be more indicative of air quality than generic "off-the-shelf" features learned from the ImageNet dataset.

## results_embedded.ipynb

Results of embedding experiments. These experiments replace the six meteorlogical parameters with a high-dimensional sparse binary embedding (representation). The hypothesis is that the model may make better use of the multimodal data (image features and meteorology) when the two have a similar number of features (n = 2048 for the image representations; n = 1976 for the embedding of meteorology).

## results_main.ipynb

Main results of supervised experiments. These experiments use supervised learning to estimate 24-hour PM~10~, OP~AA~, and OP~DTT~ based on representations of the PlanetScope images and meteoerology.
