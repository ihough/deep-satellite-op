# Estimating PM oxidative potential from satellite images using deep learning

This repository contains the code for the paper [Assessing the potential of PlanetScope satellite imagery to estimate particulate matter oxidative potential]() by Ian Hough, Loïc Argentier, Ziyang Jiang, Tongshu Zheng, Mike Bergin, David Carlson, Jean-Luc Jaffrezo, Jocelyn Chanussot, and Gaëlle Uzu.

The paper proposes a deep learning method that estimates the oxidative potential (OP) of ambient particulate matter (PM) based on 3 m/pixel PlanetScope satellite images and common meteorological parameters (temperature, humidity, etc.). The model consists of a ResNet50 convolutional neural network that represents the satellite images, and an MLP that estimates 24-hour OP based on the representations and meteorology. We train the model on 5 years of OP measures in Grenoble, France and evaluate its accuracy on a held-out test dataset.

The trained models are available at [https://doi.org/10.57745/ZOYJHD](https://doi.org/10.57745/ZOYJHD).

## Overview

The deep learning method is implemented using [pytorch-lightning](https://lightning.ai/docs/pytorch/stable/). The project uses [conda](https://docs.conda.io/en/latest/) for environment management. The code is organized in a [src layout](https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout/):


```
├── data/
|   ├── processed/     # Processed data
|   ├── raw/           # Raw data
|   └── README.md      # Data documentation
|
├── experiments/
|   ├── contrastive/   # Contrastive pretraining experiments
|   ├── embedding/     # Experiments with high-dimensional representation of meteorology
|   └── supervised/    # Supervised learning experiments
|
├── models/            # Trained models
|
├── reports/           # Reports
|
├── src/
|   ├── clis/          # CLIs to run experiments
|   ├── data_modules/  # Data processing code
|   ├── datasets/      # Data processing code
|   ├── dev/           # Code for developing cloud filter
|   ├── models/        # Model code
|   ├── tests/         # Unit tests
|   ├── transforms/    # Augmentations and image processing
|   ├── utils/         # Helper scripts
|   ├── workflow/      # Workflow scripts
|   └── constants.py   # Project-wide constants
|
├── .gitignore
├── environment.yml   # Conda environment definition
├── environment.lock  # Conda environment definition (exact)
├── LICENSE           # License
├── pyproject.toml    # Build system
└── README.md         # Documentation
```

## To run the code

#### 1. Set up the environment

```bash
conda create --name deep-satellite-op --file environment.yml
conda activate deep-satellite-op
```

To reproduce the original environment as closely as possible (exact package versions) use `environment.lock` rather than `environment.yml`.

#### 2. Download the data

* Air quality monitoring station measures: manually download from https://api.atmo-aura.fr/

* Meteorological parameters:

```bash
python src/workflow/get_copernicus_data.py
```

* PlanetScope images (requires a [Planet API key](https://developers.planet.com/quickstart/apis/)):

```bash
python src/workflow/get_planet_data.py
```

* Oxidative potential measures: contact Ian Hough or Gaëlle Uzu.

#### 3. Preprocess the data

```bash
python src/workflow/prepare_data.py
```

#### 4. Run the experiments

Note: you can comment out unwanted experiments in these files. You may run the experiments in any order, except the SimSiam experiments, which must be run after the contrastive pretraining experiments.

* Contrastive pretraining experiments (only needed for the SimSiam model):

```bash
python src/workflow/run_contrastive.py
```

* Main experiments (Baseline, Random, Transfer, Fine-tuning, SimSiam, SimSiam BL, and SimSiam DL models):

```bash
python src/workflow/run_supervised.py
```

* Experiments with high-dimensional representation (embedding) of meteorology (Transfer HD and Fine-tuning HD models):

```bash
python src/workflow/run_embedding.py
```
