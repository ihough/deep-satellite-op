"""Pre-train a CNN on satellite images using SimSiam contrastive learning"""

import logging
import os
from pathlib import Path

from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
from pytorch_lightning.cli import ArgsType

from clis import MyCLI
from constants import PROJ_ROOT, EXPERIMENTS_DIR, MODELS_DIR, SEED
from data_modules import ContrastiveDataModule
from models import SimSiam

logger = logging.getLogger(__name__)


class ContrastiveCLI(MyCLI):
    """CLI for contrastive pretraining"""

    EXPERIMENTS_DIR: Path = EXPERIMENTS_DIR / "contrastive"
    MODELS_DIR: Path = MODELS_DIR / "contrastive"

    def get_save_dir(self) -> Path:
        """Return a dirpath named with dataset and pretraining params

        e.g. experiments/contrastive/RGB_PS2_clear/simsiam_RGB
        """

        if self.config.get("resume") is not None:
            return Path(self.config.resume).parents[3]

        # Dataset
        # e.g. RGB_PS2_clear
        dataset = self.datamodule.image_dataset().data_path.stem

        # Pretraining params (method, channels, initial weights)
        # e.g. simsiam_RGB_IMAGENET1K-V2
        pretraining = "_".join(
            [
                self.model.__class__.__name__.lower(),  # method e.g. simsiam
                self.datamodule.image_dataset().channels,  # channels e.g. RGB
            ]
        )
        if self.model.weights is not None:
            pretraining += "_" + self.model.weights.replace("_", "-")

        # {EXPERIMENTS_DIR}/{dataset}/{backbone and pretraining params}
        # e.g. experiments/contrastive/RGB_PS2_clear/simsiam_RGB
        return Path(self.EXPERIMENTS_DIR, dataset, pretraining)

    def get_tb_logger_dirname(self) -> str:
        """Describe the training parameters

        Logs folder will be named with train params for filtering on tensorboard
        e.g. batch-32_lr-0.001_cos-50_out-2048_adam_ReLU
        """

        # Model and optimizer params
        # e.g. batch-32_lr-0.001_cos-50_out-2048_adam_ReLU_train-all
        description = f"batch-{self.datamodule.batch_size:02}"
        description += f"_lr-{self.model.learning_rate}"
        if self.model.cosine_decay:
            description += f"_cos-{self.model.cosine_epochs}"
        description += f"_out-{self.model.out_dim}"
        description += f"_{self.model.optimizer}"
        description += f"_{self.model.activation}"
        if self.datamodule.train_all:
            description += "_train-all"

        return description


def cli_main(args: ArgsType = None):
    """Run the CLI"""

    cli = ContrastiveCLI(
        model_class=SimSiam,
        datamodule_class=ContrastiveDataModule,
        save_config_kwargs=dict(overwrite=True),  # overwrite config if resuming
        trainer_defaults=dict(
            # Loggers are set in cli.instantiate_classes() so that save_dir can be chosen
            # based on experiment parameters
            callbacks=[
                # Checkpoint with best val_loss
                ModelCheckpoint(
                    filename="best_{epoch:02d}_{val_loss:.3f}",
                    monitor="val_loss",
                    save_weights_only=True,
                ),
                # Checkpoint of latest epoch
                ModelCheckpoint(
                    filename="last_{epoch:02d}-{val_loss:.3f}",
                    mode="max",
                    monitor="epoch",
                ),
                LearningRateMonitor(),  # Learning rate monitor
                # DeviceStatsMonitor(cpu_stats=True),
            ],
            max_epochs=250,
            log_every_n_steps=10,
            deterministic=True,
            # profiler="simple",
        ),
        seed_everything_default=SEED,
        args=args,
        run=False,
    )

    # Report what's going on
    if cli.config.get("resume") is not None:
        logger.info("Resuming %s", Path(cli.trainer.log_dir).relative_to(PROJ_ROOT))
    elif cli.config.get("config") is not None:
        logger.info("Running %s", Path(cli.get_config_path()).relative_to(PROJ_ROOT))
        logger.info("Logging to %s", Path(cli.trainer.log_dir).relative_to(PROJ_ROOT))

    # Fit, possibly resuming from a checkpoint
    cli.trainer.fit(
        model=cli.model, datamodule=cli.datamodule, ckpt_path=cli.config.get("resume")
    )

    # Don't save weights to MODELS_DIR if training was interrupted
    if cli.trainer.state.status == "interrupted":
        exit()

    # Symlink last backbone weights to MODELS_DIR
    # Use relative symlinks (../../experiments/contrastive/...) for git compatibility
    logging.info("Linking backbone weights into %s", MODELS_DIR)
    callback = [x for x in cli.trainer.checkpoint_callbacks if x.monitor == "epoch"][0]
    ckpt_path = callback.best_model_path
    out_path = cli.get_model_weights_path(ckpt_path)
    out_path.parent.mkdir(parents=True, exist_ok=True)
    out_path.symlink_to(os.path.relpath(ckpt_path, out_path.parent))
    logging.info("-> %s", out_path.relative_to(PROJ_ROOT))

    # ALTERNATIVE: save only last backbone state dict
    # torch.save(
    #     cli.model.__class__.load_from_checkpoint(ckpt_path).backbone.state_dict(),
    #     out_path,
    # )

    logging.info("Done")


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )
    cli_main()
