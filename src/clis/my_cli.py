from pathlib import Path
import warnings

import pandas as pd
from pytorch_lightning.cli import LightningArgumentParser, LightningCLI
from pytorch_lightning.loggers import CSVLogger, TensorBoardLogger


class MyCLI(LightningCLI):
    """Base CLI"""

    EXPERIMENTS_DIR: Path = None
    MODELS_DIR: Path = None

    def add_arguments_to_parser(self, parser: LightningArgumentParser) -> None:
        parser.add_argument(
            "-r",
            "--resume",
            type=str,
            help="Path to experiment dir or checkpoint to resume",
        )

    def before_instantiate_classes(self) -> None:
        # Resume an existing experiment
        if self.config.get("resume") is not None:
            if self.get_config_path() is not None:
                raise ValueError("Do not specify `--config` when using `--resume`")

            self.config.resume = self.get_resume_ckpt_path()

            # Load the experiment's original config and merge in any params set on the
            # command line (e.g. trainer.max_epochs)
            experiment_cfg = self.parser.parse_path(self.get_resume_config_path())
            default_cfg = self.parser.parse_args(args=[])
            for k, v in self.config.items():
                if v != default_cfg[k]:
                    experiment_cfg[k] = v  # param set on command line
            self.config = experiment_cfg

    def instantiate_classes(self) -> None:
        super().instantiate_classes()

        # Make loggers
        # - csv_logger -> {save_dir}/{experiment_name}/{version}
        # - tb_logger -> {save_dir}/{experiment_name}/{version}/{train params}
        #     subdir named with train params for easy filtering on tensorboard
        #     e.g. adam_batch-32_lr-0.001_cos-25_dim-512_extra-0_drop-0.2_batch-32_max-250
        csv_logger = CSVLogger(
            save_dir=self.get_save_dir(),
            name=self.get_experiment_name(),
            version=self.get_version(),
        )
        tb_logger = TensorBoardLogger(
            save_dir=csv_logger.save_dir,
            name=csv_logger.name,
            version=csv_logger.version,
            sub_dir=self.get_tb_logger_dirname(),  # subdir named for training params
            default_hp_metric=False,
        )

        # Set CSV logger as first logger to save config.yaml in {experiment_dir}
        self.trainer.loggers = [csv_logger, tb_logger]

        # Ignore warning from dataloaders about few workers; entire data file is loaded
        # into memory so no benefit from adding more workers
        warnings.filterwarnings(
            action="ignore",
            message=r"The '\w+dataloader' does not have many workers",
            append=True,
        )

        if self.config.get("resume") is not None:
            # If resuming, ignore non-empty dir warnings from csv_logger and checkpoints
            warnings.filterwarnings(
                action="ignore",
                message=r".+ directory .+ exists and is not empty",
                append=True,
            )

            # If resuming an experiment with a metrics.csv file, add the file's column
            # headers as CSV logger metric keys. Otherwise CSV logger raises a ValueError
            # if it logs before encountering all metrics in the file (e.g. if it logs from
            # train loop before running val loop and file contains val metrics).
            metrics_path = Path(csv_logger.experiment.metrics_file_path)
            if metrics_path.exists():
                logged_metrics = pd.read_csv(metrics_path, nrows=0).columns.to_list()
                csv_logger.experiment.metrics_keys = logged_metrics

    def get_config_path(self) -> str | None:
        """Return the config path"""

        if isinstance(self.config.config, list):
            return self.config.config[0]
        return self.config.config

    def get_resume_ckpt_path(self) -> str:
        """Return path to last checkpoint of the experiment to resume"""

        resume = Path(self.config.resume)
        if not resume.exists():
            raise FileNotFoundError(f"`--resume` path does not exist: {resume}")
        if resume.is_dir():
            resume = sorted(resume.glob("checkpoints/last_*.ckpt"))
            if not any(resume):
                raise ValueError("`--resume` dir contains no `last_*.ckpt` file")
            resume = resume[-1]  # assume last alphabetically is most recent
        return str(resume.resolve())

    def get_resume_config_path(self) -> str:
        """Return path to resume experiment's config"""

        config = Path(self.config.resume).parent.with_name("config.yaml")
        if not config.exists():
            raise FileNotFoundError("`--resume` dir contains no `config.yaml` file")

        return str(config)

    def get_save_dir(self) -> Path:
        """Implement to return the dir in which to save experiment artifacts"""

        raise NotImplementedError()

    def get_experiment_name(self) -> str:
        """Return experiment name e.g. exp01"""

        if self.config.get("resume") is not None:
            return Path(self.config.resume).parents[2].name
        return Path(self.get_config_path()).stem.split("_")[-1]

    def get_version(self) -> str:
        """Return experiment version (None or resume version)"""

        if self.config.get("resume") is not None:
            return Path(self.config.resume).parents[1].name
        return None

    def get_tb_logger_dirname(self) -> str:
        """Implement to return a name based on the training parameters"""

        raise NotImplementedError()

    def get_model_weights_path(self, ckpt_path: str) -> Path:
        """Return path for saving model weights

        e.g. models/supervised/supervised.pm10-imp.db.nocnn.exp01.version_0.best_*.pkl
        """

        ckpt_path = Path(ckpt_path).relative_to(self.EXPERIMENTS_DIR)
        filename = (
            ".".join(ckpt_path.parts[:-2]) + "." + ckpt_path.with_suffix(".pkl").name
        )
        return Path(self.MODELS_DIR, filename)
