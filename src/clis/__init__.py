"""Command-line interfaces"""

from .my_cli import MyCLI
from .contrastive_cli import cli_main as contrastive_cli
from .supervised_cli import cli_supervised as supervised_cli
from .embedding_cli import cli_embedding as embedding_cli  # load after supervised_cli
