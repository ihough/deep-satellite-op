"""Train a model to predict air quality from satellite images"""

import logging
import os
from pathlib import Path

from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.cli import ArgsType
import torch

from clis import MyCLI
from constants import PROJ_ROOT, EXPERIMENTS_DIR, MODELS_DIR, SEED
from data_modules import SupervisedDataModule
from models import AQLearner

logger = logging.getLogger(__name__)


class SupervisedCLI(MyCLI):
    """CLI for supervised learning of air quality"""

    EXPERIMENTS_DIR: Path = EXPERIMENTS_DIR / "supervised"
    MODELS_DIR: Path = MODELS_DIR / "supervised"

    def get_save_dir(self) -> Path:
        """Return a dirpath named with the target, image dataset, backbone

        e.g. experiments/supervised/pm10-imp/RGB_PS2_clear/simsiam_RGB_exp01_v0
        """

        if self.config.get("resume") is not None:
            return Path(self.config.resume).parents[3]

        # Target
        target = self.datamodule.target.replace("_", "-")

        # Dataset
        # e.g. RGB_PS2_clear
        dataset = "db"
        if self.datamodule.has_image_dataset():
            dataset = self.datamodule.image_dataset().data_path.stem

        # Backbone
        if self.model.backbone is None:
            backbone = "nocnn"
        else:
            if self.model.weights is None:
                # Random init
                # e.g. random_RGB
                backbone = "_".join(["random", self.datamodule.image_dataset().channels])
            elif self.model.weights.endswith(".pkl"):
                if "jiang2022" in self.model.weights:
                    # Weights from Jiang et al. 2022
                    # e.g. jiang2022_beijing
                    backbone = "jiang2022_" + self.model.weights.split("_")[-2].lower()
                else:
                    # Weights from saved model weights
                    # e.g. simsiam_RGB_exp01_v0
                    parts = self.model.weights.split(".")
                    backbone = "_".join(parts[1:4]).replace("version_", "v")
            elif self.model.weights.endswith(".ckpt"):
                # Weights from pretraining checkpoint
                # e.g. simsiam_RGB_exp01_v0
                parts = self.model.weights.split("/")
                backbone = "_".join(parts[3:6]).replace("version_", "v")
            else:
                # Torchvision weights
                # e.g. IMAGENET1K-V2_RGB
                backbone = "_".join(
                    [
                        self.model.weights.replace("_", "-"),
                        self.datamodule.image_dataset().channels,
                    ]
                )

        # {EXPERIMENTS_DIR}/{target}/{dataset}/{backbone}
        # e.g. experiments/supervised/pm10-imp/RGB_PS2_clear/simsiam_RGB_exp01_v0
        return Path(self.EXPERIMENTS_DIR, target, dataset, backbone)

    def get_tb_logger_dirname(self) -> str:
        """Describe the training parameters

        Logs folder will be named with train params for filtering on tensorboard
        e.g. batch-32_lr-0.001_dim-512_drop-0.2_adam_ReLU
        """

        # Model and optimizer params
        # e.g. batch-32_lr-0.001_bb-3_extra-10_dim-512_drop-0.2_adam_ReLU
        description = f"batch-{self.datamodule.batch_size:02}"
        description += f"_lr-{self.model.learning_rate}"
        if self.model.backbone_train_idx is not None:
            if self.model.backbone_train_idx == 0:
                description += "_bb-all"
            else:
                description += f"_bb-{abs(self.model.backbone_train_idx)}"
        if self.model.n_extra > 0:
            description += f"_extra-{self.model.n_extra}"
        if self.model.dim1 == self.model.dim2:
            description += f"_dim-{self.model.dim1}"
        else:
            description += f"_dim1-{self.model.dim1}_dim2-{self.model.dim2}"
        description += f"_drop-{self.model.dropout}"
        description += f"_{self.model.optimizer}"
        description += f"_{self.model.activation}"

        return description


def cli_main(cli: MyCLI):
    """Run the CLI"""

    # Report what's going on
    if cli.config.get("resume") is not None:
        logger.info("Resuming %s", Path(cli.trainer.log_dir).relative_to(PROJ_ROOT))
    elif cli.config.get("config") is not None:
        logger.info("Running %s", Path(cli.get_config_path()).relative_to(PROJ_ROOT))
        logger.info("Logging to %s", Path(cli.trainer.log_dir).relative_to(PROJ_ROOT))

    # Fit, possibly resuming from a checkpoint
    cli.trainer.fit(
        model=cli.model, datamodule=cli.datamodule, ckpt_path=cli.config.get("resume")
    )
    if cli.datamodule.has_image_dataset():
        for dataset in [cli.datamodule.train_dataset, cli.datamodule.val_dataset]:
            if dataset is not None:
                dataset.image_dataset.unload()

    # Don't continue if training was interrupted
    if cli.trainer.state.status == "interrupted":
        exit()

    # Test the best weights
    best_path = Path(cli.trainer.checkpoint_callback.best_model_path)
    logging.info("Testing with weights %s", best_path.stem)
    cli.model.load_state_dict(torch.load(best_path)["state_dict"])
    cli.trainer.test(cli.model, cli.datamodule)
    if cli.datamodule.image_dataset() is not None:
        cli.datamodule.test_dataset.image_dataset.unload()

    # Predict with best model
    logging.info("Predicting with weights %s", best_path.stem)
    preds = cli.trainer.predict(cli.model, cli.datamodule)
    if cli.datamodule.image_dataset() is not None:
        cli.datamodule.predict_dataset.image_dataset.unload()

    # Save predictions to csv
    preds = cli.datamodule.labels()[["date", "station", cli.datamodule.target]].assign(
        pred=torch.cat(preds), ckpt=best_path.name
    )
    preds.loc[cli.datamodule.train_idx, "set"] = "train"
    preds.loc[cli.datamodule.val_idx, "set"] = "val"
    preds.loc[cli.datamodule.test_idx, "set"] = "test"
    preds_path = Path(
        cli.trainer.log_dir, "predictions", "pred_" + best_path.with_suffix(".csv").name
    )
    preds_path.parent.mkdir(exist_ok=True)
    preds.to_csv(preds_path, index=False)
    logging.info("-> %s", preds_path.relative_to(PROJ_ROOT))
    del preds

    # # Symlink weights to MODELS_DIR
    # # Use relative symlinks (../../experiments/supervised/...) for git compatibility
    # logging.info("Linking weights into %s", MODELS_DIR)
    # for callback in cli.trainer.checkpoint_callbacks:
    #     ckpt_path = callback.best_model_path
    #     out_path = cli.get_model_weights_path(ckpt_path)
    #     out_path.parent.mkdir(parents=True, exist_ok=True)
    #     out_path.symlink_to(os.path.relpath(ckpt_path, out_path))
    #     logging.info("-> %s", out_path.relative_to(PROJ_ROOT))
    #
    #     # ALTERNATIVE: save only state dict
    #     # torch.save(
    #     #     cli.model.__class__.load_from_checkpoint(ckpt_path).state_dict(), out_path
    #     # )

    logging.info("Done")


def cli_supervised(args: ArgsType = None):
    """Init and run CLI for standard supervised learning"""

    cli = SupervisedCLI(
        model_class=AQLearner,
        datamodule_class=SupervisedDataModule,
        save_config_kwargs=dict(overwrite=True),  # overwrite config if resuming
        trainer_defaults=dict(
            # Loggers are set in cli.instantiate_classes() so that save_dir can be chosen
            # based on experiment parameters
            callbacks=[
                # Checkpoint with best val_loss
                ModelCheckpoint(
                    filename="best_{epoch:02d}_{val_loss:.3f}",
                    monitor="val_loss",
                    save_weights_only=True,
                ),
                # Checkpoint of latest epoch
                ModelCheckpoint(
                    filename="last_{epoch:02d}-{val_loss:.3f}",
                    mode="max",
                    monitor="epoch",
                ),
            ],
            max_epochs=250,
            log_every_n_steps=10,
            deterministic=True,
        ),
        seed_everything_default=SEED,
        args=args,
        run=False,
    )
    cli_main(cli)


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )
    cli_supervised()
