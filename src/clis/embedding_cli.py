"""Train a model on satellite images and high-dimensional meteorology"""

import logging
from pathlib import Path

from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.cli import ArgsType

from clis.supervised_cli import SupervisedCLI, cli_main
from constants import EXPERIMENTS_DIR, MODELS_DIR, SEED
from data_modules import EmbeddingDataModule
from models import AQLearner

logger = logging.getLogger(__name__)


class EmbeddingCLI(SupervisedCLI):
    """CLI for supervisd experiments using high-dimensional embedding of met"""

    EXPERIMENTS_DIR: Path = EXPERIMENTS_DIR / "embedding"
    MODELS_DIR: Path = MODELS_DIR / "embedding"


def cli_embedding(args: ArgsType = None):
    """Init and run CLI for supervised learning with embedded meteorology"""

    cli = EmbeddingCLI(
        model_class=AQLearner,
        datamodule_class=EmbeddingDataModule,
        save_config_kwargs=dict(overwrite=True),  # overwrite config if resuming
        trainer_defaults=dict(
            # Loggers are set in cli.instantiate_classes() so that save_dir can be chosen
            # based on experiment parameters
            callbacks=[
                # Checkpoint with best val_loss
                ModelCheckpoint(
                    filename="best_{epoch:02d}_{val_loss:.3f}",
                    monitor="val_loss",
                    save_weights_only=True,
                ),
                # Checkpoint of latest epoch
                ModelCheckpoint(
                    filename="last_{epoch:02d}-{val_loss:.3f}",
                    mode="max",
                    monitor="epoch",
                ),
            ],
            max_epochs=250,
            log_every_n_steps=10,
            deterministic=True,
        ),
        seed_everything_default=SEED,
        args=args,
        run=False,
    )
    cli_main(cli)


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )
    cli_embedding()
