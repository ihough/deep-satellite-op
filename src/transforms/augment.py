"""Augmentation for SimSiam"""

import torch
from torchvision.transforms import Compose, RandomResizedCrop, RandomHorizontalFlip


class Augment:
    """Return an augmented view of an image

    Transforms applied:
        RandomResizedCrop
        RandomHorizontalFlip
    """

    def __init__(
        self, crop_size: int, crop_scale: tuple[float, float] = (0.2, 1.0)
    ) -> None:
        """
        Args:
            crop_size: Size for RandomResizedCrop (default: 96)
            crop_scale: Scale range for RandomResizedCrop (default: (0.2, 1.0))
        """

        super().__init__()

        self.transform = Compose(
            [
                RandomResizedCrop(size=crop_size, scale=crop_scale, antialias=True),
                RandomHorizontalFlip(),
            ]
        )

    def __repr__(self) -> str:
        desc = str(self.transform).replace("Compose", self.__class__.__name__)
        return desc

    def __call__(self, sample: torch.Tensor) -> tuple[torch.Tensor, torch.Tensor]:
        return self.transform(sample)


def _sanity_check():
    print("Augment")
    print("-------")
    torch.manual_seed(0)
    transform = Augment(96)
    print(transform)
    test_images = torch.randn(8, 4, 300, 300)
    view1 = transform(test_images)
    view2 = transform(test_images)
    assert not torch.equal(view1, view2)
    print("test images:", test_images.shape)
    print("view1: ", view1.shape)
    print("view2: ", view2.shape)


if __name__ == "__main__":
    _sanity_check()
