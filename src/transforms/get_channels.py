"""Get channels of Planet data by index or string e.g. 'RGB'"""

from utils.planet import PlanetChannelsType, PlanetDataType, planet_get_channels


class GetChannels:
    """Get specific channels of Planet data

    Examples:
        Get Red, Green, Blue channels from TOAR data:
            GetChannels("RGB", data_type="TOAR")
    """

    def __init__(self, channels: PlanetChannelsType, data_type: str) -> None:
        """
        Args:
            channels: Output channel order e.g. "RGB" or [2, 1, 0] or 2
            data_type: The Planet data type e.g. "TOAR"
        """

        self.channels = channels
        self.data_type = data_type

    def __call__(self, data: PlanetDataType) -> PlanetDataType:
        """Returns Planet data with specified channel order"""

        return planet_get_channels(data, channels=self.channels, data_type=self.data_type)

    def __repr__(self) -> str:
        desc = self.__class__.__name__
        desc += f"({self.channels}, data_type={self.data_type})"
        return desc


def _sanity_check():
    import torch

    print("GetChannels")
    print("-----------")
    rgb = torch.rand(2, 3, 2, 2)
    red, green, blue = [rgb[:, i] for i in range(3)]
    print("data:", rgb.shape)
    get_rgb = GetChannels(channels="RGB", data_type="RGB")
    print("get_rgb:", get_rgb)
    batch = get_rgb(rgb)
    print("get_rgb(data):", batch.shape)
    print(batch[0])
    assert torch.equal(batch, torch.stack([red, green, blue], dim=1))

    # Works for all combinations of channels and data type
    assert torch.equal(
        GetChannels("BGR", data_type="RGB")(rgb),
        torch.stack([blue, green, red], dim=1),
    )
    infrared = torch.rand(2, 2, 2)
    toar = torch.stack([blue, green, red, infrared], dim=1)
    assert torch.equal(GetChannels("RGB", data_type="TOAR")(toar), rgb)
    assert torch.equal(GetChannels("RGB", data_type="ATM")(toar), rgb)


if __name__ == "__main__":
    _sanity_check()
