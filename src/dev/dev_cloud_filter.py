"""Develop a filter to exclude cloudy Planet images"""

import re

from matplotlib import pyplot as plt

from constants import PROCESSED_DIR
from datasets import PlanetDataset
from utils import planet_plot


def count_images(subset: PlanetDataset) -> None:
    # Print the number of images remaining after applying the filter
    print("Remaining images:", len(subset))
    print(subset.labels.groupby(["instrument", "item_type"])["station"].count())

    # Print the number of remaining images with OP data
    print("\n")
    with_op = subset.labels[
        ["instrument", "item_type", "station", "op_aa25_m3", "op_dtt25_m3"]
    ].dropna()
    print("Remaining with OP:", len(with_op.index))
    print(with_op.groupby(["instrument", "item_type"])["station"].count())


def show_images(
    subset: PlanetDataset,
    data_type: str,
    channels: str = "RGB",
    sort_by: str = "green_q50",
    ascending: bool = False,
    batch_size: int = 40,
    nrow: int = 5,
    ncol: int = 8,
    size: int = 3,
    normalize: bool = False,
    value_range: tuple[float, float] = (0, 0.3),
) -> None:
    labs = subset.labels.sort_values(sort_by, ascending=ascending)
    i = 0
    while i < len(subset):
        idx = labs.index[i : i + batch_size]
        i += batch_size
        p = planet_plot(
            images=subset[idx],
            data_type=data_type,
            channels=channels,
            labels=(
                labs.loc[idx]
                .apply(
                    lambda row: (
                        f"{row.instrument} {row.item_type} {row.station}"
                        f"\n{row.date.date().isoformat()}"
                        # f"\ncld {round(row.cloud_cover, 2)}"
                        # f"\n{round(row.green_q05, 3)} {round(row.green_q50, 3)} {round(row.green_q95, 3)}"
                        f"\n{row.green_q50} {row.green_q95} {round(row.green_sd, 1)}"
                        f"\npm {round(row.pm10, 1)} op {round(row.op_aa25_m3, 2)}"
                        # f"\n{round(row.op_aa25_m3, 2)}"
                    ),
                    axis="columns",
                )
                .to_list()
            ),
            nrow=nrow,
            ncol=ncol,
            size=size,
            normalize=normalize,
            value_range=value_range,
        )
        plt.show()


toar = PlanetDataset(PROCESSED_DIR / "planet_TOAR.pt")
toar_labels = toar.labels


#
# Check different cloud filters: apply filter and count + view remaining "clear" images
#


# TOAR cover == 1 and cloud_cover == 0
# 2035 images of which 1987 clear
query = (
    "cover == 1 and cloud_cover == 0"
    " and green_q05 < 0.25"
    " and not date in ['2018-08-18', '2021-01-07', '2021-06-25']"
    " and not (date in ['2019-12-06', '2020-01-08'] and station != 'VIF')"
    " and not (date == '2020-05-10' and instrument == 'PS2')"
    " and not (date == '2021-08-12' and instrument == 'PS2' and station == 'GRE-cb')"
)
clear = toar.subset(query)
count_images(clear)
show_images(clear, data_type="TOAR", normalize=True, sort_by="green_q05")
# Remaining images: 1987
# instrument  item_type
# PS2         PSScene      1340
# PS2.SD      PSScene       310
# PSB.SD      PSScene       337
#
# Remaining with OP: 507
# instrument  item_type
# PS2         PSScene      339
# PS2.SD      PSScene       83
# PSB.SD      PSScene       85


# TOAR cover == 1 and cloud_cover > 0 and cloud_cover < 1
# 604 images of which 79 clear
query = (
    "cover == 1 and cloud_cover > 0 and cloud_cover < 1"
    " and green_q05 < 0.25 and green_q50 < 0.26 and green_q95 < 0.5"
    " and date in ["
    "'2017-01-19','2017-01-26','2017-03-15','2017-03-17','2017-04-05','2017-07-08',"
    "'2017-07-28','2017-08-09','2017-10-22','2018-02-13','2018-03-08','2018-03-16',"
    "'2018-03-19','2018-03-23','2018-03-24','2018-03-26','2018-04-25','2018-04-26',"
    "'2018-04-27','2018-04-28','2018-05-05','2018-05-07','2018-05-11','2018-05-12',"
    "'2018-05-21','2018-06-01','2018-06-02','2018-06-08','2018-06-09','2018-06-24',"
    "'2018-08-06','2018-08-19','2018-09-22','2018-09-30','2018-10-14','2019-03-16',"
    "'2019-04-01','2019-04-20','2019-04-22','2019-04-23','2019-04-29','2019-07-21',"
    "'2019-08-05','2019-08-19','2019-09-11','2019-09-27','2019-10-14','2019-10-24',"
    "'2020-01-13','2020-02-25','2020-06-15','2020-06-21','2020-07-02','2020-09-02',"
    "'2020-10-03','2021-03-25','2021-06-10','2021-07-07','2021-07-24','2021-08-28',"
    "'2021-09-22','2021-09-29','2021-10-26'"
    "]"
)
clear = toar.subset(query)
count_images(clear)
show_images(clear, data_type="TOAR", normalize=True, sort_by="date", ascending=True)
# Remaining images: 79
# instrument  item_type
# PS2         PSScene      70
# PS2.SD      PSScene       6
# PSB.SD      PSScene       3
#
# Remaining with OP: 24
# instrument  item_type
# PS2         PSScene      24


# TOAR cover == 1 and cloud_cover == 1
# 557 images of which basically all cloudy
toar.subset("cover == 1 and cloud_cover == 1")


# TOAR cover < 1
# 50 images of which 30 clear
query = "cover < 1 and cover > 0.7 and green_q95 < 0.21"
clear = toar.subset(query)
count_images(clear)
show_images(clear, data_type="TOAR", normalize=True, sort_by="green_q95")
# Remaining images: 30
# instrument  item_type
# PS2         PSScene      26
# PS2.SD      PSScene       2
# PSB.SD      PSScene       2
#
# Remaining with OP: 0


# RGB where TOAR not available
# 50 images of which 29 clear
rgb = PlanetDataset(PROCESSED_DIR / "planet_RGB.pt")
rgb_clear = rgb.subset(
    rgb.labels.loc[~rgb.labels["scene_id"].isin(toar.labels["scene_id"])].query(
        "green_q50 < 204 and green_q95 < 235 and green_sd > 15"
    ).index
)
count_images(rgb_clear)
show_images(rgb_clear, data_type="RGB", sort_by="green_q95")
# Remaining images: 29
# instrument  item_type
# PS2         PSScene      29
#
# Remaining with OP: 4
# instrument  item_type
# PS2         PSScene      4


# Combined filter
#
query = """
(
    (
        cover == 1 and cloud_cover == 0
        and green_q05 < 0.25
        and not date in ['2018-08-18', '2021-01-07', '2021-06-25']
        and not (date in ['2019-12-06', '2020-01-08'] and station != 'VIF')
        and not (date == '2020-05-10' and instrument == 'PS2')
        and not (date == '2021-08-12' and instrument == 'PS2' and station == 'GRE-cb')
    )
    or (
        cover == 1 and cloud_cover > 0 and cloud_cover < 1
        and green_q05 < 0.25 and green_q50 < 0.26 and green_q95 < 0.5
        and date in [
            '2017-01-19', '2017-01-26', '2017-03-15', '2017-03-17', '2017-04-05',
            '2017-07-08', '2017-07-28', '2017-08-09', '2017-10-22', '2018-02-13',
            '2018-03-08', '2018-03-16', '2018-03-19', '2018-03-23', '2018-03-24',
            '2018-03-26', '2018-04-25', '2018-04-26', '2018-04-27', '2018-04-28',
            '2018-05-05', '2018-05-07', '2018-05-11', '2018-05-12', '2018-05-21',
            '2018-06-01', '2018-06-02', '2018-06-08', '2018-06-09', '2018-06-24',
            '2018-08-06', '2018-08-19', '2018-09-22', '2018-09-30', '2018-10-14',
            '2019-03-16', '2019-04-01', '2019-04-20', '2019-04-22', '2019-04-23',
            '2019-04-29', '2019-07-21', '2019-08-05', '2019-08-19', '2019-09-11',
            '2019-09-27', '2019-10-14', '2019-10-24', '2020-01-13', '2020-02-25',
            '2020-06-15', '2020-06-21', '2020-07-02', '2020-09-02', '2020-10-03',
            '2021-03-25', '2021-06-10', '2021-07-07', '2021-07-24', '2021-08-28',
            '2021-09-22', '2021-09-29', '2021-10-26'
        ]
    )
    or (
        cover > 0.7 and cover < 1 and green_q95 < 0.21
    )
)
""".strip()
query = re.sub("\n *", " ", query)
clear = toar.subset(query)
count_images(clear)
# Remaining images: 2096
# instrument  item_type
# PS2         PSScene      1436
# PS2.SD      PSScene       318
# PSB.SD      PSScene       342
#
# Remaining with OP: 531
# instrument  item_type
# PS2         PSScene      363
# PS2.SD      PSScene       83
# PSB.SD      PSScene       85
