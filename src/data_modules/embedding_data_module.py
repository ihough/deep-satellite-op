import logging
from pathlib import Path
from typing import Callable

import pandas as pd

from constants import SEED, PROCESSED_DIR
from data_modules import SupervisedDataModule


# Logging
logger = logging.getLogger(__name__)


class EmbeddingDataModule(SupervisedDataModule):
    """DataModule for supervised learning with high-dimensional meteorology"""

    def __init__(
        self,
        target: str = "pm10",
        data_type: str | None = "RGB",
        instrument: str | None = "PS2",
        *,
        embedding: str = "met-embed_d3-n256",
        channels: str | None = None,
        transform: Callable | None = None,
        image_normalize: bool | Callable = True,
        exclude_outliers: bool = True,
        val_frac: float = 0.2,
        test_frac: float = 0.2,
        splits: str | Path | None = None,
        groupby: str | None = "date",
        shuffle: bool = True,
        batch_size: int = 4,
        drop_last: bool = False,
        rand_seed: int | None = SEED,
    ):

        super().__init__(
            target,
            data_type,
            instrument,
            channels=channels,
            transform=transform,
            image_normalize=image_normalize,
            extra_features=None,
            extra_normalize=False,
            exclude_outliers=exclude_outliers,
            val_frac=val_frac,
            test_frac=test_frac,
            splits=splits,
            groupby=groupby,
            shuffle=shuffle,
            batch_size=batch_size,
            drop_last=drop_last,
            rand_seed=rand_seed,
        )

        # High-dimensional representation of met variables
        self.embedding_path = (PROCESSED_DIR / embedding).with_suffix(".csv")
        self._met_embedding: pd.DataFrame | None = None
        self.extra_features = (
            f"{self.met_embedding().shape[1]}-dimensional representation of meteorology"
        )

    def extra_values(self, idx: list[int] | None = None) -> pd.DataFrame | None:
        return self.met_embedding(idx=idx)

    def met_embedding(self, idx: list[int] | None = None) -> pd.DataFrame:
        """High-dimensional representation of meteorological variables"""

        # Cache on first access
        if self._met_embedding is None:
            try:
                self._met_embedding = (
                    pd.read_csv(
                        self.embedding_path,
                        index_col=["date", "station"],
                        parse_dates=["date"],
                    )
                    .reindex(self.labels()[["date", "station"]])
                    .reset_index(drop=True)
                )
            except FileNotFoundError as err:
                raise FileNotFoundError(
                    f"No such embedding: {self.embedding_path}"
                ) from err

        if idx is not None:
            return self._met_embedding.iloc[idx]

        return self._met_embedding


def _sanity_check():
    """Sanity checks"""

    # logging.basicConfig(level=logging.DEBUG)

    print("EmbeddingDataModule")
    print("--------------------")
    module = EmbeddingDataModule(
        target="pm10",
        data_type="RGB",
        instrument="PS2.SD",
        embedding="met-embed_d3-n256",
        shuffle=False,
    )
    # subset for testing
    module._image_dataset = module.image_dataset().subset(range(10))
    module._labels = None  # will be reset on first access
    module._met_embedding = None  # will be reset on first access
    print(module)

    assert module.met_embedding().shape == (10, 1976)
    assert module.extra_values().shape == (10, 1976)

    module.setup("fit")
    assert module.train_dataset.extra.shape == (5, 1976)
    assert module.val_dataset.extra.shape == (1, 1976)
    module.setup("test")
    assert module.test_dataset.extra.shape == (1, 1976)
    module.setup("predict")
    assert module.predict_dataset.extra.shape == (10, 1976)


if __name__ == "__main__":
    _sanity_check()
