"""DataModule for supervised learning of air quality"""

import logging
from pathlib import Path
from typing import Callable, Optional

import pandas as pd
from pytorch_lightning import LightningDataModule
from torchvision.transforms import Normalize
from torch.utils.data import DataLoader

from constants import SEED, OP_STATIONS
from datasets import PlanetClear, SupervisedDataset
from utils import split_train_val_test
from workflow.prepare_data import load_db_imputed

# Thresholds to exclude outlying values for target air quality metrics
TARGET_LIMITS = {"pm10": 50, "pm10_imp": 50, "op_aa25_m3": 6, "op_dtt25_m3": 5}


# Logging
logger = logging.getLogger(__name__)


class SupervisedDataModule(LightningDataModule):
    """DataModule for supervised learning of air quality"""

    def __init__(
        self,
        target: str = "pm10",
        data_type: Optional[str] = "RGB",
        instrument: Optional[str] = "PS2",
        *,
        channels: Optional[str] = None,
        transform: Optional[Callable] = None,
        image_normalize: bool | Callable = True,
        extra_features: Optional[list[str]] = None,
        extra_normalize: bool | tuple[pd.Series, pd.Series] = True,
        exclude_outliers: bool = True,
        val_frac: float = 0.2,
        test_frac: float = 0.2,
        splits: Optional[str | Path] = None,
        groupby: Optional[str] = "date",
        shuffle: bool = True,
        batch_size: int = 4,
        drop_last: bool = False,
        rand_seed: Optional[int] = SEED,
    ):
        """
        Args:
            target: Name of the air quality metric to be learned
            data_type: Planet data type to use (default: "RGB"). Set this and `instrument`
                to `None` to use only `extra_features` (no images) and entire dataset
                (including station days with no clear image).
            instrument: Planet instrument to use (default: "PS2"). Set this and `data_type`
                to `None` to use only `extra_features` (no images) and entire dataset
                (including station days with no clear image).
            channels: Image channels to use (default: `None` = all image channels)
            transform: Optional transform to augment the images (default: `None`)
            image_normalize:
                bool: Whether to z-score normalize images (after any `transform`) by mean
                    and std of train images (default: `True`)
                Callable: A custom normalization to apply to images after any `transform`
            extra_features: Optional list of additional features to use when predicting
                (default: `None`). Must be in `self.labels().columns`.
            extra_normalize:
                bool: Whether to z-score normalize extra data by mean and std of train
                    extra data (default: `True`)
                tuple[Series, Series]: means and stds to z-score normalize extra features
            exclude_outliers: Whether to exclude outlying target values (default: `True`)
            val_frac, test_frac: Fraction of samples to use for validation and testing
                (default: 0.2, 0.2). All other samples are used for training.
            splits: Optional path to file identifying train, val, and test samples
                (default: None). Overrides `val_frac` and `test_frac`.
            groupby: Optional name of column in `self.labels()` to group by when assigning
                train / val / test sets (default: "date")
            shuffle: Whether to shuffle the data (default: `True`). If `True` then:
                * Data are shuffled before assigning to train / val / test
                * Training data are reshuffled at start of each epoch (val and test data
                    are never shuffled)
            batch_size: Number of samples per train batch (default: 4)
            drop_last: Whether to drop the last train batch if it contains < `batch_size`
                samples (default: `False`). Val and test batches are never dropped.
            rand_seed: Seed for random number generator (default: `constants.SEED`)
        """

        super().__init__()
        self.save_hyperparameters()

        self.target = target

        if data_type is None and instrument is None and extra_features is None:
            raise ValueError(
                "Specify (`data_type`, `instrument`), `extra_features`, or both"
            )
        self.data_type = data_type
        self.instrument = instrument
        self.channels = channels
        self.transform = transform
        self.image_normalize = image_normalize

        if isinstance(extra_features, str):
            extra_features = [extra_features]
        self.extra_features = extra_features
        self.extra_normalize = extra_normalize

        self.exclude_outliers = exclude_outliers

        # Cache the base image dataset and labels (if no image dataset)
        # Need to do this before ensuring target and extra features are in labels
        self._image_dataset: PlanetClear | None = None
        self._labels: pd.DataFrame = None

        # Ensure target and extra features are in labels
        # This will load and cache base image dataset labels
        label_cols = self.labels().columns
        if not target in label_cols:
            raise ValueError(f"`target` not found in `labels()`: {target}")
        if self.extra_features is not None:
            missing = [var for var in self.extra_features if not var in label_cols]
            if any(missing):
                raise ValueError(f"`extra_features` not found in `labels()`: {missing}")

        # Ensure splits are valid
        if val_frac + test_frac >= 1:
            raise ValueError(
                f"`val_frac` + `test_frac` must be < 1; got: {val_frac + test_frac}"
            )
        self.val_frac = val_frac
        self.test_frac = test_frac

        if splits is not None:
            splits = Path(splits)
            if not splits.exists():
                raise FileNotFoundError(f"`splits` does not exist: {splits}")
        self.splits = splits

        self.groupby = groupby
        self.shuffle = shuffle

        self.batch_size = batch_size
        self.drop_last = drop_last
        self.rand_seed = rand_seed

        # Attributes assigned in self.setup()
        self.train_idx: list[int] = None
        self.val_idx: list[int] = None
        self.test_idx: list[int] = None
        self.target_idx: list[int] = None  # to track samples that are not missing target
        self.train_dataset: SupervisedDataset = None
        self.val_dataset: SupervisedDataset = None
        self.test_dataset: SupervisedDataset = None
        self.predict_dataset: SupervisedDataset = None
        self.setup_stages = []  # to track setup stages for restoring from checkpoint

    def __repr__(self) -> str:
        desc = f"{self.__class__.__name__}("
        desc += f"\n  target: {self.target}"
        desc += f"\n  image_dataset: {self.image_dataset()}"
        if self.transform is not None:
            desc += "\n  transform: " + str(self.transform).replace("\n)", "\n  )")
        desc += f"\n  extra_features: {self.extra_features}"
        desc += "\n  splits: "
        if self.splits is not None:
            desc += self.splits.name
        else:
            desc += f"val_frac={self.val_frac} test_frac={self.test_frac} seed={self.rand_seed}"
        desc += "\n)"
        return desc

    def has_image_dataset(self) -> bool:
        return self.data_type is not None and self.instrument is not None

    def has_extra_data(self) -> bool:
        return self.extra_features is not None

    def image_dataset(self, idx: Optional[list[int]] = None) -> PlanetClear | None:
        """Return the base image dataset or a subset of it"""

        # Return None if no image dataset
        if not self.has_image_dataset():
            return None

        # Cache base image dataset on first access
        if self._image_dataset is None:
            self._image_dataset = PlanetClear(
                data_type=self.data_type,
                instrument=self.instrument,
                channels=self.channels,
            )

        if idx is not None:
            return self._image_dataset.subset(idx)

        return self._image_dataset

    def labels(self, idx: Optional[list[int]] = None) -> pd.DataFrame:
        """Return the image dataset labels, or the extra DB if no image dataset"""

        # Cache on first access
        if self._labels is None:
            try:
                self._labels = self.image_dataset().labels
            except AttributeError:
                # Load imputed DB if no image dataset
                self._labels = (
                    load_db_imputed().loc[(slice(None), OP_STATIONS), :].reset_index()
                )

            # Paranoia: confirm labels have range index
            if not isinstance(self._labels.index, pd.RangeIndex):
                raise IndexError(
                    f"labels().index must be RangeIndex; got {type(self._labels.index)}"
                )

        if idx is not None:
            return self._labels.iloc[idx]

        return self._labels

    def target_values(self, idx: Optional[list[int]] = None) -> pd.Series:
        """Return values for target, possibly subset"""

        values = self.labels()[self.target]

        if idx is not None:
            return values.iloc[idx]

        return values

    def extra_values(self, idx: Optional[list[int]] = None) -> pd.DataFrame | None:
        """Return values for extra features, possibly subset, possibly normalized"""

        # Return None if no extra features
        if not self.has_extra_data():
            return None

        extra_values = self.labels()[self.extra_features]

        # Possibly subset
        if idx is not None:
            extra_values = extra_values.iloc[idx]

        # Possibly normalize
        if self.extra_normalize:
            extra_values = extra_values - self.extra_normalize[0]
            extra_values = extra_values / self.extra_normalize[1]

        return extra_values

    def split_train_val_test(self) -> tuple[list[int], list[int], list[int]]:
        """Split samples into train, val, and test sets"""

        if self.splits is not None:
            splits = pd.read_csv(
                self.splits, index_col=["date", "station"], parse_dates=["date"]
            )
            splits = self.labels().join(splits, on=["date", "station"])
            train_idx = splits.loc[splits["set"] == "train"].index.to_list()
            val_idx = splits.loc[splits["set"] == "val"].index.to_list()
            test_idx = splits.loc[splits["set"] == "test"].index.to_list()
        else:
            train_idx, val_idx, test_idx = split_train_val_test(
                data=self.labels(),
                test_frac=self.test_frac,
                val_frac=self.val_frac,
                groups=self.groupby,
                shuffle=self.shuffle,
                rand_seed=self.rand_seed,
            )

        # Return sorted indexes; data may be reshuffled by dataloaders
        return sorted(train_idx), sorted(val_idx), sorted(test_idx)

    def make_image_normalize(self) -> Normalize:
        """Return a transform that normalizes by mean and SD of train images"""

        if not self.has_image_dataset():
            return None

        if self.train_idx is None:
            raise ValueError("Set `self.train_idx` before making image normalize")

        # NOTE: this temporarily loads all train images to a local variable
        train_data = self.image_dataset(idx=self.train_idx).data
        return Normalize(
            mean=train_data.mean(dim=(0, -2, -1)),
            std=train_data.std(dim=(0, -2, -1)),
        )

    def make_extra_normalize(self) -> tuple[pd.DataFrame, pd.DataFrame]:
        """Return the mean and SD of train extra data (for normalization)"""

        if not self.has_extra_data():
            return None

        if self.train_idx is None:
            raise ValueError("Set `self.train_idx` before making `make_extra_normalize`")

        # Compute mean and SD of train extra data
        train_data = self.labels(idx=self.train_idx)[self.extra_features]
        return train_data.mean(), train_data.std()

    def make_target_idx(self) -> list[int]:
        """Return an index identifying samples with a valid value for the target"""

        values = self.target_values().dropna()  # drop missing values
        if self.exclude_outliers:
            values = values.loc[values < TARGET_LIMITS[self.target]]

        return values.index.to_list()

    def drop_where_target_missing(self, idx: list[int], name: str) -> list[int]:
        """Drop samples that have no value for self.target"""

        # Skip if split is empty e.g. if self.test_frac == 0
        if not any(idx):
            return idx

        # Drop where target missing
        new_idx = [x for x in idx if x in self.target_idx]

        # Raise if no samples left
        if not any(new_idx):
            raise ValueError(f"No {name} samples have value for {self.target}")

        # Report how many samples were dropped and how many remain
        change = len(idx) - len(new_idx)
        if change > 0:
            logger.warning(
                "%s: dropping %s %s samples with missing or outlying %s (%s remaining)",
                self.__class__.__name__,
                change,
                name,
                self.target,
                len(new_idx),
            )

        return new_idx

    def setup(self, stage: str) -> None:
        """Prepare data"""

        # Assign samples to train / val / test
        # Does not run if splits have already been assigned
        # Do this *before* dropping samples where target is missing to ensure splits are
        # same regardless of target if dataset and rand_seed are same
        if any([self.train_idx is None, self.val_idx is None, self.test_idx is None]):
            self.train_idx, self.val_idx, self.test_idx = self.split_train_val_test()

        # Possibly assign z-score normalization based on mean + std of train images
        # Do this *before* dropping samples where target is missing to ensure
        # normalization is same regardless of target if dataset and rand_seed are same
        # Does not run if self.xxx_normalize is False or is already a transform
        if self.image_normalize is True:
            self.image_normalize = self.make_image_normalize()
        if self.extra_normalize is True:
            self.extra_normalize = self.make_extra_normalize()

        # Drop samples where the target is missing
        # Do this *after* splitting, loading extra data, and assigning normalizations
        if self.target_idx is None:
            # Keep track of which samples have a value for target
            self.target_idx = self.make_target_idx()
            self.train_idx = self.drop_where_target_missing(self.train_idx, name="train")
            self.val_idx = self.drop_where_target_missing(self.val_idx, name="val")
            self.test_idx = self.drop_where_target_missing(self.test_idx, name="test")

        # Create train / val / test datasets
        # These yield tuples (image, extra, target, idx) for each sample in the dataset
        if stage == "fit":
            self.train_dataset = SupervisedDataset(
                target=self.target_values(idx=self.train_idx),
                image_dataset=self.image_dataset(idx=self.train_idx),
                transform=self.transform,
                normalize=self.image_normalize,
                extra=self.extra_values(idx=self.train_idx),
            )
        if stage in ["fit", "validate"]:
            self.val_dataset = SupervisedDataset(
                target=self.target_values(idx=self.val_idx),
                image_dataset=self.image_dataset(idx=self.val_idx),
                transform=self.transform,
                normalize=self.image_normalize,
                extra=self.extra_values(idx=self.val_idx),
            )
        if stage == "test":
            self.test_dataset = SupervisedDataset(
                target=self.target_values(idx=self.test_idx),
                image_dataset=self.image_dataset(idx=self.test_idx),
                transform=self.transform,
                normalize=self.image_normalize,
                extra=self.extra_values(idx=self.test_idx),
            )
        if stage == "predict":
            self.predict_dataset = SupervisedDataset(
                target=self.target_values(),
                image_dataset=self.image_dataset(),
                transform=self.transform,
                normalize=self.image_normalize,
                extra=self.extra_values(),
            )

        # Track which setup stages have been run to allow restoring from checkpoint
        if not stage in self.setup_stages:
            self.setup_stages.append(stage)

    def train_dataloader(self) -> DataLoader:
        return DataLoader(
            dataset=self.train_dataset,
            batch_size=self.batch_size,
            shuffle=self.shuffle,
            drop_last=self.drop_last,
            collate_fn=lambda x: x,  # uses __getitems__ so entire batch loaded at once
        )

    def val_dataloader(self) -> DataLoader:
        return DataLoader(
            dataset=self.val_dataset,
            # batch_size=self.batch_size,
            batch_size=32,  # batch size 32 is about max that fits in memory
            shuffle=False,  # Never shuffle validation data
            drop_last=False,  # Never drop validation batches
            collate_fn=lambda x: x,  # uses __getitems__ so entire batch loaded at once
        )

    def test_dataloader(self) -> DataLoader:
        return DataLoader(
            dataset=self.test_dataset,
            # batch_size=self.batch_size,
            batch_size=32,  # batch size 32 is about max that fits in memory
            shuffle=False,  # Do not shuffle test data
            drop_last=False,  # Never drop test batches
            collate_fn=lambda x: x,  # uses __getitems__ so entire batch loaded at once
        )

    def predict_dataloader(self) -> DataLoader:
        return DataLoader(
            dataset=self.predict_dataset,
            # batch_size=self.batch_size,
            batch_size=32,  # batch size 32 is about max that fits in memory
            shuffle=False,  # Do not shuffle predict data
            drop_last=False,  # Never drop predict batches
            collate_fn=lambda x: x,  # uses __getitems__ so entire batch loaded at once
        )

    def state_dict(self) -> dict:
        """State to allow loading from checkpoint"""

        # Only save state that is not stored in self.hparams
        # Don't save datasets -> would make really big file
        state = dict(
            train_idx=self.train_idx,
            val_idx=self.val_idx,
            test_idx=self.test_idx,
            image_normalize=self.image_normalize,
            extra_normalize=self.extra_normalize,
            target_idx=self.target_idx,
            setup_stages=self.setup_stages,  # track which setup stages have been run
        )
        return state

    def load_state_dict(self, state_dict: dict) -> None:
        """Restore state based on state_dict"""

        # Restore train / val / test split and normalizations
        self.unsetup()
        for k, v in state_dict.items():
            setattr(self, k, v)

        # Restore setup stages (train / val / test / predict datasets)
        # Need to do this b/c when resuming training from checkpoint lightning calls
        # datamodule's setup() *before* loading state dict
        for stage in self.setup_stages:
            self.setup(stage)

    def unsetup(self) -> None:
        """Clear train / val / test splits so they can be reassigned"""

        self.train_idx = None
        self.val_idx = None
        self.test_idx = None
        self.image_normalize = self.hparams["image_normalize"]  # reset to initial value
        self.extra_normalize = self.hparams["extra_normalize"]  # reset to initial value
        self.target_idx = None
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None
        self.predict_dataset = None
        self.setup_stages = []


def _sanity_check():
    """Sanity checks"""

    import torch
    from torch.utils.data.sampler import RandomSampler, SequentialSampler

    # logging.basicConfig(level=logging.DEBUG)

    print("SupervisedDataModule")
    print("--------------------")
    module = SupervisedDataModule(
        target="pm10",
        data_type="RGB",
        instrument="PS2.SD",
        extra_features=["t2m", "rh"],
        shuffle=False,
    )
    module._image_dataset = module.image_dataset().subset(range(10))  # subset for testing
    module._labels = None  # subset for testing (will be reset on first access)
    print(module)

    # image_dataset() is unloaded
    assert module.image_dataset().is_loaded() is False

    # image_dataset() can be None
    no_images = SupervisedDataModule(
        target="pm10", data_type=None, instrument=None, extra_features=["t2m", "rh"]
    )
    assert no_images.image_dataset() is None
    assert no_images.has_image_dataset() is False

    # image_dataset(idx) returns subset
    subset = module.image_dataset(range(3))
    assert subset.file_idx == list(range(3))
    assert no_images.image_dataset(range(3)) is None
    del subset

    # labels() returns the image dataset labels, or extra db; can subset
    extra_db = load_db_imputed().loc[(slice(None), OP_STATIONS), :].reset_index()
    assert all(module.labels() == module.image_dataset().labels)
    assert all(no_images.labels() == extra_db)
    assert all(module.labels(range(3)) == module.image_dataset().labels.iloc[range(3)])
    assert all(no_images.labels(range(3)) == extra_db.iloc[range(3)])

    # extra_values() returns values of extra features; can subset
    module.extra_normalize, no_images.extra_normalize = (0, 1), (0, 1)
    assert all(module.extra_values() == module.labels()[module.extra_features])
    assert all(no_images.extra_values() == no_images.labels()[module.extra_features])
    assert all(
        module.extra_values(range(3)) == module.labels(range(3))[module.extra_features]
    )
    assert all(
        no_images.extra_values(range(3)) == extra_db[module.extra_features].iloc[range(3)]
    )
    no_extra = SupervisedDataModule(target="pm10", data_type="RGB", instrument="PS2.SD")
    assert no_extra.extra_values() is None
    assert no_extra.has_extra_data() is False
    assert no_extra.extra_values(range(3)) is None
    module.extra_normalize, no_images.extra_normalize = True, True

    # target_values() return values for target; can subset
    assert all(
        module.target_values(range(3, 6))  # avoid rows where target is missing
        == module.labels(range(3, 6))[module.target]
    )
    assert all(
        no_images.target_values(range(3)) == extra_db[module.target].iloc[range(3)]
    )
    assert all(
        no_extra.target_values(range(3, 6))
        == no_extra.labels(range(3, 6))[no_extra.target]
    )

    # split_train_val_test() returns train / val / test splits
    train_idx, val_idx, test_idx = module.split_train_val_test()
    assert train_idx == list(range(0, 6))
    assert val_idx == list(range(6, 8))
    assert test_idx == list(range(8, 10))

    # split_train_val_test() does not load image_dataset's data
    assert module.image_dataset().is_loaded() is False

    # make_image_normalize() returns normalization transform based on train images
    module.train_idx = list(range(0, 6))
    normalize = module.make_image_normalize()
    train_data = module.image_dataset(idx=module.train_idx).data
    assert all(train_data.mean(dim=(0, -2, -1)) == normalize.mean)
    assert all(train_data.std(dim=(0, -2, -1)) == normalize.std)
    del train_data, normalize
    module.train_idx = None

    # make_image_normalize() does not load image_dataset's data
    assert module.image_dataset().is_loaded() is False

    # make_image_normalize() returns None if no image dataset
    assert no_images.make_image_normalize() is None

    # make_extra_normalize() returns mean + std of extra train data
    module.train_idx = list(range(0, 6))
    train = module.labels()[module.extra_features].iloc[module.train_idx]
    extra_normalize = module.make_extra_normalize()
    assert all(extra_normalize[0] == train.mean())
    assert all(extra_normalize[1] == train.std())
    del train, extra_normalize
    module.train_idx = None

    # make_extra_normalize() returns None if no extra data
    assert SupervisedDataModule("pm10", "RGB", "PS2.SD").make_extra_normalize() is None

    # drop_where_target_missing() drops samples that have no value for the target
    module.train_idx = list(range(0, 6))
    module.val_idx = list(range(6, 8))
    module.test_idx = list(range(8, 10))
    module.target_idx = [0, 1, 3, 4, 5, 6, 9]
    assert module.drop_where_target_missing(module.train_idx, "train") == [0, 1, 3, 4, 5]
    assert module.drop_where_target_missing(module.val_idx, "val") == [6]
    assert module.drop_where_target_missing(module.test_idx, "test") == [9]
    module.train_idx, module.val_idx, module.test_idx = None, None, None
    module.target_idx = None

    # setup() assigns train/val/test and normalizers (if any)
    assert module.train_idx is None
    assert module.val_idx is None
    assert module.test_idx is None
    assert module.image_normalize is True
    assert module.extra_normalize is True
    assert module.target_idx is None
    assert module.train_dataset is None
    assert module.val_dataset is None
    assert module.test_dataset is None
    assert module.predict_dataset is None
    assert module.setup_stages == []
    module.setup("fit")
    assert module.train_idx is not None
    assert module.val_idx is not None
    assert module.test_idx is not None
    assert isinstance(module.image_normalize, Normalize)
    assert isinstance(module.extra_normalize, tuple)
    assert isinstance(module.extra_normalize[0], pd.Series)
    assert isinstance(module.extra_normalize[1], pd.Series)

    # setup() drops samples missing target
    assert module.target_idx is not None
    assert module.train_idx == [0, 1, 3, 4, 5]
    assert module.val_idx == [6]
    assert module.test_idx == [9]

    # setup("fit") assigns train and val datasets
    dataset = module.train_dataset
    assert dataset.target_name == module.target
    assert torch.equal(
        dataset.target, torch.as_tensor(module.target_values(module.train_idx).to_numpy())
    )
    assert dataset.image_dataset.file_idx == module.train_idx
    assert dataset.transform == module.transform
    assert dataset.normalize == module.image_normalize
    assert dataset.extra_names == module.extra_features
    assert torch.equal(
        dataset.extra, torch.as_tensor(module.extra_values(module.train_idx).to_numpy())
    )
    assert module.val_dataset is not None
    assert module.test_dataset is None
    assert module.predict_dataset is None
    assert module.setup_stages == ["fit"]
    del dataset

    # setup("validate") assigns val dataset
    module.train_dataset, module.val_dataset = None, None
    module.setup_stages = []
    module.setup("validate")
    assert module.train_dataset is None
    dataset = module.val_dataset
    assert dataset.target_name == module.target
    assert torch.equal(
        dataset.target, torch.as_tensor(module.target_values(module.val_idx).to_numpy())
    )
    assert dataset.image_dataset.file_idx == module.val_idx
    assert dataset.transform == module.transform
    assert dataset.normalize == module.image_normalize
    assert dataset.extra_names == module.extra_features
    assert torch.equal(
        dataset.extra, torch.as_tensor(module.extra_values(module.val_idx).to_numpy())
    )
    assert module.test_dataset is None
    assert module.predict_dataset is None
    assert module.setup_stages == ["validate"]
    del dataset

    # setup("test") assigns test dataset
    module.val_dataset = None
    module.setup_stages = []
    module.setup("test")
    assert module.train_dataset is None
    assert module.val_dataset is None
    dataset = module.test_dataset
    assert dataset.target_name == module.target
    assert torch.equal(
        dataset.target, torch.as_tensor(module.target_values(module.test_idx).to_numpy())
    )
    assert dataset.image_dataset.file_idx == module.test_idx
    assert dataset.transform == module.transform
    assert dataset.normalize == module.image_normalize
    assert dataset.extra_names == module.extra_features
    assert torch.equal(
        dataset.extra, torch.as_tensor(module.extra_values(module.test_idx).to_numpy())
    )
    assert module.predict_dataset is None
    assert module.setup_stages == ["test"]
    del dataset

    # setup("predict") assigns predict dataset
    module.test_dataset = None
    module.setup_stages = []
    module.setup("predict")
    assert module.train_dataset is None
    assert module.val_dataset is None
    assert module.test_dataset is None
    dataset = module.predict_dataset
    assert dataset.target_name == module.target
    assert len(dataset.target) == 10
    assert torch.equal(
        dataset.target.nan_to_num(-1),
        torch.as_tensor(module.target_values().to_numpy()).nan_to_num(-1),
    )
    assert dataset.image_dataset.file_idx == module.target_values().index.to_list()
    assert dataset.transform == module.transform
    assert dataset.normalize == module.image_normalize
    assert dataset.extra_names == module.extra_features
    assert torch.equal(dataset.extra, torch.as_tensor(module.extra_values().to_numpy()))
    assert module.setup_stages == ["predict"]
    module.predict_dataset = None
    del dataset

    # setup() doesn't overwrite existing splits, normalize, or extra data and doesn't
    # redrop samples missing target
    train_idx = module.train_idx
    val_idx = module.val_idx
    test_idx = module.test_idx
    image_normalize = module.image_normalize
    extra_normalize = module.extra_normalize
    module.target_idx = list(range(2, 9))
    module.shuffle = True
    module.setup("fit")
    assert module.train_idx == train_idx
    assert module.val_idx == val_idx
    assert module.test_idx == test_idx
    assert module.image_normalize == image_normalize
    assert all(module.extra_normalize[0] == extra_normalize[0])
    assert all(module.extra_normalize[1] == extra_normalize[1])
    assert module.target_idx == list(range(2, 9))

    # unsetup() clears splits, normalize, extra data, and datasets for reassignment
    module.unsetup()
    assert module.train_idx is None
    assert module.val_idx is None
    assert module.test_idx is None
    assert module.image_normalize is True
    assert module.extra_normalize is True
    assert module.target_idx is None
    assert module.train_dataset is None
    assert module.val_dataset is None
    assert module.test_dataset is None
    assert module.predict_dataset is None
    assert module.setup_stages == []

    # Train / val / test split only changes if seed changes *and* splits are unset
    # Module with shuffle and seed
    module.shuffle = True
    module.rand_seed = 0
    module.setup("test")
    assert module.test_idx == [0, 5]
    # Unsetup and resetup with same seed
    module.unsetup()
    module.setup("test")
    assert module.test_idx == [0, 5]
    # Resetup with new seed
    module.rand_seed += 1
    module.setup("test")
    assert module.test_idx == [0, 5]
    # Unsetup and resetup with new seed
    module.unsetup()
    module.setup("test")
    assert module.test_idx != [0, 5]

    # state_dict stores train/val/test indexes, normalization, target_idx setup stages
    state_dict = module.state_dict()
    assert "train_idx" in state_dict
    assert "val_idx" in state_dict
    assert "test_idx" in state_dict
    assert "image_normalize" in state_dict
    assert "extra_normalize" in state_dict
    assert "target_idx" in state_dict
    assert "setup_stages" in state_dict

    # load_state_dict sets train/val/test idx, normalize, target_idx, and re-runs setup
    # stages (to allow loading from checkpoint)
    module.rand_seed -= 1
    module.unsetup()
    module.setup("fit")
    module.setup("test")
    module.setup("predict")
    state_dict = module.state_dict()
    module.unsetup()
    assert module.train_idx is None
    assert module.val_idx is None
    assert module.test_idx is None
    assert module.image_normalize is True
    assert module.extra_normalize is True
    assert module.target_idx is None
    assert module.train_dataset is None
    assert module.val_dataset is None
    assert module.test_dataset is None
    assert module.predict_dataset is None
    assert module.setup_stages == []
    module.load_state_dict(state_dict)
    assert module.train_idx == state_dict["train_idx"]
    assert module.val_idx == state_dict["val_idx"]
    assert module.test_idx == state_dict["test_idx"]
    assert module.image_normalize == state_dict["image_normalize"]
    assert module.extra_normalize == state_dict["extra_normalize"]
    assert module.target_idx == state_dict["target_idx"]
    assert module.setup_stages == state_dict["setup_stages"]
    assert isinstance(module.train_dataset, SupervisedDataset)
    assert isinstance(module.val_dataset, SupervisedDataset)
    assert isinstance(module.test_dataset, SupervisedDataset)
    assert isinstance(module.predict_dataset, SupervisedDataset)

    # Data loaders
    module = SupervisedDataModule(
        "pm10", "RGB", "PS2.SD", extra_features=["t2m", "rh"], batch_size=4
    )
    module._image_dataset = module.image_dataset().subset(range(24))  # subset for testing
    module._labels = None  # subset for testing (will be reset on first access)
    module.setup("fit")
    loader = module.train_dataloader()
    assert loader.dataset == module.train_dataset
    assert loader.batch_size == module.batch_size
    assert isinstance(loader.sampler, RandomSampler)
    assert loader.drop_last == module.drop_last
    batch = next(iter(loader))
    assert batch[0].shape == (module.batch_size, 3, 334, 334)
    assert batch[1].shape == (module.batch_size, 2)
    assert batch[2].shape == (module.batch_size,)
    assert batch[3].shape == (module.batch_size,)

    loader = module.val_dataloader()
    assert loader.dataset == module.val_dataset
    assert loader.batch_size == 32  # hardcoded to 32 for faster validation
    assert isinstance(loader.sampler, SequentialSampler)  # never shuffle val samples
    assert loader.drop_last is False  # never drop val samples
    batch = next(iter(loader))
    assert batch[0].shape == (len(module.val_dataset), 3, 334, 334)
    assert batch[1].shape == (len(module.val_dataset), 2)
    assert batch[2].shape == (len(module.val_dataset),)
    assert batch[3].shape == (len(module.val_dataset),)

    module.setup("test")
    loader = module.test_dataloader()
    assert loader.dataset == module.test_dataset
    assert loader.batch_size == 32  # hardcoded to 32 for faster validation
    assert isinstance(loader.sampler, SequentialSampler)  # never shuffle test samples
    assert loader.drop_last is False  # never drop test samples
    batch = next(iter(loader))
    assert batch[0].shape == (len(module.test_dataset), 3, 334, 334)
    assert batch[1].shape == (len(module.test_dataset), 2)
    assert batch[2].shape == (len(module.test_dataset),)
    assert batch[3].shape == (len(module.test_dataset),)

    module.setup("predict")
    loader = module.predict_dataloader()
    assert loader.dataset == module.predict_dataset
    assert loader.batch_size == 32  # hardcoded to 32 for faster validation
    assert isinstance(loader.sampler, SequentialSampler)  # never shuffle predict samples
    assert loader.drop_last is False  # never drop predict samples
    batch = next(iter(loader))
    assert batch[0].shape == (len(module.predict_dataset), 3, 334, 334)
    assert batch[1].shape == (len(module.predict_dataset), 2)
    assert batch[2].shape == (len(module.predict_dataset),)
    assert batch[3].shape == (len(module.predict_dataset),)

    # Can load splits from file
    module = SupervisedDataModule(
        "pm10_imp", "RGB", "PS2.SD", shuffle=True, splits="data/processed/splits.csv"
    )
    module._image_dataset = module.image_dataset().subset(range(20))  # subset for testing
    module._labels = None  # subset for testing (will be reset on first access)
    module.setup("fit")
    assert module.train_idx == [1, 2, 6, 7, 9, 10, 11, 13, 14, 15, 17, 18, 19]
    assert module.val_idx == [0, 3, 4, 5, 8, 16]
    assert module.test_idx == [12]


if __name__ == "__main__":
    _sanity_check()
