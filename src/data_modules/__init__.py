"""DataModule classes"""

from .contrastive_data_module import ContrastiveDataModule
from .supervised_data_module import SupervisedDataModule
from .embedding_data_module import EmbeddingDataModule  # load after SupervisedDataModule
