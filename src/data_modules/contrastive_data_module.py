"""DataModule for contrastive pretraining"""

from pathlib import Path
from typing import Any, Callable, Optional

import pandas as pd
from pytorch_lightning import LightningDataModule
from torchvision.transforms import Normalize
from torch.utils.data import DataLoader

from constants import SEED
from datasets import PlanetClear, ContrastiveDataset
from transforms import Augment
from utils import split_train_val_test


class ContrastiveDataModule(LightningDataModule):
    """DataModule for contrastive pretraining"""

    def __init__(
        self,
        data_type: str = "RGB",
        instrument: str = "PS2",
        *,
        channels: Optional[str] = None,
        crop_size: int = 96,
        normalize: bool | Normalize = True,
        val_frac: float = 0.2,
        test_frac: float = 0.2,
        splits: Optional[str] = None,
        groupby: Optional[str] = "date",
        shuffle: bool = True,
        train_all: bool = False,
        batch_size: int = 32,
        drop_last: bool = False,
        rand_seed: Optional[int] = SEED,
    ):
        """
        Args:
            data_type: The Planet data type to include (default: "RGB")
            instrument: The Planet instrument to include (default: "PS2")
            channels: Channels to use (default: `None` = all data channels)
            crop_size: Size for `RandomResizedCrop` augmentation (default: 96)
            normalize:
                bool: Whether to z-score normalize augmented views by bandwise mean and
                    std of train images (default: `True`)
                Callable: A custom transform to normalize augmented views
            val_frac, test_frac: Fraction of images to use for validation and testing
                (default: 0.2, 0.2). All other images are used for training.
            splits: Optional path to file identifying train, val, and test images
                (default: None). Overrides `val_frac` and `test_frac`.
            groupby: Optional name of column in `image_dataset().labels` to group by when
                assigning train / val / test sets (default: "date")
            shuffle: Whether to shuffle the images (default: `True`). If `True` then:
                * Images are shuffled before assigning to train / val / test
                * Training images are reshuffled at start of each epoch (val and test
                    images are never shuffled)
            train_all: Whether to train on all images (default: False). If `True` then
                model is trained on all train, val, and test images.
            batch_size: Number of images per train batch (default: 32)
            drop_last: Whether to drop the last train batch if it contains < `batch_size`
                samples (default: `False`). Val and test batches are never dropped.
            rand_seed: Seed for random number generator (default: `constants.SEED`)
        """

        super().__init__()
        self.save_hyperparameters()

        self.data_type = data_type
        self.instrument = instrument
        self.channels = channels
        self.crop_size = crop_size
        self.normalize = normalize

        # Ensure splits are valid
        if val_frac + test_frac >= 1:
            raise ValueError(
                f"`val_frac` + `test_frac` must be < 1; got: {val_frac + test_frac}"
            )
        self.val_frac = val_frac
        self.test_frac = test_frac

        if splits is not None:
            splits = Path(splits)
            if not splits.exists():
                raise FileNotFoundError(f"`splits` does not exist: {splits}")
        self.splits = splits

        self.groupby = groupby
        self.shuffle = shuffle
        self.train_all = train_all

        self.batch_size = batch_size
        self.drop_last = drop_last
        self.rand_seed = rand_seed

        # Attributes assigned in self.setup()
        self.train_idx: list[int] = None
        self.val_idx: list[int] = None
        self.test_idx: list[int] = None
        self.train_dataset: ContrastiveDataset = None
        self.val_dataset: ContrastiveDataset = None
        self.setup_stages = []  # to track setup stages for restoring from checkpoint

    def __repr__(self) -> str:
        desc = f"{self.__class__.__name__}("
        desc += f"\n  image_dataset: {self.image_dataset()}"
        desc += f"\n  crop_size: {self.crop_size}"
        desc += "\n  splits: "
        if self.splits is not None:
            desc += self.splits.name
        else:
            desc += f"val {self.val_frac} test {self.test_frac} seed {self.rand_seed}"
        if self.train_all:
            desc += " train all"
        desc += "\n)"
        return desc

    def image_dataset(self, idx: Optional[list[int]] = None) -> PlanetClear:
        """Base image dataset, possibly subset"""

        return PlanetClear(
            data_type=self.data_type,
            instrument=self.instrument,
            channels=self.channels,
            idx=idx,
        )

    def split_train_val_test(self) -> tuple[list[int], list[int], list[int]]:
        """Split images into train, val, and test sets"""

        if self.splits is not None:
            splits = pd.read_csv(
                self.splits, index_col=["date", "station"], parse_dates=["date"]
            )
            splits = self.image_dataset().labels.join(splits, on=["date", "station"])
            train_idx = splits.loc[splits["set"] == "train"].index.to_list()
            val_idx = splits.loc[splits["set"] == "val"].index.to_list()
            test_idx = splits.loc[splits["set"] == "test"].index.to_list()
        else:
            train_idx, val_idx, test_idx = split_train_val_test(
                data=self.image_dataset().labels,
                test_frac=self.test_frac,
                val_frac=self.val_frac,
                groups=self.groupby,
                shuffle=self.shuffle,
                rand_seed=self.rand_seed,
            )

        if self.train_all:
            train_idx = train_idx + val_idx + test_idx

        # Return sorted indexes; data may be reshuffled by dataloaders
        return sorted(train_idx), sorted(val_idx), sorted(test_idx)

    def make_normalize(self) -> Callable:
        """Return a transform that normalizes images by mean and SD of train data"""

        if self.train_idx is None:
            raise ValueError("Set `self.train_idx` before making image normalize")

        # NOTE: this temporarily loads all train images to a local variable
        train_data = self.image_dataset(idx=self.train_idx).data
        return Normalize(
            mean=train_data.mean(dim=(0, -2, -1)),
            std=train_data.std(dim=(0, -2, -1)),
        )

    def setup(self, stage: str) -> None:
        """Prepare data"""

        # Assign samples to train / val / test
        # Does not run if splits have already been assigned
        if any([self.train_idx is None, self.val_idx is None, self.test_idx is None]):
            self.train_idx, self.val_idx, self.test_idx = self.split_train_val_test()

        # If self.normalize is True assign to a z-score transform
        # Does not run if self.normalize is False or is already a transform
        if self.normalize is True:
            self.normalize = self.make_normalize()

        # Create train / val datasets
        # These yield pairs of augmented views for each image in the image dataset
        # Don't need test dataset b/c never used, test_idx is only set to exclude test
        # images from train and val
        if stage in ["fit", "train"]:
            self.train_dataset = ContrastiveDataset(
                image_dataset=self.image_dataset(idx=self.train_idx),
                transform=Augment(crop_size=self.crop_size),
                normalize=self.normalize,
            )
        if stage in ["fit", "validate"]:
            self.val_dataset = ContrastiveDataset(
                image_dataset=self.image_dataset(idx=self.val_idx),
                transform=Augment(crop_size=self.crop_size),
                normalize=self.normalize,
            )

        # Track which setup stages have been run to allow restoring from checkpoint
        if not stage in self.setup_stages:
            self.setup_stages.append(stage)

    def train_dataloader(self) -> DataLoader:
        return DataLoader(
            dataset=self.train_dataset,
            batch_size=self.batch_size,
            shuffle=self.shuffle,
            drop_last=self.drop_last,
            collate_fn=lambda x: x,  # uses __getitems__ so entire batch loaded at once
        )

    def val_dataloader(self) -> DataLoader:
        return DataLoader(
            dataset=self.val_dataset,
            batch_size=32,  # batch size 32 is about max that fits in memory
            shuffle=False,  # Never shuffle validation data
            drop_last=False,  # Never drop validation batches
            collate_fn=lambda x: x,  # uses __getitems__ so entire batch loaded at once
        )

    def state_dict(self) -> dict[str, Any]:
        """State to allow loading from checkpoint"""

        # Only save state that is not stored in self.hparams
        # Don't save datasets -> would make really big file
        state = dict(
            train_idx=self.train_idx,
            val_idx=self.val_idx,
            test_idx=self.test_idx,
            normalize=self.normalize,
            setup_stages=self.setup_stages,  # track which setup stages have been run
        )
        return state

    def load_state_dict(self, state_dict: dict[str, Any]) -> None:
        """Restore state based on state_dict"""

        # Restore train / val / test split and normalization
        self.unsetup()
        for k, v in state_dict.items():
            setattr(self, k, v)

        # Restore setup stages (train / val datasets)
        # Need to do this b/c when resuming training from checkpoint lightning calls
        # datamodule's setup() *before* loading state dict
        for stage in self.setup_stages:
            self.setup(stage)

    def unsetup(self) -> None:
        """Clear train / val / test splits so they can be reassigned"""

        self.train_idx = None
        self.val_idx = None
        self.test_idx = None
        self.normalize = self.hparams_initial["normalize"]  # reset to initial value
        self.train_dataset = None
        self.val_dataset = None
        self.setup_stages = []


def _sanity_check():
    """Sanity checks"""

    from torch.utils.data.sampler import RandomSampler, SequentialSampler

    def subset_for_testing(
        module: ContrastiveDataModule, n: int = 10
    ) -> ContrastiveDataModule:
        """Limit the module's image_dataset to the first n samples"""

        def testing_dataset(idx=range(n)):
            return PlanetClear(
                data_type=module.data_type,
                instrument=module.instrument,
                channels=module.channels,
                idx=idx,
            )

        module.image_dataset = testing_dataset
        return module

    print("ContrastiveDataModule")
    print("---------------------")
    module = subset_for_testing(
        ContrastiveDataModule("RGB", "PS2.SD", shuffle=False), n=20
    )
    print(module)

    # image_dataset() starts unloaded
    assert module.image_dataset().is_loaded() is False

    # split_train_val_test() returns train / val / test splits
    train_idx, val_idx, test_idx = module.split_train_val_test()
    assert train_idx == list(range(0, 12))
    assert val_idx == list(range(12, 16))
    assert test_idx == list(range(16, 20))

    # split_train_val_test() does not load image_dataset's data
    assert module.image_dataset().is_loaded() is False

    # train_mean_std_normalizer() returns normalizer transform based on train data
    module.train_idx = train_idx
    normalize = module.make_normalize()
    train_data = module.image_dataset(idx=train_idx).data
    assert all(train_data.mean(dim=(0, -2, -1)) == normalize.mean)
    assert all(train_data.std(dim=(0, -2, -1)) == normalize.std)
    del train_data, normalize
    module.train_idx = None

    # get_train_mean_std_normalizer() does not load image_dataset's data
    assert module.image_dataset().is_loaded() is False

    # setup() assigns train/val/test and normalize
    assert module.train_idx is None
    assert module.val_idx is None
    assert module.test_idx is None
    assert module.normalize is True
    assert module.train_dataset is None
    assert module.val_dataset is None
    assert module.setup_stages == []
    module.setup("fit")
    assert module.train_idx is not None
    assert module.val_idx is not None
    assert module.test_idx is not None
    assert isinstance(module.normalize, Normalize)

    # setup("fit") assigns train and val datasets
    assert module.train_dataset is not None
    assert module.val_dataset is not None
    assert module.setup_stages == ["fit"]

    # setup("validate") assigns val dataset
    module.train_dataset, module.val_dataset = None, None
    module.setup_stages = []
    module.setup("validate")
    assert module.train_dataset is None
    assert module.val_dataset is not None
    module.val_dataset = None
    assert module.setup_stages == ["validate"]

    # setup() doesn't overwrite existing splits or normalize
    module.shuffle = True
    normalize = module.normalize
    module.setup("fit")
    assert module.train_idx == train_idx
    assert module.val_idx == val_idx
    assert module.test_idx == test_idx
    assert module.normalize == normalize
    module.setup_stages = ["fit", "validate"]

    # unsetup() clears splits, normalize, and datasets so they can be reassigned
    module.unsetup()
    assert module.train_idx is None
    assert module.val_idx is None
    assert module.test_idx is None
    assert module.normalize is True
    assert module.train_dataset is None
    assert module.val_dataset is None
    assert module.setup_stages == []

    # Train / val / test split only changes if seed changes *and* splits are unset
    # Module with shuffle and seed
    module = subset_for_testing(
        ContrastiveDataModule("RGB", "PS2.SD", shuffle=True, rand_seed=0), n=20
    )
    module.setup("test")
    assert module.test_idx == [0, 3, 12, 15]
    # Unsetup and resetup with same seed
    module.unsetup()
    module.setup("test")
    assert module.test_idx == [0, 3, 12, 15]
    # Resetup with new seed
    module.rand_seed = module.rand_seed + 1
    module.setup("test")
    assert module.test_idx == [0, 3, 12, 15]
    # Unsetup and resetup with new seed
    module.unsetup()
    module.setup("test")
    assert module.test_idx != [0, 3, 12, 15]

    # state_dict stores train/val/test indexes and normalization
    state_dict = module.state_dict()
    assert "train_idx" in state_dict
    assert "val_idx" in state_dict
    assert "test_idx" in state_dict
    assert "normalize" in state_dict
    assert "setup_stages" in state_dict

    # load_state_dict() sets train/val/test idx and normalize and re-runs setup stages
    # (to allow loading from checkpoint)
    module.unsetup()
    module.setup("fit")
    state_dict = module.state_dict()
    module.unsetup()
    assert module.train_idx is None
    assert module.val_idx is None
    assert module.test_idx is None
    assert module.normalize is True
    assert module.train_dataset is None
    assert module.val_dataset is None
    assert module.setup_stages == []
    module.load_state_dict(state_dict)
    assert module.train_idx == state_dict["train_idx"]
    assert module.val_idx == state_dict["val_idx"]
    assert module.test_idx == state_dict["test_idx"]
    assert module.normalize == state_dict["normalize"]
    assert module.setup_stages == state_dict["setup_stages"]
    assert isinstance(module.train_dataset, ContrastiveDataset)
    assert isinstance(module.val_dataset, ContrastiveDataset)

    # Data loaders
    module.batch_size = 4
    module.setup("fit")
    loader = module.train_dataloader()
    assert loader.dataset == module.train_dataset
    assert loader.batch_size == module.batch_size
    assert isinstance(loader.sampler, RandomSampler)
    assert loader.drop_last == module.drop_last
    batch = next(iter(loader))
    assert batch[0].shape == (module.batch_size, 3, 96, 96)
    assert batch[1].shape == batch[0].shape
    assert len(batch[2]) == module.batch_size

    loader = module.val_dataloader()
    assert loader.dataset == module.val_dataset
    assert loader.batch_size == 32  # hardcoded to 32 for faster validation
    assert isinstance(loader.sampler, SequentialSampler)
    assert loader.drop_last is False
    batch = next(iter(loader))
    assert batch[0].shape == (module.batch_size, 3, 96, 96)
    assert batch[1].shape == batch[0].shape
    assert len(batch[2]) == module.batch_size

    # Can load splits from file
    module = subset_for_testing(
        ContrastiveDataModule("RGB", "PS2.SD", shuffle=True, splits="data/processed/splits.csv"), n=20
    )
    module.setup("fit")
    assert module.train_idx == [1, 2, 6, 7, 9, 10, 11, 13, 14, 15, 17, 18, 19]
    assert module.val_idx == [0, 3, 4, 5, 8, 16]
    assert module.test_idx == [12]


if __name__ == "__main__":
    _sanity_check()
