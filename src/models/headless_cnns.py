"""Headless convolutional neural networks for semi-supervised learning"""

from pathlib import Path
from typing import Optional

import torch
from torch import nn, Tensor
from torchvision.models import resnet50, ResNet


def resnet50_headless(
    *,
    in_channels: int = 3,
    zero_init_residual: bool = True,
    weights: Optional[str] = None,
    **kwargs,
) -> tuple[ResNet, int]:
    """Initialize a headless ResNet50 model

    Args:
        in_channels: Number of channels in the input data (default: 3)
        kwargs: Further arguments passed to torchvision.models.resnet50()

    Returns:
        An headless ResNet50 model accepting in_channels input data channels
    """

    # Initialize a ResNet50 model
    model = resnet50(weights=weights, zero_init_residual=zero_init_residual, **kwargs)

    # Save the input dimension of the original output layer. This will be the output
    # dimension of the headless model i.e. the number of features in a representation
    out_dim = model.fc.in_features

    # Make the model headless by setting output layer to Identity()
    model.fc = torch.nn.Identity()

    # Possibly change the number of channels
    if in_channels != model.conv1.in_channels:
        # Save the original conv1 layer weights
        n_copy_channels = min(model.conv1.in_channels, in_channels)
        orig_conv1_weights = model.conv1.weight[:, 0:n_copy_channels]

        # Make a new conv1 that accepts in_channels channels
        model.conv1 = model.conv1.__class__(
            in_channels=in_channels,
            out_channels=model.conv1.out_channels,
            kernel_size=model.conv1.kernel_size,
            stride=model.conv1.stride,
            padding=model.conv1.padding,
            bias=model.conv1.bias,
        )

        # Copy the original conv1 weights to the new conv1
        # Use torch.no_grad() to avoid "a view of a leaf Variable that requires grad ..."
        # error; final model.conv1.weight still has requires_grad
        with torch.no_grad():
            model.conv1.weight[:, 0:n_copy_channels] = orig_conv1_weights
        assert model.conv1.weight.requires_grad  # ensure requires grad

    return model, out_dim


class HeadlessCNN(nn.Module):
    """A headless convolutional neural network for semi-supervised learning"""

    def __init__(
        self,
        architecture: str = "resnet50",
        in_channels: int = 3,
        weights: Optional[str] = None,
        **kwargs,
    ) -> None:
        """
        Args:
            architecture: A CNN architecture (default: "resnet50"). A headless version of
                the CNN will be initialized i.e. the CNN will output features
                (representations of images) rather than predictions.
            in_channels: Number of channels in the input data (default: 3)
            weights: Optional name of torchvision weights to load or path to saved weights
                or pretraining checkpoint (default: `None`)
            **kwargs: Further arguments passed to the CNN's `__init__()`
        """

        super().__init__()

        self.architecture = architecture.lower()
        self.in_channels = in_channels
        self.weights = weights

        # Determine whether weights is a torchvision weights name or a path
        self.weights_path = None
        if self.weights is not None and Path(self.weights).suffix != "":
            self.weights_path = self.weights
            self.weights = None

        # Initialize the CNN
        match self.architecture:
            case "resnet50":
                self.model, self.out_dim = resnet50_headless(
                    in_channels=self.in_channels, weights=self.weights, **kwargs
                )
            case _:
                raise NotImplementedError(f"Unknown `architecture`: {self.architecture}")

        # Load weights from checkpoint if specified
        if self.weights_path is not None:
            self.load_weights(self.weights_path)

    def forward(self, x: Tensor) -> Tensor:
        return self.model(x)

    def load_weights(self, path: str) -> None:
        """Load weights from a saved state_dict or pretraining checkpoint"""

        state_dict = torch.load(path)

        # If pretraining checkpoint, extract the CNN state_dict
        if "state_dict" in state_dict.keys():
            state = {}
            for k, v in state_dict["state_dict"].items():
                if k.startswith("backbone.model"):
                    state[k[15:]] = v  # unprefix backbone keys
            state_dict = state
        else:
            # Don't load fc weight + bias from Jiang et al. 2022 pretrained model
            state_dict.pop("fc.weight", None)
            state_dict.pop("fc.bias", None)

        self.model.load_state_dict(state_dict, strict=True)


def _sanity_check():
    """Sanity checks"""

    #
    # resnet50_headless()
    #
    print("resnet50_headless()")
    print("-------------------")
    model, out_dim = resnet50_headless()
    assert out_dim == 2048
    assert model.conv1.in_channels == 3
    print("resnet50_headless():")
    desc = str(model).split("\n")
    print("\n".join(desc[:5] + ["  ..."] + desc[-3:]))
    print("in_channels:", model.conv1.in_channels)
    print("out_dim:", out_dim)

    # Can change in_channels
    print()
    print("resnet50_headless(in_channels=4):")
    model, out_dim = resnet50_headless(in_channels=4)
    assert out_dim == 2048
    assert model.conv1.in_channels == 4
    desc = str(model).split("\n")
    print("\n".join(desc[:5] + ["  ..."] + desc[-3:]))
    print("in_channels", model.conv1.in_channels)
    print("out_dim:", out_dim)

    # Changing in_channels does not modify weights of first layer's kept channels
    print()
    print("Changing in_channels does not modify weights of first layer's kept channels")
    model, _ = resnet50_headless(weights="IMAGENET1K_V2")
    assert model.conv1.in_channels == 3
    model2, _ = resnet50_headless(in_channels=4, weights="IMAGENET1K_V2")
    assert model2.conv1.in_channels == 4
    assert torch.equal(model.conv1.weight, model2.conv1.weight[:, 0:3])

    print()
    print("HeadlessCNN")
    print("-----------")
    resnet = HeadlessCNN("resnet50", in_channels=4)
    assert isinstance(resnet.model, ResNet)
    assert resnet.out_dim == 2048
    assert resnet.model.conv1.in_channels == 4
    desc = str(resnet).split("\n")
    print("\n".join(desc[:5] + ["    ..."] + desc[-3:]))


if __name__ == "__main__":
    _sanity_check()
