"""PyTorch Lightning module to predict air quality (AQ) from satellite images"""

from typing import Optional
import warnings

from pytorch_lightning import LightningModule
from pytorch_lightning.loggers import TensorBoardLogger
import torch
from torch import nn, Tensor
from torchmetrics import (
    MetricCollection,
    MeanAbsoluteError,
    MeanSquaredError,
    RunningMean,
)

from models import HeadlessCNN


class AQMLP(nn.Module):
    """MLP to predict air quality (AQ) based on image features + extra data"""

    def __init__(
        self,
        in_dim: int = 2048,
        dim1: int = 512,
        dim2: int = 512,
        dropout: float = 0.2,
        activation: str = "ReLU",
    ) -> None:
        """
        Args:
            in_dim: Dimension of input (image features + extra data)
            dim1: Dimension of first hidden layer (default: 512)
            dim2: Dimension of second hidden layer (default: 512)
            dropout: Dropout probability MLP (default: 0.2)
            activation: Activation function (default: "ReLU")
        """

        super().__init__()

        self.in_dim = in_dim
        self.dim1 = dim1
        self.dim2 = dim2
        self.dropout = dropout
        self.activation = nn.__dict__[activation]

        self.model = nn.Sequential(
            # Input layer
            nn.Linear(self.in_dim, self.dim1),
            self.activation(inplace=True),
            nn.Dropout(self.dropout),
            # Hidden layer
            nn.Linear(self.dim1, self.dim2),
            self.activation(inplace=True),
            nn.Dropout(self.dropout),
            # Output layer
            nn.Linear(self.dim2, 1),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.model(x)


class AQLearner(LightningModule):
    """PyTorch Lightning module for predicting air quality (AQ)"""

    def __init__(
        self,
        backbone: str | None = "resnet50",
        in_channels: int = 3,
        weights: Optional[str] = None,
        backbone_train_idx: Optional[int] = None,
        n_extra: int = 0,
        dim: Optional[int] = 512,
        dim1: Optional[int] = None,
        dim2: Optional[int] = None,
        dropout: float = 0.2,
        activation: str = "ReLU",
        learning_rate: float = 0.001,
        optimizer: str = "adam",
    ) -> None:
        """
        Args:
            backbone:
                String: A CNN architecture (default: "resnet50"). A headless version of
                    the CNN will be initialized i.e. the CNN will output features
                    (representations of images) rather than predictions.
                None: The model will ignore images and use only extra data
            in_channels: Number of image channels (default: 3)
            weights: Optional name of torchvision weights to load or path to saved weights
                or pretraining checkpoint (default: None)
            backbone_train_idx: Optional index of first backbone layer to train, along
                with all subsequent layers (default: None = do not train backbone). For
                example, `backbone_train_idx=-3` trains the backbone's last 3 layers
                ([layer4, avgpool, fc] for resnet50). `backbone_train_idx=0` trains the
                entire backbone.
            n_extra: Dimension of extra data appended to the backbone's representation of
                an image when predicting (default: 0)
            dim: Dimension of predictor MLP's hidden layers (default: 512)
            dim1, dim2: Optional, dims for MLP's hidden layers (default: None, None).
                Overrides dim.
            dropout: Dropout probability for predictor MLP (default: 0.2)
            activation: Activation function for the predictor MLP (default: "ReLU")
            learning_rate: Learning rate for training (default: 0.001)
            optimizer: Optimizer for training (default: "adam")
        """

        super().__init__()
        self.save_hyperparameters()

        self.backbone = backbone
        self.in_channels = in_channels
        self.weights = weights

        if backbone_train_idx is not None and backbone_train_idx > 0:
            raise ValueError(
                f"`backbone_train_idx` must be None or <= 0; got {backbone_train_idx}"
            )
        self.backbone_train_idx = backbone_train_idx

        if backbone is None and n_extra == 0:
            raise ValueError(f"`n_extra` must be >0 if `backbone` is None; got {n_extra}")
        self.n_extra = n_extra

        self.dim1 = dim1 if dim1 is not None else dim
        self.dim2 = dim2 if dim2 is not None else dim
        self.dropout = dropout
        self.activation = activation
        self.learning_rate = learning_rate
        self.optimizer = optimizer.lower()

        # Init and freeze the backbone CNN if any
        if self.backbone is not None:
            self.backbone = HeadlessCNN(
                architecture=self.backbone,
                in_channels=self.in_channels,
                weights=self.weights,
            )
            self.freeze_backbone()

        # Define the predictor MLP
        n_features = 0 if self.backbone is None else self.backbone.out_dim
        n_features += self.n_extra
        self.predictor = AQMLP(
            in_dim=n_features,
            dim1=self.dim1,
            dim2=self.dim2,
            dropout=self.dropout,
            activation=self.activation,
        )

        # Metrics, one of which is used as loss
        # * Don't use R^2 or R b/c metrics are calculated by batch and averaged but mean
        #   batchwise R^2 != R^2 on entire dataset
        # * Postfix metrics -> show all in group on tensorboard
        self.metrics = MetricCollection(
            {"mse": MeanSquaredError(), "mae": MeanAbsoluteError()}, postfix="/train"
        )
        self.val_metrics = self.metrics.clone(postfix="/val")
        self.test_metrics = self.metrics.clone(postfix="/test")
        self.loss_metric = "mse"

        self.running_mean: RunningMean  # for logging running mean train loss

    def freeze_backbone(self) -> None:
        """Freeze the backbone, possibly excluding top layers"""

        # Freeze the backbone; also set to eval mode -> disables any BatchNorm layers as
        # they may be updated even without grad
        self.backbone.requires_grad_(False)
        self.backbone.train(False)

        # Possibly unfreeze some backbone layers
        if self.backbone_train_idx is not None:
            children = self.backbone.model.children()
            for child in list(children)[self.backbone_train_idx :]:
                child.requires_grad_(True)
                child.train(True)

    def forward(self, x: Tensor, extra: Tensor) -> Tensor:
        """Predict OP based on an image (and possibly extra data)"""

        if self.backbone is not None:
            x = self.backbone(x)  # Represent images with backbone CNN, if any
            x = torch.cat([x, extra], dim=1)  # Append extra data, if any
        else:
            x = extra

        x = self.predictor(x)  # predict with MLP

        return x.squeeze(-1)  # Return 1-dimensional tensor to match target

    def shared_step(
        self,
        images: Tensor,
        extra: Tensor,
        target: Tensor,
        idx: Tensor,
        metrics: MetricCollection,
    ) -> dict[str, Tensor]:
        """Compute loss scores"""

        pred = self.forward(images, extra)
        scores = metrics(pred, target)

        return scores

    def training_step(self, batch: tuple[Tensor, Tensor, Tensor, Tensor]) -> Tensor:
        """Training loop"""

        scores = self.shared_step(*batch, metrics=self.metrics)
        loss = scores[self.loss_metric + "/train"]

        # Log metrics
        # * train loss -> logger only, per step and epochal mean
        # * scores -> logger only, epochal mean
        # * running mean train loss -> progress bar only, per step
        self.log("train_loss", loss, on_epoch=True)
        self.log_dict(scores, on_step=False, on_epoch=True)
        with warnings.catch_warnings():
            self.running_mean.update(loss, weight=len(batch[-1]))
            self.log(
                "mean_loss", self.running_mean.compute(), prog_bar=True, logger=False
            )

        return loss

    def on_train_start(self) -> None:
        # Set up a metric to track running mean train loss over each epoch
        self.running_mean = RunningMean(
            self.trainer.progress_bar_callback.total_train_batches
        ).to(self.trainer.model.device)
        warnings.filterwarnings(
            action="ignore",
            message="The ``compute`` method.+was called before the ``update`` method",
            append=True,
        )

    def on_train_epoch_start(self) -> None:
        # Ensure backbone is in eval mode if not trainable. Need to do this at start of
        # every train epoch b/c lightning calls model.train() before every train epoch.
        if self.backbone is not None:
            self.freeze_backbone()

        # Don't display previous epoch's val loss on this epoch's progress bar
        self.trainer.progress_bar_metrics.pop("loss/val", None)

        self.running_mean.reset()

    def on_train_epoch_end(self) -> None:
        # Start next epoch's progress bar on a new line (don't overwrite this epoch's bar)
        print("")

    def validation_step(
        self, batch: tuple[Tensor, Tensor, Tensor, Tensor], batch_idx: int
    ) -> Tensor:
        """Validation loop"""

        scores = self.shared_step(*batch, metrics=self.val_metrics)
        loss = scores[self.loss_metric + "/val"]

        self.log("val_loss", loss, prog_bar=True)
        self.log_dict(scores)

        return loss

    def on_validation_epoch_end(self) -> None:
        # Log epochal mean val loss to tensorboard with epoch (not step) as x-axis
        if not self.trainer.sanity_checking and self.tb_logger is not None:
            self.tb_logger.log_metrics(
                {"epochal/val_loss": self.trainer.logged_metrics["val_loss"]},
                step=self.current_epoch,
            )

    def test_step(
        self, batch: tuple[Tensor, Tensor, Tensor, Tensor], batch_idx: int
    ) -> Tensor:
        """Test loop"""

        scores = self.shared_step(*batch, metrics=self.test_metrics)
        loss = scores[self.loss_metric + "/test"]

        self.log_dict(scores)

        return loss

    def predict_step(
        self, batch: tuple[Tensor, Tensor, Tensor, Tensor], batch_idx: int
    ) -> Tensor:
        """Predict loop"""

        images, extra, _, _ = batch
        return self.forward(images, extra)

    def configure_optimizers(self):
        """Configure optimizers"""

        params = self.parameters()

        if self.optimizer == "adam":
            optimizer = torch.optim.Adam(params=params, lr=self.learning_rate)
        elif self.optimizer == "sgd":
            optimizer = torch.optim.SGD(
                params=params, lr=self.learning_rate, momentum=0.9, weight_decay=1e-4
            )
        else:
            raise NotImplementedError(f"Unrecognized `optimizer`: {self.optimizer}")

        return [optimizer]

    @property
    def tb_logger(self) -> TensorBoardLogger | None:
        """Return the attached TensorBoardLogger, if any"""

        for logger in self.loggers:
            if isinstance(logger, TensorBoardLogger):
                return logger
        return None


def _sanity_check():
    """Sanity checks"""

    print("AQMLP")
    print("-----")
    torch.manual_seed(0)
    predictor = AQMLP()
    print(predictor)

    print()
    print("AQLearner")
    print("---------")
    torch.manual_seed(0)
    model = AQLearner()
    assert model.backbone.out_dim == 2048
    desc = str(model).split("\n")
    print("\n".join(desc[:3]), "\n" + "\n".join(["      ..."] + desc[-29:]))

    print()
    print("hparams:")
    print(model.hparams)

    # Backbone is frozen by default
    for module in model.backbone.modules():
        assert module.training is False
    for param in model.backbone.parameters():
        assert param.requires_grad is False

    # Can unfreeze entire backbone
    # backbone and backbone.model still frozen; all child modules trainable
    model = AQLearner(backbone_train_idx=0)
    assert model.backbone.training is False
    assert model.backbone.model.training is False
    children = list(model.backbone.model.children())
    for child in children:
        for module in child.modules():
            assert module.training is True
        for param in child.parameters():
            assert param.requires_grad is True

    # Can unfreeze only top layers of backbone
    model = AQLearner(backbone_train_idx=-3)
    assert model.backbone.training is False
    assert model.backbone.model.training is False
    children = list(model.backbone.model.children())
    for child in children[:-3]:
        for module in child.modules():
            assert module.training is False
        for param in child.parameters():
            assert param.requires_grad is False
    for child in children[-3:]:
        for module in child.modules():
            assert module.training is True
        for param in child.parameters():
            assert param.requires_grad is True
    del children

    # Raises if backbone_train_idx > 0
    try:
        AQLearner(backbone_train_idx=1)
    except ValueError as err:
        if "must be None or <= 0" not in str(err):
            raise err
    else:
        raise AssertionError

    # Batch of 2 4x96x96 images, 2 extra features per sample, OP, and index
    torch.manual_seed(0)
    views, extra, targets, idx = (
        torch.rand(2, 3, 96, 96),
        torch.rand(2, 2),
        torch.rand(2),
        torch.arange(2),
    )
    print()
    print("batch:", views.shape, extra.shape, targets.shape, idx.shape)

    # Features (represented images)
    model = AQLearner()
    reps = model.backbone(views)
    print()
    print("represented images:", reps.shape)
    assert reps.shape == (2, 2048)

    # Prediction
    preds = model.predictor(reps).detach()
    print()
    print("prediction:", preds)
    assert preds.shape == (2, 1)

    # forward() represents, adds extra data, predicts, and unsqueezes to 1-dimensional
    # * For image only
    empty = torch.empty(0)
    assert model(views, extra=empty).shape == (2,)
    # * For image and extra
    model2 = AQLearner(n_extra=2)
    assert model2(views, extra).shape == (2,)
    # * For extra only
    model3 = AQLearner(backbone=None, n_extra=2)
    assert model3(empty, extra).shape == (2,)

    # shared_step() returns loss and predictions
    result = model.shared_step(views, empty, targets, idx, metrics=model.metrics)
    print()
    print("shared_step(views, empty, target, idx):")
    print("metrics:", {k: v.detach().round(decimals=3) for k, v in result.items()})

    # shared_step() for images and extra
    result2 = model2.shared_step(views, extra, targets, idx, metrics=model.metrics)
    print()
    print("shared_step(views, extra, target, idx):")
    print("metrics:", {k: v.detach().round(decimals=3) for k, v in result2.items()})

    # shared_step() for extra only
    result3 = model3.shared_step(empty, extra, targets, idx, metrics=model.metrics)
    print()
    print("shared_step(empty, extra, target, idx):")
    print("metrics:", {k: v.detach().round(decimals=3) for k, v in result3.items()})

    # It works for singleton batch
    views, extra, targets, idx = (
        torch.rand(1, 3, 96, 96),
        torch.rand(1, 2),
        torch.rand(1),
        torch.arange(1),
    )
    # * For image only
    assert model(views, empty).shape == (1,)
    # * For image and extra
    assert model2(views, extra).shape == (1,)
    # * For extra only
    assert model3(empty, extra).shape == (1,)

    # Don't test train / val / test / predict steps b/c they are hard to run without a
    # Trainer due to logging


if __name__ == "__main__":
    _sanity_check()
