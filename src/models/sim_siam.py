"""PyTorch Lightning module for SimSiam contrastive learning"""

from typing import Optional
import warnings

from pytorch_lightning import LightningModule
from pytorch_lightning.loggers import TensorBoardLogger
import torch
from torch import nn, Tensor
from torchmetrics import RunningMean

from models import HeadlessCNN


class SimSiamProjector(nn.Module):
    """SimSiam projector MLP: projector(backbone(view)) -> encoding

    * Three-layer MLP with batch normalization of each layer
    * First two layers have same dimension as output of backbone
    * Disable output layer's bias gradient b/c it gets batch normalized
    """

    def __init__(
        self, in_dim: int = 2048, out_dim: int = 2048, activation: str = "ReLU"
    ) -> None:
        """
        Args:
            in_dim: Input and hidden layer dimension (default: 2048)
            out_dim: Output dimension (default: 2048)
            activation: Activation function (default: "ReLU")
        """

        super().__init__()

        self.in_dim = in_dim
        self.out_dim = out_dim
        self.activation = nn.__dict__[activation]

        self.model = nn.Sequential(
            # Input layer
            nn.Linear(self.in_dim, self.in_dim, bias=False),
            nn.BatchNorm1d(self.in_dim),
            self.activation(inplace=True),
            # Hidden layer
            nn.Linear(self.in_dim, self.in_dim, bias=False),
            nn.BatchNorm1d(self.in_dim),
            self.activation(inplace=True),
            # Output layer
            nn.Linear(self.in_dim, self.out_dim, bias=True),
            nn.BatchNorm1d(self.out_dim, affine=False),
        )
        # Disable output layer's bias gradient b/c it gets batch normalized
        self.model[-2].bias.requires_grad = False

    def forward(self, x: Tensor) -> Tensor:
        return self.model(x)


class SimSiamPredictor(nn.Module):
    """SimSiam predictor MLP: predictor(encoding) -> prediction

    * Two-layer MLP, input layer has no bias
    * Hidden layer batch normalized, recommended dimension is in_dim / 4 (bottleneck)
    * Output layer is not batch normalized
    """

    def __init__(
        self,
        in_dim: int = 2048,
        hidden_dim: int = 512,
        out_dim: int = 2048,
        activation: str = "ReLU",
    ) -> None:
        """
        Args:
            in_dim: Input dimension (default: 2048)
            hidden_dim: Hidden layer dimension (default: 512). SimSiam paper recommends
                bottleneck of in_dim / 4.
            out_dim: Output dimension (default: 2048)
            activation: Activation function (default: "ReLU")
        """

        super().__init__()

        self.in_dim = in_dim
        self.hidden_dim = hidden_dim
        self.out_dim = out_dim
        self.activation = nn.__dict__[activation]

        self.model = nn.Sequential(
            # Input layer
            nn.Linear(self.in_dim, self.hidden_dim, bias=False),
            nn.BatchNorm1d(self.hidden_dim),
            self.activation(inplace=True),
            # Output layer
            nn.Linear(self.hidden_dim, self.out_dim),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.model(x)


class SimSiam(LightningModule):
    """PyTorch Lightning module for SimSiam

    SimSiam is a method for unsupervised contrastive representational learning
    using a siamese network. Each of the network's arms constists of an encoder
    `f`, which is a backbone CNN with an MLP head, and a predictor MLP `h`. Two
    randomly augmented views of a single image are encoded (one by each arm) and
    one arm's predictor is applied while the other arm receives a stop gradient.
    The arms are then switched and the predictor and stop gradient are applied
    again. The objective is to maximize the similarity of the the two arms'
    output i.e. `h(f(x1))` should be similar to `stopgrad(f(x2))` and
    `stopgrad(f(x1))` should be similar to `h(f(x2))`.

    Reference:
        Chen, X., & He, K. (2021). Exploring simple Siamese representation
        learning. Proceedings of the IEEE/CVF Conference on Computer Vision and
        Pattern Recognition, 15745-15753.
        https://doi.org/10.1109/CVPR46437.2021.01549

    Code based on reference implementation:
        https://github.com/facebookresearch/simsiam
    """

    def __init__(
        self,
        backbone: str = "resnet50",
        in_channels: int = 3,
        weights: Optional[str] = None,
        out_dim: int = 2048,
        hidden_dim: int = 512,
        activation: str = "ReLU",
        learning_rate: float = 1e-3,
        optimizer: str = "adam",
        cosine_decay: bool = True,
        cosine_epochs: int = 100,
        check_encoder: bool = False,
    ) -> None:
        """
        Args:
            backbone: String identifying a CNN (default: "resnet50"). A headless version
                of the CNN will be initialized i.e. the CNN will output features
                (representations of images) rather than predictions.
            in_channels: The number of channels per image (default: 3).
            weights: Optional string identifying pretrained weights to use for the
                backbone (default: None)
            out_dim: Output dimension of projector and predictor (default: 2048)
            hidden_dim: Dimension of predictor's hidden layer (default: 512). SimSiam
                paper recommends setting hidden_dim = out_dim / 4.
            activation: Activation function for the projector and predictor MLPs
                (default: "ReLU")
            learning_rate: Learning rate for training (default: 1e-3). SimSiam paper used
                0.05 * batch_size / 256.
            optimizer: Optimizer for training (default: "adam"). Set to "sgd" to use SGD
                with momentum 0.9 and weight decay 1e-4 as in SimSiam paper.
            cosine_decay: Whether to cosine anneal the learning rate (default: True)
            cosine_epochs: Period for cosine annealing if used (default: 100)
            check_encoder: Whether to check if the encoder has collapsed i.e. gives same
                output for every image (default: False). If true, computes and logs
                channel-wise standard deviation across each batch's encodings.
        """

        super().__init__()
        self.save_hyperparameters()

        self.in_channels = in_channels
        self.weights = weights
        self.out_dim = out_dim
        self.hidden_dim = hidden_dim
        self.activation = activation
        self.learning_rate = learning_rate
        self.optimizer = optimizer
        self.cosine_decay = cosine_decay
        self.cosine_epochs = cosine_epochs
        self.check_encoder = check_encoder

        # -------
        # Encoder
        # -------
        # The encoder consists of a headless "backbone" CNN whose output is fed into a
        # 3-layer MLP projector "head". Once the backbone has been pretrained, the
        # projector can be replaced by a different network (e.g. a linear classifier) that
        # is then trained via supervised learning. This workflow (unsupervised pretraining
        # of a backbone [representational learning] followed by supervised training of a
        # projector) is a form of semi-supervised learning.

        # Backbone CNN (headless) -> "features" / "representation" / "represented view"
        self.backbone = HeadlessCNN(
            architecture=backbone, in_channels=self.in_channels, weights=self.weights
        )

        # Projector head -> "encoding" / "encoded view"
        self.projector = SimSiamProjector(
            in_dim=self.backbone.out_dim, out_dim=self.out_dim, activation=self.activation
        )

        # ---------
        # Predictor
        # ---------
        # The predictor is a two-layer MLP that predicts an encoded view of an image. The
        # prediction is then compared to a different encoded view of the same image. The
        # original SimSiam paper found that the predictor prevents the network from
        # "collapsing" to constant output; results were most robust when the predictor's
        # first layer was a bottleneck of dimension out_dim / 4.

        # Predictor -> "prediction" / "predicted view"
        self.predictor = SimSiamPredictor(
            in_dim=self.out_dim, hidden_dim=self.hidden_dim, out_dim=self.out_dim
        )

        # -------------------
        # Training parameters
        # -------------------
        self.loss = nn.CosineSimilarity(dim=1)
        self.learning_rate = learning_rate
        self.optimizer = optimizer.lower()

        self.running_mean: RunningMean  # for logging running mean train loss

    def encode(self, x: Tensor) -> Tensor:
        """Encode i.e. apply backbone and projector"""

        x = self.backbone(x)
        x = self.projector(x)
        return x

    def encode_and_predict(
        self, x1: Tensor, x2: Tensor
    ) -> tuple[Tensor, Tensor, Tensor, Tensor]:
        """Encode and predict a pair of views"""

        # Encode (backbone + projector)
        z1 = self.encode(x1)
        z2 = self.encode(x2)

        # Predict
        p1 = self.predictor(z1)
        p2 = self.predictor(z2)

        # Apply stop gradient to encodings
        return p1, p2, z1.detach(), z2.detach()

    def compute_loss(self, p1: Tensor, p2: Tensor, z1: Tensor, z2: Tensor) -> Tensor:
        """Compute symmetric loss between both arms' encodings + predictions"""

        # Pairwise losses; encodings should already be detached
        loss_1_2 = self.loss(p1, z2).mean()
        loss_2_1 = self.loss(p2, z1).mean()

        # Negative symmetric loss = negative average of pairwise losses
        return -0.5 * (loss_1_2 + loss_2_1)

    def compute_sd(self, x: Tensor) -> Tensor:
        """Compute channel-wise standard deviation of an encoding

        Used to check whether the output collapses to a constant vector. If the
        output collapses, each channel of the encoding will have an SD of zero
        (after batch-normalizing by dividing each sample by its l2-norm).
        Conversely, if each channel is zero-mean isotropic Gaussian, then the SD
        will approach 1 / sqrt(out_dim) ~= 0.0221 for out_dim = 2048.
        """

        # Batch-normalize by dividing each encoded view by its l2-norm
        x = x / x.norm(dim=1, keepdim=True)

        # Compute average of channel-wise standard deviation
        return x.std(dim=0).mean()

    def shared_step(
        self, view1: Tensor, view2: Tensor, idx: Tensor, check_encoder: bool = False
    ) -> dict[str, Tensor]:
        """Encode and predict a pair of views and calculate loss"""

        # Encode and predict
        p1, p2, z1, z2 = self.encode_and_predict(view1, view2)

        # Calculate loss
        loss = self.compute_loss(p1, p2, z1, z2)
        result = {"loss": loss}

        # Possibly sanity-check the encoder
        if check_encoder:
            result["z1_sd"] = self.compute_sd(z1)
            result["z2_sd"] = self.compute_sd(z2)

        return result

    def training_step(self, batch: tuple[Tensor, Tensor, Tensor]) -> Tensor:
        """Training loop"""

        # Run and compute loss; possibly sanity-check the encoder
        train_dict = self.shared_step(*batch, check_encoder=self.check_encoder)
        loss = train_dict.pop("loss")

        # Log metrics
        # * train loss -> logger only, per step and epochal mean
        # * encoder check metrics -> logger only, per step
        # * running mean train loss -> progress bar only, per step
        self.log("train_loss", loss, on_epoch=True)
        if self.check_encoder:
            self.log_dict(train_dict)
        with warnings.catch_warnings():
            self.running_mean.update(loss, weight=len(batch[-1]))
            self.log(
                "mean_loss", self.running_mean.compute(), prog_bar=True, logger=False
            )

        return loss

    def on_train_start(self) -> None:
        # Set up a metric to track running mean train loss over each epoch
        self.running_mean = RunningMean(
            self.trainer.progress_bar_callback.total_train_batches
        ).to(self.trainer.model.device)
        warnings.filterwarnings(
            action="ignore",
            message="The ``compute`` method.+was called before the ``update`` method",
            append=True,
        )

    def on_train_epoch_start(self) -> None:
        # Don't display previous epoch's val loss on this epoch's train progress bar
        self.trainer.progress_bar_metrics.pop("val_loss", None)

        self.running_mean.reset()

    def on_train_epoch_end(self) -> None:
        # Start next epoch's progress bar on a new line (don't overwrite this epoch's bar)
        print("")

    def validation_step(
        self, batch: tuple[Tensor, Tensor, Tensor], batch_idx: int
    ) -> Tensor:
        """Validation loop"""

        # Run and compute loss
        val_dict = self.shared_step(*batch, check_encoder=False)
        loss = val_dict.pop("loss")

        # Log metrics to loggers and progress bar
        self.log("val_loss", loss, prog_bar=True)

        return loss

    def on_validation_epoch_end(self) -> None:
        # If logging to tensorboard
        # * Log epochal mean val loss with epoch (not step) as x-axis
        if not self.trainer.sanity_checking and self.tb_logger is not None:
            self.tb_logger.log_metrics(
                {"epochal/val_loss": self.trainer.logged_metrics["val_loss"]},
                step=self.current_epoch,
            )

    def configure_optimizers(self):
        """Configure optimizers"""

        params = self.parameters()

        if self.optimizer == "adam":
            optimizer = torch.optim.Adam(params=params, lr=self.learning_rate)
        elif self.optimizer == "sgd":
            optimizer = torch.optim.SGD(
                params=params, lr=self.learning_rate, momentum=0.9, weight_decay=1e-4
            )
        else:
            raise NotImplementedError(f"Unrecognized `optimizer`: {self.optimizer}")

        if self.cosine_decay:
            scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
                optimizer=optimizer, T_max=self.cosine_epochs
            )
            return [optimizer], [scheduler]

        return [optimizer]

    @property
    def tb_logger(self) -> TensorBoardLogger | None:
        """Return the attached TensorBoardLogger, if any"""

        for logger in self.loggers:
            if isinstance(logger, TensorBoardLogger):
                return logger
        return None


def _sanity_check():
    """Sanity checks"""

    print("SimSiamProjector")
    print("----------------")
    projector = SimSiamProjector(in_dim=512, out_dim=256)
    print(projector)

    print()
    print("SimSiamPredictor")
    print("----------------")
    predictor = SimSiamPredictor(in_dim=512, hidden_dim=128, out_dim=512)
    print(predictor)

    print()
    print("SimSiam")
    print("-------")
    model = SimSiam(in_channels=3, out_dim=256, hidden_dim=64)
    desc = str(model).split("\n")
    # print("\n".join(desc[:8] + ["      ..."] + desc[-26:]))
    print(
        "\n".join(desc[:7]),
        "\n" + desc[7] + " ... )",
        "\n" + desc[40] + " ... )",
        "\n" + desc[82] + " ... )",
        "\n" + desc[142] + " ... )",
        "\n" + "\n".join(desc[175:]),
    )

    print()
    print("hparams:")
    print(model.hparams)

    # Batch of 2 3x96x96 views + idx
    torch.manual_seed(0)
    view1, view2, idx = (torch.randn(2, 3, 96, 96), torch.randn(2, 3, 96, 96), range(2))
    print()
    print("batch:", [view1.shape, view2.shape, idx])

    # Representation (features)
    rep = model.backbone(view1)
    assert rep.shape == (2, 2048)
    print()
    print("representation:", rep.shape)

    # Encoding
    z1 = model.projector(rep)
    assert z1.shape == (2, 256)
    print()
    print("encoding:", z1.shape)
    assert torch.equal(z1, model.encode(view1))

    # Prediction
    p1 = model.predictor(z1)
    assert p1.shape == (2, 256)
    print()
    print("prediction:", p1.shape)
    assert torch.equal(p1, model.encode_and_predict(view1, view2)[0])

    # Run and calculate loss
    shared_output = model.shared_step(view1, view2, idx)
    print()
    print("shared step: ", shared_output)

    # Don't test train/val steps b/c they are hard to run without a Trainer due to logging


if __name__ == "__main__":
    _sanity_check()
