"""Test util.meteo functions"""

import numpy as np
from pytest import approx

from utils.meteo import wind_components, wind_direction, wind_speed


def test_wind_components():
    assert wind_components(ws=2, wd=0) == approx((0, -2))  # from north
    assert wind_components(ws=2, wd=90) == approx((-2, 0))  # from east
    assert wind_components(ws=2, wd=180) == approx((0, 2))  # from south
    assert wind_components(ws=2, wd=270) == approx((2, 0))  # from west
    assert wind_components(ws=2, wd=270) == approx((2, 0))  # from west

    speed = np.sqrt(2)
    assert wind_components(ws=speed, wd=45) == approx((-1, -1))  # from northeast
    assert wind_components(ws=speed, wd=135) == approx((-1, 1))  # from southeast
    assert wind_components(ws=speed, wd=225) == approx((1, 1))  # from southwest
    assert wind_components(ws=speed, wd=315) == approx((1, -1))  # from northwest


def test_wind_direction():
    assert wind_direction(u=0, v=0) == 270  # no wind is treated as wind from west
    assert wind_direction(u=1, v=0) == 270
    assert wind_direction(u=1, v=1) == 225
    assert wind_direction(u=5, v=5) == 225
    assert wind_direction(u=0, v=1) == 180
    assert wind_direction(u=-1, v=1) == 135
    assert wind_direction(u=-1, v=0) == 90
    assert wind_direction(u=-1, v=-1) == 45
    assert wind_direction(u=0, v=-1) == 0
    assert wind_direction(u=1, v=-1) == 315


def test_wind_speed():
    assert wind_speed(u=1, v=0) == 1
    assert wind_speed(u=5, v=0) == 5
    assert wind_speed(u=0, v=1) == 1
    assert wind_speed(u=1, v=1) == np.sqrt(2)
    assert wind_speed(u=-1, v=1) == np.sqrt(2)
    assert wind_speed(u=-1, v=-1) == np.sqrt(2)
