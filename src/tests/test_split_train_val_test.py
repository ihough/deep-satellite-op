"""Test util split_train_val_test()"""

from pandas import DataFrame
from pytest import raises

from utils import split_train_val_test


def test_split_train_val_test():
    # Data is 100 samples with 20 groups (5 samples per group)
    samples = list(range(100))
    groups = sorted(5 * list(range(20)))
    data = DataFrame(dict(sample=samples, group=groups))
    grps = data["group"]  # Series to allow arbitrary indexing

    # Sets do not overlap
    train, val, test = split_train_val_test(data, test_frac=0.11, val_frac=0.08)
    assert not any(set(test).intersection(val))
    assert not any(set(test).intersection(train))
    assert not any(set(val).intersection(train))

    # Takes test from end of dataset; takes val from before test
    assert (train, val, test) == (samples[:-19], samples[-19:-11], samples[-11:])

    # Keeps grouped samples together
    train, val, test = split_train_val_test(
        data, test_frac=0.11, val_frac=0.08, groups=groups
    )
    assert (train, val, test) == (samples[:-25], samples[-25:-15], samples[-15:])
    assert not any(set(grps[test]).intersection(grps[val]))
    assert not any(set(grps[test]).intersection(grps[train]))
    assert not any(set(grps[val]).intersection(grps[train]))

    # Groups can be a column name
    splits = split_train_val_test(data, test_frac=0.11, val_frac=0.08, groups="group")
    assert splits == (samples[:-25], samples[-25:-15], samples[-15:])

    # Can shuffle samples before splitting
    expect_test = [9, 21, 36, 44, 47, 64, 67, 70, 83, 87, 96]
    expect_val = [4, 18, 40, 62, 65, 79, 84, 95]
    train, val, test = split_train_val_test(
        data, test_frac=0.11, val_frac=0.08, shuffle=True, rand_seed=0
    )
    assert (sorted(val), sorted(test)) == (expect_val, expect_test)
    assert not any(set(test).intersection(val))
    assert not any(set(test).intersection(train))
    assert not any(set(val).intersection(train))

    # Can shuffle groups before splitting
    expect_test = [0, 1, 2, 3, 4, 60, 61, 62, 63, 64, 75, 76, 77, 78, 79]
    expect_val = [45, 46, 47, 48, 49, 55, 56, 57, 58, 59]
    train, val, test = split_train_val_test(
        data, test_frac=0.11, val_frac=0.08, groups="group", shuffle=True, rand_seed=0
    )
    assert (sorted(val), sorted(test)) == (expect_val, expect_test)
    assert not any(set(grps[test]).intersection(grps[val]))
    assert not any(set(grps[test]).intersection(grps[train]))
    assert not any(set(grps[val]).intersection(grps[train]))

    # Works if data is Series
    splits = split_train_val_test(data["sample"], test_frac=0.11, val_frac=0.08)
    assert splits == (samples[:-19], samples[-19:-11], samples[-11:])

    # Series with grouping
    splits = split_train_val_test(
        data["sample"], test_frac=0.11, val_frac=0.08, groups=groups
    )
    assert splits == (samples[:-25], samples[-25:-15], samples[-15:])

    # Series with shuffle
    expect_test = [9, 21, 36, 44, 47, 64, 67, 70, 83, 87, 96]
    expect_val = [4, 18, 40, 62, 65, 79, 84, 95]
    train, val, test = split_train_val_test(
        data["sample"], test_frac=0.11, val_frac=0.08, shuffle=True, rand_seed=0
    )
    assert (sorted(val), sorted(test)) == (expect_val, expect_test)
    assert not any(set(test).intersection(val))
    assert not any(set(test).intersection(train))
    assert not any(set(val).intersection(train))

    # Series with grouping and shuffle
    expect_test = [0, 1, 2, 3, 4, 60, 61, 62, 63, 64, 75, 76, 77, 78, 79]
    expect_val = [45, 46, 47, 48, 49, 55, 56, 57, 58, 59]
    train, val, test = split_train_val_test(
        data["sample"],
        test_frac=0.11,
        val_frac=0.08,
        groups=groups,
        shuffle=True,
        rand_seed=0,
    )
    assert (sorted(val), sorted(test)) == (expect_val, expect_test)
    assert not any(set(grps[test]).intersection(grps[val]))
    assert not any(set(grps[test]).intersection(grps[train]))
    assert not any(set(grps[val]).intersection(grps[train]))

    # Works if data's index != position of each row
    # The returned lists identify the **position** (iloc) of the train and test samples
    # **not** their "index" (loc).
    data2 = data.set_index(data.sample(frac=1, random_state=0).index)
    expect_test = [0, 1, 2, 3, 4, 60, 61, 62, 63, 64, 75, 76, 77, 78, 79]
    expect_val = [45, 46, 47, 48, 49, 55, 56, 57, 58, 59]
    train, val, test = split_train_val_test(
        data2, test_frac=0.11, val_frac=0.08, groups="group", shuffle=True, rand_seed=0
    )
    assert (sorted(val), sorted(test)) == (expect_val, expect_test)
    assert test != data2.index[test].to_list()
    assert val != data2.index[val].to_list()

    # Works if groups are not numeric
    groups2 = sorted(list(5 * "abcdefghijklmnopqrst"))
    splits = split_train_val_test(data, test_frac=0.11, val_frac=0.08, groups=groups2)
    assert splits == (samples[:-25], samples[-25:-15], samples[-15:])

    # Raises if val_frac is invalid
    with raises(ValueError, match="`val_frac` must be in range \\[0, 1\\); got"):
        split_train_val_test(data, test_frac=0.11, val_frac=-0.08)
    with raises(ValueError, match="`val_frac` must be in range \\[0, 1\\); got"):
        split_train_val_test(data, test_frac=0.11, val_frac=1.0)

    # Allows val_frac = 0
    expect_test = [9, 21, 36, 44, 47, 64, 67, 70, 83, 87, 96]
    expect_val = []
    train, val, test = split_train_val_test(
        data, test_frac=0.11, val_frac=0, shuffle=True, rand_seed=0
    )
    assert (sorted(val), sorted(test)) == (expect_val, expect_test)

    # Allows test_frac = 0
    expect_test = []
    expect_val = [9, 21, 36, 44, 47, 64, 67, 70, 83, 87, 96]
    train, val, test = split_train_val_test(
        data, test_frac=0, val_frac=0.11, shuffle=True, rand_seed=0
    )
    assert (sorted(val), sorted(test)) == (expect_val, expect_test)

    # Allows val_frac and test_frac = 0
    splits = split_train_val_test(data, test_frac=0, val_frac=0)
    assert splits == (samples, [], [])

    # Raises if test_frac + val_frac is invalid
    with raises(ValueError, match="`test_frac` \\+ `val_frac` must be < 1; got"):
        split_train_val_test(data, test_frac=0.5, val_frac=0.5)


if __name__ == "__main__":
    test_split_train_val_test()
