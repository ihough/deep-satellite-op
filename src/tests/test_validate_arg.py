"""Test util validate_arg()"""

from pytest import raises

from utils import validate_arg


def test_validate_arg():
    # Passes if arg in allowed
    allowed = {"a", "b", "c"}
    assert validate_arg("a", allowed, arg_name="test") == "a"
    assert validate_arg(["b", "a"], allowed, arg_name="test") == ["b", "a"]

    # Raises if arg not in allowed
    with raises(ValueError) as excinfo:
        validate_arg("d", allowed, arg_name="test")
    assert str(excinfo.value) == f"Invalid `test`: d is not in {allowed}"

    # Raises if any arg not in allowed
    with raises(ValueError) as excinfo:
        validate_arg(["a", "d"], allowed, arg_name="test")
    assert str(excinfo.value) == f"Invalid `test`: d is not in {allowed}"

    # Arg can be integer
    allowed = {1, 2, 3}
    assert validate_arg(1, allowed, arg_name="test") == 1
    with raises(ValueError) as excinfo:
        validate_arg([1, 4], allowed, arg_name="test")
    assert str(excinfo.value) == f"Invalid `test`: 4 is not in {allowed}"

    # Allowed can be list
    allowed = [1, 2, 3]
    assert validate_arg([1, 2], allowed, arg_name="test") == [1, 2]
    with raises(ValueError) as excinfo:
        validate_arg(4, allowed, arg_name="test")
    assert str(excinfo.value) == f"Invalid `test`: 4 is not in {allowed}"

    # Allowed can be dict keys
    allowed = dict(a=1, b=2, c=3)
    assert validate_arg("a", allowed.keys(), "test") == "a"
    with raises(ValueError) as excinfo:
        validate_arg(1, allowed.keys(), arg_name="test")
    assert str(excinfo.value) == f"Invalid `test`: 1 is not in {allowed.keys()}"

    # Allowed can be dict values
    assert validate_arg(1, allowed.values(), "test") == 1
    with raises(ValueError) as excinfo:
        validate_arg("a", allowed.values(), arg_name="test")
    assert str(excinfo.value) == f"Invalid `test`: a is not in {allowed.values()}"

    # as_list = True ensures arg is returned as list
    assert validate_arg("a", allowed.keys(), arg_name="test", as_list=True) == ["a"]
