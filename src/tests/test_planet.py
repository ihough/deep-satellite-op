"""Test datasets.planet_datasets functions"""

import numpy as np
from pytest import approx, raises
import torch

from utils.planet import (
    normalize_min_max,
    planet_check_dimension,
    planet_check_shape,
    planet_get_channels,
    planet_reshape,
    planet_to_tensor,
)


def test_normalize_min_max():
    tensor = torch.linspace(50, 9950, 4 * 5 * 5).reshape(4, 5, 5)

    # value_range = None (default) clamps by min, max of tensor and shifts
    normed = normalize_min_max(tensor)
    assert normed.amin((1, 2)) == approx([0.0, 0.2525252, 0.5050505, 0.7575757])
    assert normed.amax((1, 2)) == approx([0.2424242, 0.4949494, 0.7474747, 1.0])
    assert normed.mean((1, 2)) == approx([0.121212, 0.373737, 0.626262, 0.878787])

    # value_range = (min, max) clamps by min, max
    normed = normalize_min_max(tensor, value_range=(0, 10000))
    assert normed.amin((1, 2)) == approx([0.005, 0.255, 0.505, 0.755])
    assert normed.amax((1, 2)) == approx([0.245, 0.495, 0.745, 0.995])
    assert normed.mean((1, 2)) == approx([0.125, 0.375, 0.625, 0.875])

    # Works for batch of images
    batch = torch.stack([tensor[:2], tensor[-2:]])
    normed = normalize_min_max(batch, value_range=(0, 10000))
    assert normed.amin((2, 3)) == approx(torch.tensor([[0.005, 0.255], [0.505, 0.755]]))
    assert normed.amax((2, 3)) == approx(torch.tensor([[0.245, 0.495], [0.745, 0.995]]))
    assert normed.mean((2, 3)) == approx(torch.tensor([[0.125, 0.375], [0.625, 0.875]]))

    # Raises if tensor is not torch.Tensor
    with raises(TypeError, match=r"`tensor` must be a torch.Tensor; got"):
        normalize_min_max(tensor.numpy())

    # Raises if value_range is not tuple
    with raises(
        TypeError, match=r"`value_range` must be a tuple \(min, max\); got \[0, 10000\]"
    ):
        normalize_min_max(tensor, value_range=[0, 10000])


def test_planet_check_dimension():
    # Passes if 3 or 4 dimensions
    img = np.random.randint(0, 100, size=(4, 9, 9))
    assert planet_check_dimension(img) is None
    batch = np.random.randint(0, 100, size=(2, 4, 9, 9))
    assert planet_check_dimension(batch) is None

    # Raises if < 3 dimensions
    bad = np.random.randint(0, 100, size=(9, 9))
    with raises(ValueError, match=r"`data` must have 3 or 4 dimensions; got 2"):
        planet_check_dimension(bad)

    # Raises if > 4 dimensions
    bad2 = np.random.randint(0, 100, size=(1, 2, 4, 9, 9))
    with raises(ValueError, match=r"`data` must have 3 or 4 dimensions; got 5"):
        planet_check_dimension(bad2)

    # Works with tensor data
    img_t = torch.from_numpy(img)
    assert planet_check_dimension(img_t) is None
    batch_t = torch.from_numpy(batch)
    assert planet_check_dimension(batch_t) is None
    bad_t = torch.from_numpy(bad)
    with raises(ValueError, match=r"`data` must have 3 or 4 dimensions; got 2"):
        planet_check_dimension(bad_t)
    bad2_t = torch.from_numpy(bad2)
    with raises(ValueError, match=r"`data` must have 3 or 4 dimensions; got 5"):
        planet_check_dimension(bad2_t)


def test_planet_check_shape():
    # Channels x Height x Width is valid
    img = np.random.randint(0, 100, size=(4, 9, 9))
    assert planet_check_shape(img) is None

    # Images x Channels x Height x Width is valid
    batch = np.random.randint(0, 100, size=(2, 4, 9, 9))
    assert planet_check_shape(batch) is None

    # Checks number of dimensions
    with raises(ValueError, match=r"`data` must have 3 or 4 dimensions; got 2"):
        planet_check_shape(np.random.randint(0, 100, size=(9, 9)))

    # Raises if > 4 dimensions
    with raises(ValueError, match=r"`data` must have 3 or 4 dimensions; got 5"):
        planet_check_shape(np.random.randint(0, 100, size=(1, 2, 4, 9, 9)))

    # Raises if channel dimension has > 4 channels (i.e. data has been reshaped)
    bad = np.random.randint(0, 100, size=(9, 9, 4))
    with raises(
        ValueError, match=r"`data` has 9 channels \(> 4\); has it been reshaped\\?"
    ):
        planet_check_shape(bad)

    bad_batch = np.random.randint(0, 100, size=(2, 9, 9, 4))
    with raises(
        ValueError, match=r"`data` has 9 channels \(> 4\); has it been reshaped\\?"
    ):
        planet_check_shape(bad_batch)

    # Works with tensor data
    assert planet_check_shape(torch.from_numpy(img)) is None
    assert planet_check_shape(torch.from_numpy(batch)) is None
    with raises(
        ValueError, match=r"`data` has 9 channels \(> 4\); has it been reshaped\\?"
    ):
        planet_check_shape(torch.from_numpy(bad))
    with raises(
        ValueError, match=r"`data` has 9 channels \(> 4\); has it been reshaped\\?"
    ):
        planet_check_shape(torch.from_numpy(bad_batch))


def test_planet_get_channels():
    toar = np.random.randint(0, 100, size=(4, 9, 9))
    blue, green, red, infrared = [toar[[i]] for i in range(4)]
    rgb = toar[[2, 1, 0]]

    # Works for string channel specs
    assert np.array_equal(
        planet_get_channels(toar, "RGB", "TOAR"), np.vstack([red, green, blue])
    )
    assert np.array_equal(
        planet_get_channels(toar, "ir", "TOAR"), np.vstack([infrared, red])
    )
    assert np.array_equal(planet_get_channels(toar, "G", "TOAR"), green)
    assert np.array_equal(
        planet_get_channels(rgb, "RGB", "RGB"), np.vstack([red, green, blue])
    )
    assert np.array_equal(planet_get_channels(rgb, "bg", "RGB"), np.vstack([blue, green]))
    assert np.array_equal(planet_get_channels(rgb, "G", "RGB"), green)

    # Works for list or integer channel spec
    assert np.array_equal(
        planet_get_channels(toar, [2, 1, 0], "TOAR"), np.vstack([red, green, blue])
    )
    assert np.array_equal(
        planet_get_channels(toar, [3, 2], "TOAR"), np.vstack([infrared, red])
    )
    assert np.array_equal(planet_get_channels(toar, 0, "TOAR"), blue)
    assert np.array_equal(
        planet_get_channels(rgb, [2, 1, 0], "RGB"), np.vstack([blue, green, red])
    )
    assert np.array_equal(
        planet_get_channels(rgb, [2, 1], "RGB"), np.vstack([blue, green])
    )
    assert np.array_equal(planet_get_channels(rgb, 0, "RGB"), red)

    # Works for batch of images
    toar2 = np.random.randint(0, 100, size=(4, 9, 9))
    blue2, green2, red2, _ = [toar2[[i]] for i in range(4)]
    batch = np.stack([toar, toar2])
    assert np.array_equal(
        planet_get_channels(batch, "RGB", "TOAR"),
        np.stack([np.vstack([red, green, blue]), np.vstack([red2, green2, blue2])]),
    )
    assert np.array_equal(planet_get_channels(batch, 0, "TOAR"), np.stack([blue, blue2]))

    # Raises if channel spec unrecognized
    with raises(ValueError, match=r"Unrecognized `channels`: range\(0, 4\)"):
        planet_get_channels(toar, range(4), "TOAR")
    with raises(ValueError, match=r"Invalid `channels`: F is not in BGRI"):
        planet_get_channels(toar, "foo", "TOAR")
    with raises(ValueError, match=r"Invalid `channels`: 5 is not in \[0, 1, 2, 3\]"):
        planet_get_channels(toar, 5, "TOAR")

    # Raises if data has invalid shape
    toar3 = np.random.randint(0, 100, size=(9, 9, 4))
    with raises(
        ValueError, match=r"`data` has 9 channels \(> 4\); has it been reshaped?"
    ):
        planet_get_channels(toar3, "RGB", "TOAR")

    # Works for tensor data
    batch_t = torch.from_numpy(batch)
    assert torch.equal(
        planet_get_channels(batch_t, "RGB", "TOAR"),
        torch.stack([i[[2, 1, 0]] for i in batch_t]),
    )


def test_planet_reshape():
    img = np.random.randint(0, 100, size=(3, 4, 5))
    blue, green, red = list(img)

    # Works for string shape spec
    hwc = np.dstack([blue, green, red])
    assert np.array_equal(planet_reshape(img, "HWC"), hwc)
    assert planet_reshape(img, "HWC").shape == (4, 5, 3)

    # Works for list[int] shape spec
    assert np.array_equal(planet_reshape(img, [1, 2, 0]), hwc)
    assert planet_reshape(img, [1, 2, 0]).shape == (4, 5, 3)

    # Works on data that has already been reshaped
    assert planet_reshape(hwc, [2, 0, 1]).shape == (3, 4, 5)
    assert np.array_equal(planet_reshape(hwc, [2, 0, 1]), img)

    # Works for batch of images
    img2 = np.random.randint(0, 100, size=(3, 4, 5))
    blue2, green2, red2 = list(img2)
    batch = np.stack([img, img2])
    batch_hwc = np.stack([hwc, np.dstack([blue2, green2, red2])])
    assert np.array_equal(planet_reshape(batch, "HWC"), batch_hwc)
    assert planet_reshape(batch, "HWC").shape == (2, 4, 5, 3)
    assert np.array_equal(planet_reshape(batch, [1, 2, 0]), batch_hwc)
    assert planet_reshape(batch, [1, 2, 0]).shape == (2, 4, 5, 3)
    assert planet_reshape(batch_hwc, [2, 0, 1]).shape == (2, 3, 4, 5)
    assert np.array_equal(planet_reshape(batch_hwc, [2, 0, 1]), batch)

    # Raises if shape spec unrecognized
    with raises(ValueError, match=r"Invalid `shape`: B is not in CHW"):
        planet_reshape(img, "HWB")
    with raises(ValueError, match=r"Invalid `shape`: 3 is not in \[0, 1, 2\]"):
        planet_reshape(img, [1, 2, 3])

    # Raises if shape spec does not have 3 dimensions
    with raises(ValueError, match=r"`shape` must have 3 dimensions; got 2"):
        planet_reshape(img, "HW")

    # Raises if shape spec has duplicate dimensions
    with raises(ValueError, match=r"Duplicate dimension in `shape` \[1, 2, 2\]"):
        planet_reshape(img, "HWW")

    # Works for tensor data
    img_t = torch.from_numpy(img)
    assert torch.equal(planet_reshape(img_t, "HWC"), img_t.permute((1, 2, 0)))
    batch_t = torch.from_numpy(batch)
    assert torch.equal(planet_reshape(batch_t, "HWC"), batch_t.permute((0, 2, 3, 1)))


def test_planet_to_tensor():
    rgb = np.random.randint(0, 100, size=(3, 5, 5), dtype=np.uint8)
    toar = np.random.randint(0, 100, size=(4, 5, 5), dtype=np.uint16)

    # Converts 3D np.ndarray to 3D torch.Tensor
    tensor = planet_to_tensor(rgb, "RGB")
    assert tensor.dtype == torch.uint8
    assert tensor.shape == rgb.shape
    assert np.all(tensor.numpy() == rgb)

    tensor = planet_to_tensor(toar, "TOAR")
    assert tensor.dtype == torch.float32
    assert tensor.shape == toar.shape
    assert np.all(tensor.numpy() == toar)

    # Converts list of 3D np.ndarray to 4D torch.Tensor
    img_list = [rgb, rgb]
    tensor = planet_to_tensor(img_list, "RGB")
    assert tensor.dtype == torch.uint8
    assert tensor.shape == (2, 3, 5, 5)
    for t in tensor:
        assert np.all(t.numpy() == rgb)

    img_list = [toar, toar]
    tensor = planet_to_tensor(img_list, "SR")
    assert tensor.dtype == torch.float32
    assert tensor.shape == (2, 4, 5, 5)
    for t in tensor:
        assert np.all(t.numpy() == toar)

    # Converts 4D np.ndarray to 4D torch.Tensor
    batch = np.stack([rgb, rgb])
    assert batch.shape == (2, 3, 5, 5)
    tensor = planet_to_tensor(batch, "RGB")
    assert tensor.dtype == torch.uint8
    assert tensor.shape == batch.shape
    assert np.all(tensor.numpy() == batch)

    batch = np.stack([toar, toar])
    assert batch.shape == (2, 4, 5, 5)
    tensor = planet_to_tensor(batch, "TOAR")
    assert tensor.dtype == torch.float32
    assert tensor.shape == batch.shape
    assert np.all(tensor.numpy() == batch)

    # Works for torch.Tensor
    t3d = torch.randint(0, 100, size=(3, 5, 5), dtype=torch.uint8)
    tensor = planet_to_tensor(t3d, "RGB")
    assert tensor.dtype == torch.uint8
    assert tensor.shape == t3d.shape
    assert torch.equal(tensor, t3d)

    t4d = torch.randint(0, 100, size=(2, 4, 5, 5), dtype=torch.float32)
    tensor = planet_to_tensor(t4d, "TOAR")
    assert tensor.dtype == torch.float32
    assert tensor.shape == t4d.shape
    assert torch.equal(tensor, t4d)

    # Raises if data is list and elements are not 3D arrays
    with raises(TypeError, match=r"List `data` elements must be 3D; got"):
        planet_to_tensor([batch, batch], "TOAR")

    # Raises if data is not torch.Tensor, np.ndarray, or list of either
    with raises(TypeError, match=r"`data` must be np.ndarray, torch.Tensor, or list"):
        planet_to_tensor("foo", "TOAR")


if __name__ == "__main__":
    test_normalize_min_max()
    test_planet_check_dimension()
    test_planet_check_shape()
    test_planet_get_channels()
    test_planet_reshape()
    test_planet_to_tensor()
