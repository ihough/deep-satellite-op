"""Test util split_train_test()"""

from pandas import DataFrame
from pytest import raises, warns

from utils.split_train_test import split_train_test


def test_split_train_test():
    # Data is 100 samples with 20 groups (5 samples per group)
    samples = list(range(100))
    groups = sorted(5 * list(range(20)))
    data = DataFrame(dict(sample=samples, group=groups))
    grps = data["group"]  # Series to allow arbitrary indexing

    # Train and test do not overlap
    train, test = split_train_test(data, test_frac=0.11)
    assert not any(set(test).intersection(train))

    # Takes test from end of dataset
    assert split_train_test(data, test_frac=0.11) == (samples[:-11], samples[-11:])

    # Keeps grouped samples together
    train, test = split_train_test(data, test_frac=0.11, groups=groups)
    assert (train, test) == (samples[:-15], samples[-15:])
    assert not any(set(grps[test]).intersection(grps[train]))

    # Groups can be a column name
    assert split_train_test(data, 0.11, groups="group") == (samples[:-15], samples[-15:])

    # Can shuffle samples before splitting
    expected = [70, 87, 36, 21, 83, 9, 96, 67, 64, 47, 44]
    train, test = split_train_test(data, 0.11, shuffle=True, rand_seed=0)
    assert test == expected
    assert not any(set(test).intersection(train))

    # Can shuffle groups before splitting
    expected = [0, 1, 2, 3, 4, 75, 76, 77, 78, 79, 60, 61, 62, 63, 64]
    train, test = split_train_test(data, 0.11, groups="group", shuffle=True, rand_seed=0)
    assert test == expected
    assert not any(set(grps[test]).intersection(grps[train]))

    # Works if data is Series
    assert split_train_test(data["sample"], 0.1) == (samples[:-10], samples[-10:])

    # Series with grouping
    splits = split_train_test(data["sample"], 0.11, groups=groups)
    assert splits == (samples[:-15], samples[-15:])

    # Series with shuffle
    expected = [70, 87, 36, 21, 83, 9, 96, 67, 64, 47, 44]
    train, test = split_train_test(data["sample"], 0.11, shuffle=True, rand_seed=0)
    assert test == expected
    assert not any(set(test).intersection(train))

    # Series with grouping and shuffle
    expected = [0, 1, 2, 3, 4, 75, 76, 77, 78, 79, 60, 61, 62, 63, 64]
    train, test = split_train_test(
        data["sample"], 0.11, groups=groups, shuffle=True, rand_seed=0
    )
    assert test == expected
    assert not any(set(grps[test]).intersection(grps[train]))

    # Works if dataset's index != position of each row
    # The returned lists identify the **position** (iloc) of the train and test sample
    data2 = data.set_index(data.sample(frac=1, random_state=0).index)
    expected = [0, 1, 2, 3, 4, 75, 76, 77, 78, 79, 60, 61, 62, 63, 64]
    train, test = split_train_test(data2, 0.11, groups="group", shuffle=True, rand_seed=0)
    assert test == expected
    assert test != data2.index[test].to_list()

    # Works if groups are not numeric
    groups2 = sorted(list(5 * "abcdefghijklmnopqrst"))
    assert split_train_test(data, 0.11, groups=groups2) == (samples[:-15], samples[-15:])

    # Raises if test_frac is invalid
    with raises(ValueError, match="`test_frac` must be in range \\[0, 1\\); got"):
        split_train_test(data, test_frac=-0.1)
    with raises(ValueError, match="`test_frac` must be in range \\[0, 1\\); got"):
        split_train_test(data, test_frac=1.0)

    # Allows test_frac = 0
    assert split_train_test(data, test_frac=0) == (samples, [])

    # Raises if group column not found
    with raises(KeyError, match="`data` does not have `groups` column "):
        split_train_test(data, test_frac=0.1, groups="foo")

    # Raises if no data left for training (multiple check points)
    with raises(ValueError, match="No data left for train set; try decreasing `test_frac`"):
        split_train_test(data, test_frac=0.999)
    with raises(ValueError, match="No data left for train set; try decreasing `test_frac`"):
        split_train_test(data, test_frac=0.96, groups=groups)

    # Warns if test set is much larger than requested
    # 10 groups of 10 samples each
    groups3 = [item for sublist in [[x] * 10 for x in range(10)] for item in sublist]
    with warns(UserWarning, match="`test_frac` was .+ but test set contains .+"):
        split_train_test(data, test_frac=0.04, groups=groups3)
