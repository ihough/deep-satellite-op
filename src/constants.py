"""Project-wide constants"""

from pathlib import Path
import re


# Seed for reproducibility
SEED = 42


# Directories
PROJ_ROOT = Path(__file__).parent.parent
DATA_DIR = PROJ_ROOT / "data"
RAW_DIR = DATA_DIR / "raw"
PROCESSED_DIR = DATA_DIR / "processed"  # Prepared data (images, meteorolgy, etc.)
PROCESSED_IMAGE_DIR = PROCESSED_DIR / "images"  # Prepared Planet images
EXPERIMENTS_DIR = PROJ_ROOT / "experiments"  # Experiment artifacts (configs, logs)
MODELS_DIR = PROJ_ROOT / "models"  # Trained model weights


# Stations with oxidative potential data
OP_STATIONS = ["GRE-cb", "GRE-fr", "VIF"]


# Filters to identify mostly non-cloudy Planet images
# Based on manual inspection of images
TOAR_CLOUD_FILTER = """
    (
        (
            cover == 1 and cloud_cover == 0
            and green_q05 < 0.25
            and not date in ['2018-08-18', '2021-01-07', '2021-06-25']
            and not (date in ['2019-12-06', '2020-01-08'] and station != 'VIF')
            and not (date == '2020-05-10' and instrument == 'PS2')
            and not (date == '2021-08-12' and instrument == 'PS2' and station == 'GRE-cb')
        )
        or (
            cover == 1 and cloud_cover > 0 and cloud_cover < 1
            and green_q05 < 0.25 and green_q50 < 0.26 and green_q95 < 0.5
            and date in [
                '2017-01-19', '2017-01-26', '2017-03-15', '2017-03-17', '2017-04-05',
                '2017-07-08', '2017-07-28', '2017-08-09', '2017-10-22', '2018-02-13',
                '2018-03-08', '2018-03-16', '2018-03-19', '2018-03-23', '2018-03-24',
                '2018-03-26', '2018-04-25', '2018-04-26', '2018-04-27', '2018-04-28',
                '2018-05-05', '2018-05-07', '2018-05-11', '2018-05-12', '2018-05-21',
                '2018-06-01', '2018-06-02', '2018-06-08', '2018-06-09', '2018-06-24',
                '2018-08-06', '2018-08-19', '2018-09-22', '2018-09-30', '2018-10-14',
                '2019-03-16', '2019-04-01', '2019-04-20', '2019-04-22', '2019-04-23',
                '2019-04-29', '2019-07-21', '2019-08-05', '2019-08-19', '2019-09-11',
                '2019-09-27', '2019-10-14', '2019-10-24', '2020-01-13', '2020-02-25',
                '2020-06-15', '2020-06-21', '2020-07-02', '2020-09-02', '2020-10-03',
                '2021-03-25', '2021-06-10', '2021-07-07', '2021-07-24', '2021-08-28',
                '2021-09-22', '2021-09-29', '2021-10-26'
            ]
        )
        or (
            cover > 0.7 and cover < 1 and green_q95 < 0.21
        )
    )
"""
TOAR_CLOUD_FILTER = re.sub(r"\n *", " ", TOAR_CLOUD_FILTER).strip()

# For RGB, also keep images with no corresponding TOAR image and low green reflectance
RGB_CLOUD_FILTER = "green_q50 < 204 and green_q95 < 235 and green_sd > 15"
