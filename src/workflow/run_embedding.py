# Run supervised learning experiments with high-dimensional meteorology

import argparse
import logging
import traceback

from clis import embedding_cli
from constants import EXPERIMENTS_DIR

logger = logging.getLogger(__name__)


def run_embedding_experiments():
    base_dir = EXPERIMENTS_DIR / "embedding"

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--target",
        nargs="+",
        type=str,
        choices=["all", "pm10-imp", "op-aa25-m3", "op-dtt25-m3"],
        help="target air quality variable",
    )
    args = parser.parse_args()

    if args.target and "all" in args.target:
        args.target = [t for t in args.target if t != "all"]
        targets = sorted(d.name for d in base_dir.glob("*/") if d.name not in args.target)
        args.target.append(targets)

    experiments = []
    if args.target:
        for target in args.target:
            target_dir = base_dir / target
            target = target.replace("-", "_")

            exps = sorted(target_dir.glob("*.yaml"))
            if not any(exps):
                print(f"No experiments found in {target_dir}")
            experiments = experiments + exps
    else:
        # --------------------
        # OP AA experiments
        aa_dir = base_dir / "op-aa25-m3"
        # fmt:off
        aa_experiments = [
            # RGB, no CNN
            aa_dir / "rgb_nocnn_exp01.yaml", # embedded met (depth 3, ntrees 256)

            # RGB, resnet50 ImageNet
            aa_dir / "rgb_imagenet_exp01.yaml", # embedded met
            aa_dir / "rgb_imagenet_exp02.yaml", # embedded met, train backbone last 3
        ]
        # fmt:on

        # --------------------
        # OP DTT experiments
        dtt_dir = base_dir / "op-dtt25-m3"
        # fmt:off
        dtt_experiments = [
            # RGB, no CNN
            dtt_dir / "rgb_nocnn_exp01.yaml", # embedded met (depth 3, ntrees 256)

            # RGB, resnet50 ImageNet
            dtt_dir / "rgb_imagenet_exp01.yaml", # embedded met
            dtt_dir / "rgb_imagenet_exp02.yaml", # embedded met, train backbone last 3
        ]
        # fmt:on

        # --------------------
        # PM10 experiments
        pm_dir = base_dir / "pm10-imp"
        # fmt:off
        pm_experiments = [
            # RGB, no CNN
            pm_dir / "rgb_nocnn_exp01.yaml", # embedded met (depth 3, ntrees 256)

            # RGB, resnet50 ImageNet
            pm_dir / "rgb_imagenet_exp01.yaml", # embedded met
            pm_dir / "rgb_imagenet_exp02.yaml", # embedded met, train backbone last 3
        ]
        # fmt:on

        # --------------------
        # All experiments
        experiments = aa_experiments + dtt_experiments + pm_experiments

    for experiment in experiments:
        print("--------------------------------------------------")
        try:
            embedding_cli([f"--config={experiment}"])
        except SystemExit as exc:
            # Exit if KeyboardInterrupt
            if exc.code is None:
                exit()
            logger.error("Experiment failed: %s", experiment)
        except Exception:
            logger.error(traceback.format_exc())
            logger.error("Experiment failed: %s", experiment)
        print()


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )

    run_embedding_experiments()
