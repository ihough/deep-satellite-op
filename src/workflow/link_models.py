# Symlink final models into the models/ dir

import logging
import os
from pathlib import Path

import pandas as pd

from constants import PROJ_ROOT, EXPERIMENTS_DIR, MODELS_DIR

logger = logging.getLogger(__name__)


def list_models(dir: Path) -> pd.DataFrame:
    """Return a dataframe listing all "best" checkpoints in dir"""

    # Make DataFrame listing all "best" checkpoints
    models = pd.DataFrame(
        [
            list(path.relative_to(dir).parts) + [path]
            for path in sorted(dir.rglob("best_epoch*.ckpt"))
        ]
    )
    models.columns = [
        "dataset",
        "backbone",
        "experiment",
        "version",
        "ignore",
        "checkpoint",
        "path",
    ]
    models["dataset"] = models["dataset"].str.removesuffix("_PS2_clear")
    models["params"] = models.apply(
        lambda row: list(row["path"].parents[1].glob("batch*/"))[0].name, axis="columns"
    )
    models["fine-tuning"] = models["params"].str.contains("_bb-3_")
    models["met"] = "None"
    models.loc[models["params"].str.contains("_extra-6_"), "met"] = "Yes"
    models.loc[models["params"].str.contains("_extra-1976_"), "met"] = "HD"
    models = (
        models.drop(columns="ignore")
        .set_index(["dataset", "backbone", "experiment", "version"])[
            ["fine-tuning", "met", "params", "checkpoint", "path"]
        ]
        .sort_index()
    )

    return models


def link_model(out_path: Path, src_path: Path):
    """Symlink a model"""

    out_path.symlink_to(os.path.relpath(src_path, out_path.parent))
    logging.info("  -> %s", out_path.relative_to(PROJ_ROOT))


def link_models():
    """Symlink final models into the models/ dir"""

    # List OP AA models
    aa_models = list_models(EXPERIMENTS_DIR / "supervised" / "op-aa25-m3")
    aa_hd_models = list_models(EXPERIMENTS_DIR / "embedding" / "op-aa25-m3")

    out_dir = MODELS_DIR / "supervised" / "op-aa"
    out_dir.mkdir(parents=True, exist_ok=True)
    logging.info("Linking final OP AA models into %s", out_dir.relative_to(PROJ_ROOT))

    # Link best AA RGB models
    link_model(
        out_path=out_dir / "op-aa_rgb_baseline.pkl",
        src_path=aa_models.loc[("RGB", "nocnn", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_random-nomet.pkl",
        src_path=aa_models.loc[("RGB", "random_RGB", "exp01", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_random.pkl",
        src_path=aa_models.loc[("RGB", "random_RGB", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_transfer-nomet.pkl",
        src_path=aa_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp01", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_fine-tuning-nomet.pkl",
        src_path=aa_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_transfer.pkl",
        src_path=aa_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_fine-tuning.pkl",
        src_path=aa_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp04", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_simsiam-nomet.pkl",
        src_path=aa_models.loc[
            ("RGB", "simsiam_RGB_exp02_v0", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_simsiam.pkl",
        src_path=aa_models.loc[
            ("RGB", "simsiam_RGB_exp02_v0", "exp05", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_simsiam-bj-nomet.pkl",
        src_path=aa_models.loc[
            ("RGB", "jiang2022_beijing", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_simsiam-bj.pkl",
        src_path=aa_models.loc[
            ("RGB", "jiang2022_beijing", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_simsiam-dl-nomet.pkl",
        src_path=aa_models.loc[("RGB", "jiang2022_delhi", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_simsiam-dl.pkl",
        src_path=aa_models.loc[("RGB", "jiang2022_delhi", "exp03", "version_0"), "path"],
    )

    # Link best AA RGB embedding (high-dimensional meteorology) models
    link_model(
        out_path=out_dir / "op-aa_rgb_transfer-hd.pkl",
        src_path=aa_hd_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp01", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_rgb_fine-tuning-hd.pkl",
        src_path=aa_hd_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp02", "version_0"), "path"
        ],
    )

    # Link best AA TOAR models
    link_model(
        out_path=out_dir / "op-aa_toar_random-nomet.pkl",
        src_path=aa_models.loc[("TOAR", "random_BGRI", "exp01", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-aa_toar_random.pkl",
        src_path=aa_models.loc[("TOAR", "random_BGRI", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-aa_toar_transfer-nomet.pkl",
        src_path=aa_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp05", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_toar_fine-tuning-nomet.pkl",
        src_path=aa_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp06", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_toar_transfer.pkl",
        src_path=aa_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp07", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_toar_fine-tuning.pkl",
        src_path=aa_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp08", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_toar_simsiam-nomet.pkl",
        src_path=aa_models.loc[
            ("TOAR", "simsiam_BGRI_exp03_v0", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-aa_toar_simsiam.pkl",
        src_path=aa_models.loc[
            ("TOAR", "simsiam_BGRI_exp03_v0", "exp04", "version_0"), "path"
        ],
    )

    # List OP DTT models
    dtt_models = list_models(EXPERIMENTS_DIR / "supervised" / "op-dtt25-m3")
    dtt_hd_models = list_models(EXPERIMENTS_DIR / "embedding" / "op-dtt25-m3")

    out_dir = MODELS_DIR / "supervised" / "op-dtt"
    out_dir.mkdir(parents=True, exist_ok=True)
    logging.info("Linking final OP DTT models into %s", out_dir.relative_to(PROJ_ROOT))

    # Link best DTT RGB models
    link_model(
        out_path=out_dir / "op-dtt_rgb_baseline.pkl",
        src_path=dtt_models.loc[("RGB", "nocnn", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_random-nomet.pkl",
        src_path=dtt_models.loc[("RGB", "random_RGB", "exp01", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_random.pkl",
        src_path=dtt_models.loc[("RGB", "random_RGB", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_transfer-nomet.pkl",
        src_path=dtt_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp01", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_fine-tuning-nomet.pkl",
        src_path=dtt_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_transfer.pkl",
        src_path=dtt_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_fine-tuning.pkl",
        src_path=dtt_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp04", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_simsiam-nomet.pkl",
        src_path=dtt_models.loc[
            ("RGB", "simsiam_RGB_exp02_v0", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_simsiam.pkl",
        src_path=dtt_models.loc[
            ("RGB", "simsiam_RGB_exp02_v0", "exp05", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_simsiam-bj-nomet.pkl",
        src_path=dtt_models.loc[
            ("RGB", "jiang2022_beijing", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_simsiam-bj.pkl",
        src_path=dtt_models.loc[
            ("RGB", "jiang2022_beijing", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_simsiam-dl-nomet.pkl",
        src_path=dtt_models.loc[("RGB", "jiang2022_delhi", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_simsiam-dl.pkl",
        src_path=dtt_models.loc[("RGB", "jiang2022_delhi", "exp03", "version_0"), "path"],
    )

    # Link best DTT RGB embedding (high-dimensional meteorology) models
    link_model(
        out_path=out_dir / "op-dtt_rgb_transfer-hd.pkl",
        src_path=dtt_hd_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp01", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_rgb_fine-tuning-hd.pkl",
        src_path=dtt_hd_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp02", "version_0"), "path"
        ],
    )

    # Link best DTT TOAR models
    link_model(
        out_path=out_dir / "op-dtt_toar_random-nomet.pkl",
        src_path=dtt_models.loc[("TOAR", "random_BGRI", "exp01", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-dtt_toar_random.pkl",
        src_path=dtt_models.loc[("TOAR", "random_BGRI", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "op-dtt_toar_transfer-nomet.pkl",
        src_path=dtt_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp05", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_toar_fine-tuning-nomet.pkl",
        src_path=dtt_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp06", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_toar_transfer.pkl",
        src_path=dtt_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp07", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_toar_fine-tuning.pkl",
        src_path=dtt_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp08", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_toar_simsiam-nomet.pkl",
        src_path=dtt_models.loc[
            ("TOAR", "simsiam_BGRI_exp03_v0", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "op-dtt_toar_simsiam.pkl",
        src_path=dtt_models.loc[
            ("TOAR", "simsiam_BGRI_exp03_v0", "exp04", "version_0"), "path"
        ],
    )

    # List PM10 models
    pm_models = list_models(EXPERIMENTS_DIR / "supervised" / "pm10-imp")
    pm_hd_models = list_models(EXPERIMENTS_DIR / "embedding" / "pm10-imp")

    out_dir = MODELS_DIR / "supervised" / "pm10"
    out_dir.mkdir(parents=True, exist_ok=True)
    logging.info("Linking final PM10 models into %s", out_dir.relative_to(PROJ_ROOT))

    # Link best PM10 RGB models
    link_model(
        out_path=out_dir / "pm10_rgb_baseline.pkl",
        src_path=pm_models.loc[("RGB", "nocnn", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_random-nomet.pkl",
        src_path=pm_models.loc[("RGB", "random_RGB", "exp01", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_random.pkl",
        src_path=pm_models.loc[("RGB", "random_RGB", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_transfer-nomet.pkl",
        src_path=pm_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp01", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_fine-tuning-nomet.pkl",
        src_path=pm_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_transfer.pkl",
        src_path=pm_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_fine-tuning.pkl",
        src_path=pm_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp04", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_simsiam-nomet.pkl",
        src_path=pm_models.loc[
            ("RGB", "simsiam_RGB_exp02_v0", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_simsiam.pkl",
        src_path=pm_models.loc[
            ("RGB", "simsiam_RGB_exp02_v0", "exp05", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_simsiam-bj-nomet.pkl",
        src_path=pm_models.loc[
            ("RGB", "jiang2022_beijing", "exp02", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_simsiam-bj.pkl",
        src_path=pm_models.loc[
            ("RGB", "jiang2022_beijing", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_simsiam-dl-nomet.pkl",
        src_path=pm_models.loc[("RGB", "jiang2022_delhi", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_simsiam-dl.pkl",
        src_path=pm_models.loc[("RGB", "jiang2022_delhi", "exp03", "version_0"), "path"],
    )

    # Link best PM10 RGB embedding (high-dimensional meteorology) models
    link_model(
        out_path=out_dir / "pm10_rgb_transfer-hd.pkl",
        src_path=pm_hd_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp01", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_rgb_fine-tuning-hd.pkl",
        src_path=pm_hd_models.loc[
            ("RGB", "IMAGENET1K-V2_RGB", "exp02", "version_0"), "path"
        ],
    )

    # Link best PM10 TOAR models
    link_model(
        out_path=out_dir / "pm10_toar_random-nomet.pkl",
        src_path=pm_models.loc[("TOAR", "random_BGRI", "exp01", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "pm10_toar_random.pkl",
        src_path=pm_models.loc[("TOAR", "random_BGRI", "exp02", "version_0"), "path"],
    )
    link_model(
        out_path=out_dir / "pm10_toar_transfer-nomet.pkl",
        src_path=pm_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp05", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_toar_fine-tuning-nomet.pkl",
        src_path=pm_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp06", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_toar_transfer.pkl",
        src_path=pm_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp07", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_toar_fine-tuning.pkl",
        src_path=pm_models.loc[
            ("TOAR", "IMAGENET1K-V2_RGBI", "exp08", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_toar_simsiam-nomet.pkl",
        src_path=pm_models.loc[
            ("TOAR", "simsiam_BGRI_exp03_v0", "exp03", "version_0"), "path"
        ],
    )
    link_model(
        out_path=out_dir / "pm10_toar_simsiam.pkl",
        src_path=pm_models.loc[
            ("TOAR", "simsiam_BGRI_exp03_v0", "exp04", "version_0"), "path"
        ],
    )


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )
    link_models()
