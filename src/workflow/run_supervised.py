# Run supervised learning experiments

import argparse
import logging
import traceback

from clis import supervised_cli
from constants import EXPERIMENTS_DIR

logger = logging.getLogger(__name__)


def run_supervised_experiments():
    base_dir = EXPERIMENTS_DIR / "supervised"

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--target",
        nargs="+",
        type=str,
        choices=["all", "pm10-imp", "op-aa25-m3", "op-dtt25-m3"],
        help="target air quality variable",
    )
    args = parser.parse_args()

    if args.target and "all" in args.target:
        args.target = [t for t in args.target if t != "all"]
        targets = sorted(d.name for d in base_dir.glob("*/") if d.name not in args.target)
        args.target.append(targets)

    experiments = []
    if args.target:
        for target in args.target:
            target_dir = base_dir / target
            target = target.replace("-", "_")

            exps = sorted(target_dir.glob("*.yaml"))
            if not any(exps):
                print(f"No experiments found in {target_dir}")
            experiments = experiments + exps
    else:
        # --------------------
        # OP AA experiments
        aa_dir = base_dir / "op-aa25-m3"
        # fmt:off
        aa_experiments = [
            # All data, no CNN
            # aa_dir / "db_nocnn_exp01.yaml",  # met + time
            aa_dir / "db_nocnn_exp02.yaml",  # few met

            # RGB, no CNN
            # aa_dir / "rgb_nocnn_exp01.yaml",  # met + time
            aa_dir / "rgb_nocnn_exp02.yaml",  # few met

            # RGB, renset50 random
            aa_dir / "rgb_random_exp01.yaml",
            aa_dir / "rgb_random_exp02.yaml",  # few met

            # RGB, resnet50 ImageNet
            aa_dir / "rgb_imagenet_exp01.yaml",
            aa_dir / "rgb_imagenet_exp02.yaml", # train backbone last 3
            aa_dir / "rgb_imagenet_exp03.yaml", # few met
            aa_dir / "rgb_imagenet_exp04.yaml", # few met, train backbone last 3

            # RGB, resnet50 SimSiam
            # aa_dir / "rgb_simsiam_exp01.yaml",  # SimSiam Adam
            aa_dir / "rgb_simsiam_exp02.yaml",  # SimSiam SGD
            # aa_dir / "rgb_simsiam_exp03.yaml",  # SimSiam Adam trained on all data
            aa_dir / "rgb_simsiam_exp04.yaml",  # SimSiam SGD trained on all data
            aa_dir / "rgb_simsiam_exp05.yaml",  # SimSiam SGD, few met

            # RGB, resnet50 pretrained by Jiang et al. (2022)
            aa_dir / "rgb_beijing_exp01.yaml",
            aa_dir / "rgb_beijing_exp02.yaml",  # train backbone last 3
            aa_dir / "rgb_beijing_exp03.yaml",  # few met, train backbone last 3
            aa_dir / "rgb_delhi_exp01.yaml",
            aa_dir / "rgb_delhi_exp02.yaml",  # train backbone last 3
            aa_dir / "rgb_delhi_exp03.yaml",  # few met, train backbone last 3

            # TOAR, no CNN
            aa_dir / "toar_nocnn_exp02.yaml",  # few met

            # TOAR, renset50 random
            aa_dir / "toar_random_exp01.yaml",
            aa_dir / "toar_random_exp02.yaml",  # few met

            # TOAR, resnet50 ImageNet
            aa_dir / "toar_imagenet_exp01.yaml",  # TOAR RGB
            aa_dir / "toar_imagenet_exp02.yaml",  # TOAR RGB train backbone last 3
            aa_dir / "toar_imagenet_exp03.yaml",  # TOAR RGB few met
            aa_dir / "toar_imagenet_exp04.yaml",  # TOAR RGB few met, train backbone last 3
            aa_dir / "toar_imagenet_exp05.yaml",  # TOAR RGBI
            aa_dir / "toar_imagenet_exp06.yaml",  # TOAR RGBI train backbone last 3
            aa_dir / "toar_imagenet_exp07.yaml",  # TOAR RGBI few met
            aa_dir / "toar_imagenet_exp08.yaml",  # TOAR RGBI few met, train backbone last 3

            # TOAR, resnet50 SimSiam
            aa_dir / "toar_simsiam_exp03.yaml",  # SimSiam SGD trained on all data
            aa_dir / "toar_simsiam_exp04.yaml",  # SimSiam SGD trained on all data, few met
        ]
        # fmt:on

        # --------------------
        # OP DTT experiments
        dtt_dir = base_dir / "op-dtt25-m3"
        # fmt:off
        dtt_experiments = [
            # All data, no CNN
            # dtt_dir / "db_nocnn_exp01.yaml",  # met + time
            dtt_dir / "db_nocnn_exp02.yaml",  # few met

            # RGB, no CNN
            # dtt_dir / "rgb_nocnn_exp01.yaml",  # met + time
            dtt_dir / "rgb_nocnn_exp02.yaml",  # few met

            # RGB, renset50 random
            dtt_dir / "rgb_random_exp01.yaml",
            dtt_dir / "rgb_random_exp02.yaml",  # few met

            # RGB, resnet50 ImageNet
            dtt_dir / "rgb_imagenet_exp01.yaml",
            dtt_dir / "rgb_imagenet_exp02.yaml", # train backbone last 3
            dtt_dir / "rgb_imagenet_exp03.yaml", # few met
            dtt_dir / "rgb_imagenet_exp04.yaml", # few met, train backbone last 3

            # RGB, resnet50 SimSiam
            # dtt_dir / "rgb_simsiam_exp01.yaml",  # SimSiam Adam
            dtt_dir / "rgb_simsiam_exp02.yaml",  # SimSiam SGD
            # dtt_dir / "rgb_simsiam_exp03.yaml",  # SimSiam Adam trained on all data
            dtt_dir / "rgb_simsiam_exp04.yaml",  # SimSiam SGD trained on all data
            dtt_dir / "rgb_simsiam_exp05.yaml",  # SimSiam SGD, few met

            # RGB, resnet50 pretrained by Jiang et al. (2022)
            dtt_dir / "rgb_beijing_exp01.yaml",
            dtt_dir / "rgb_beijing_exp02.yaml",  # train backbone last 3
            dtt_dir / "rgb_beijing_exp03.yaml",  # few met, train backbone last 3
            dtt_dir / "rgb_delhi_exp01.yaml",
            dtt_dir / "rgb_delhi_exp02.yaml",  # train backbone last 3
            dtt_dir / "rgb_delhi_exp03.yaml",  # few met, train backbone last 3

            # TOAR, no CNN
            dtt_dir / "toar_nocnn_exp02.yaml",  # few met

            # TOAR, renset50 random
            dtt_dir / "toar_random_exp01.yaml",
            dtt_dir / "toar_random_exp02.yaml",  # few met

            # TOAR, resnet50 ImageNet
            dtt_dir / "toar_imagenet_exp01.yaml",  # TOAR RGB
            dtt_dir / "toar_imagenet_exp02.yaml",  # TOAR RGB train backbone last 3
            dtt_dir / "toar_imagenet_exp03.yaml",  # TOAR RGB few met
            dtt_dir / "toar_imagenet_exp04.yaml",  # TOAR RGB few met, train backbone last 3
            dtt_dir / "toar_imagenet_exp05.yaml",  # TOAR RGBI
            dtt_dir / "toar_imagenet_exp06.yaml",  # TOAR RGBI train backbone last 3
            dtt_dir / "toar_imagenet_exp07.yaml",  # TOAR RGBI few met
            dtt_dir / "toar_imagenet_exp08.yaml",  # TOAR RGBI few met, train backbone last 3

            # TOAR, resnet50 SimSiam
            dtt_dir / "toar_simsiam_exp03.yaml",  # SimSiam SGD trained on all data
            dtt_dir / "toar_simsiam_exp04.yaml",  # SimSiam SGD trained on all data, few met
        ]
        # fmt:on

        # --------------------
        # PM10 experiments
        pm_dir = base_dir / "pm10-imp"
        # fmt:off
        pm_experiments = [
            # All data, no CNN
            # pm_dir / "db_nocnn_exp01.yaml",  # met + time
            pm_dir / "db_nocnn_exp02.yaml",  # few met

            # RGB, no CNN
            # pm_dir / "rgb_nocnn_exp01.yaml",  # met + time
            pm_dir / "rgb_nocnn_exp02.yaml",  # few met

            # RGB, renset50 random
            pm_dir / "rgb_random_exp01.yaml",
            pm_dir / "rgb_random_exp02.yaml",  # few met

            # RGB, resnet50 ImageNet
            pm_dir / "rgb_imagenet_exp01.yaml",
            pm_dir / "rgb_imagenet_exp02.yaml",  # train backbone last 3
            pm_dir / "rgb_imagenet_exp03.yaml",  # few met
            pm_dir / "rgb_imagenet_exp04.yaml",  # few met, train backbone last 3

            # RGB, resnet50 SimSiam
            # pm_dir / "rgb_simsiam_exp01.yaml",  # SimSiam Adam
            pm_dir / "rgb_simsiam_exp02.yaml",  # SimSiam SGD
            # pm_dir / "rgb_simsiam_exp03.yaml",  # SimSiam Adam trained on all data
            pm_dir / "rgb_simsiam_exp04.yaml",  # SimSiam SGD trained on all data
            pm_dir / "rgb_simsiam_exp05.yaml",  # SimSiam SGD, few met

            # RGB, resnet50 pretrained by Jiang et al. (2022)
            pm_dir / "rgb_beijing_exp01.yaml",
            pm_dir / "rgb_beijing_exp02.yaml",  # train backbone last 3
            pm_dir / "rgb_beijing_exp03.yaml",  # few met, train backbone last 3
            pm_dir / "rgb_delhi_exp01.yaml",
            pm_dir / "rgb_delhi_exp02.yaml",  # train backbone last 3
            pm_dir / "rgb_delhi_exp03.yaml",  # few met, train backbone last 3

            # TOAR, no CNN
            pm_dir / "toar_nocnn_exp02.yaml",  # few met

            # TOAR, renset50 random
            pm_dir / "toar_random_exp01.yaml",
            pm_dir / "toar_random_exp02.yaml",  # few met

            # TOAR, resnet50 ImageNet
            pm_dir / "toar_imagenet_exp01.yaml",  # TOAR RGB
            pm_dir / "toar_imagenet_exp02.yaml",  # TOAR RGB train backbone last 3
            pm_dir / "toar_imagenet_exp03.yaml",  # TOAR RGB few met
            pm_dir / "toar_imagenet_exp04.yaml",  # TOAR RGB few met, train backbone last 3
            pm_dir / "toar_imagenet_exp05.yaml",  # TOAR RGBI
            pm_dir / "toar_imagenet_exp06.yaml",  # TOAR RGBI train backbone last 3
            pm_dir / "toar_imagenet_exp07.yaml",  # TOAR RGBI few met
            pm_dir / "toar_imagenet_exp08.yaml",  # TOAR RGBI few met, train backbone last 3

            # TOAR, resnet50 SimSiam
            pm_dir / "toar_simsiam_exp03.yaml",  # SimSiam SGD trained on all data
            pm_dir / "toar_simsiam_exp04.yaml",  # SimSiam SGD trained on all data, few met
        ]
        # fmt:on

        # --------------------
        # All experiments
        experiments = aa_experiments + dtt_experiments + pm_experiments

    for experiment in experiments:
        print("--------------------------------------------------")
        try:
            supervised_cli([f"--config={experiment}"])
        except SystemExit as exc:
            # Exit if KeyboardInterrupt
            if exc.code is None:
                exit()
            logger.error("Experiment failed: %s", experiment)
        except Exception:
            logger.error(traceback.format_exc())
            logger.error("Experiment failed: %s", experiment)
        print()


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )

    run_supervised_experiments()
