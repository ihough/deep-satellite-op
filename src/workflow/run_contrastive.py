# Run contrastive pretraining experiments

import logging
import traceback

from clis import contrastive_cli
from constants import EXPERIMENTS_DIR

logger = logging.getLogger(__name__)


def run_contrastive_experiments():
    base_dir = EXPERIMENTS_DIR / "contrastive"

    experiments = [
        # RGB data
        # base_dir / "rgb_simsiam_exp01.yaml",
        base_dir / "rgb_simsiam_exp02.yaml",  # optimizer SGD
        # base_dir / "rgb_simsiam_exp03.yaml",  # train on all data
        base_dir / "rgb_simsiam_exp04.yaml",  # SGD, train on all data

        # TOAR data
        # base_dir / "toar_simsiam_exp01.yaml",
        # base_dir / "toar_simsiam_exp02.yaml",  # train on all data
        base_dir / "toar_simsiam_exp03.yaml",  # SGD, train on all data
    ]

    for experiment in experiments:
        print("--------------------------------------------------")
        try:
            contrastive_cli([f"--config={experiment}"])
        except SystemExit as exc:
            # Exit on KeyboardInterrupt
            if exc.code is None:
                exit()
            logger.error("Experiment failed: %s", experiment)
        except Exception:
            logger.error(traceback.format_exc())
            logger.error("Experiment failed: %s", experiment)
        print()


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )

    run_contrastive_experiments()
