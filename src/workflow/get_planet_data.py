"""Download PlanetScope data for specific stations and timeperiods"""

import asyncio
import json
import logging
from pathlib import Path
import warnings
from typing import Literal, Optional

import geopandas as gpd
import numpy as np
import pandas as pd
import planet
from planet.auth import AuthType

from constants import RAW_DIR


logger = logging.getLogger(__name__)


# Directory to store downloaded images
# Images will be organized into folders within this dir
PLANET_DIR = RAW_DIR / "planet"

# Timeperiod(s) for which to download images (start and end are inclusive)
# Get 2017-2021 even though GRE-cb and VIF only measured OP for parts of this period
TIMEPERIODS = [
    {"first_day": "2017-01-01", "last_day": "2021-12-31"},
]

# Buffer distance (metres) used to define an area of interest (AOI) around each station.
# Buffers will be squares in the UTM zone corresponding to each station.
BUFFER_DIST = 500

# Minimum AOI coverage for images
# Images covering less than this fraction of AOI will not be downloaded
MIN_COVER = 0.9

# Planet item types to download
ITEM_TYPES = ["PSScene"]

# Planet asset types to download:
# * ortho_visual = orthorectified, color-corrected red, green, blue
# * ortho_analytic_4b = orthorectified, calibrated 4-band top of atmosphere
ASSET_TYPES = ["ortho_visual", "ortho_analytic_4b"]
ASSET_TO_DATA_TYPE = dict(ortho_visual="RGB", ortho_analytic_4b="TOAR")

# Type of data for each asset type
DATA_TYPE_TO_ASSET = {val: key for key, val in ASSET_TO_DATA_TYPE.items()}

# Planet Product bundles to download (and fallback if primary unavailable)
# https://developers.planet.com/apis/orders/product-bundles-reference/
BUNDLES = {
    "ortho_visual": "visual",
    "ortho_analytic_4b": "analytic_udm2",
}
FALLBACK_BUNDLES = {
    "ortho_visual": None,
    "ortho_analytic_4b": "analytic_3b_udm2",
}


def as_geojson(aoi: gpd.GeoDataFrame) -> dict:
    """Return a dict geojson representation of a GeoDataFrame's geometry"""

    return json.loads(aoi.geometry.to_json())


def calc_cover(aoi: gpd.GeoDataFrame, items: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    """Calculate portion of AOI covered by Planet scenes"""

    # Get the AOI geometry as a (multi)polygon
    # Need polygon for one-to-many intersection
    assert len(aoi.geometry.values) == 1, "aoi must have a single geometry"
    aoi_geom = aoi.geometry.values[0]

    # Calculate portion of AOI covered by each scene
    # * Ignore geographic CRS warning b/c approximate areas are good enough for overlap
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", message="Geometry is in a geographic CRS")

        # The fraction of the AOI covered by each scene
        items["scene_cover"] = items.intersection(aoi_geom).area / aoi_geom.area

        # The fraction of the AOI covered by each strip (union of all scenes per overpass)
        strip_cover = (
            items.dissolve(by=["instrument", "satellite_id", "strip_id"])
            .intersection(aoi_geom)
            .area
            / aoi_geom.area
        ).rename("strip_cover")
        items = items.join(strip_cover, on=["instrument", "satellite_id", "strip_id"])

        return items


def check_download(out_dir: Path, order_id: str) -> Path | str:
    """Raise if the order is not fully downloaded"""

    order_dir = out_dir / order_id
    try:
        planet.OrdersClient.validate_checksum(order_dir, "md5")
    except planet.exceptions.ClientError as exc:
        msg = str(exc)
        if msg.endswith("does not exist.") or msg.endswith("checksums do not match."):
            return Path(msg.split("File (")[1].split(") ")[0])
        else:
            raise exc

    return "Download complete"


def get_utm_zone(point: gpd.GeoSeries) -> str | list[str]:
    """Return the EPSG code(s) of the UTM zone(s) containing point(s)

    NOTE: does not handle special zones for Norway, Svalbard, the poles

    Args:
        point: geopandas GeoSeries or GeometryArray
    """

    assert isinstance(point, gpd.GeoSeries) or isinstance(point, gpd.GeoDataFrame)
    assert all(point.geom_type == "Point")

    # Ensure WGS84 coordinates
    point = point.to_crs("EPSG:4326")

    # Determine UTM zone number based on longitude
    zone = np.uint16(np.floor((point.geometry.x + 180) / 6)) + 1

    # Determine EPSG code(s)
    epsg = zone + np.where(point.geometry.y >= 0, 32600, 32700)

    # Return a single EPSG code if all points are in same zone
    if np.unique(epsg).shape[0] == 1:
        return "EPSG:" + str(epsg[0])
    else:
        return ["EPSG:" + str(e) for e in epsg]


def list_planet_files(base_dir: Path, station: str) -> tuple[pd.DataFrame, pd.DataFrame]:
    """List downloaded Planet images by parsing order manifests

    Assumed data structure:
    {base_dir}
    └── {data type}
        └── {instrument}
            └── {station}
                └── {image type}
                    └── {order ID}
                        └── **/<images>
    """

    scenes = []
    composites = []
    scene_cols = ["id", "asset_type", "path"]
    comp_cols = ["instrument", "date", "strip_id", "asset_type", "path"]
    for path in base_dir.glob(f"*/*/{station}/**/manifest.json"):
        relpath = path.relative_to(base_dir)
        image_type = relpath.parts[3]
        if image_type == "scene":
            files = (
                pd.DataFrame(
                    dict(path=path.parent / f["path"], **f["annotations"])
                    for f in json.loads(path.read_text())["files"]
                )
                .rename(columns=lambda x: x.replace("planet/", ""))
                .rename(columns=dict(item_id="id"))
                .query("asset_type.isin(@ASSET_TYPES)")
            )
            scenes.append(files[scene_cols])
        elif image_type == "composite":
            files = pd.DataFrame(json.loads(path.read_text())["files"])
            files = files.loc[files["path"].str.endswith("_composite.tif")]
            files["instrument"] = relpath.parts[1]
            files["date"] = pd.to_datetime(files["path"].str.slice(0, 10))
            files["strip_id"] = files["path"].str.extract(r"_strip_(\d+)_")
            files["asset_type"] = DATA_TYPE_TO_ASSET[relpath.parts[0]]
            files["path"] = path.parent / files["path"]
            composites.append(files[comp_cols])
        else:
            raise ValueError(
                f"Unrecognized image_type for manifest {path}. Expected 'scene' or"
                f" 'composite'; got '{image_type}'."
            )
    scenes = pd.concat(scenes) if len(scenes) > 0 else pd.DataFrame(columns=scene_cols)
    composites = (
        pd.concat(composites) if len(composites) > 0 else pd.DataFrame(columns=comp_cols)
    )

    return scenes, composites


def make_date_filter(period: dict, field: str = "acquired") -> dict:
    """Construct a Planet filter to return items between two dates"""

    return planet.data_filter.date_range_filter(
        field_name=field,
        gte=pd.to_datetime(period["first_day"]),
        lt=pd.to_datetime(period["last_day"]) + pd.Timedelta(1, "day"),
    )


def place_order(
    items: pd.DataFrame,
    image_type: Literal["scene", "composite"],
    aoi: gpd.GeoDataFrame,
    station: str,
    timeperiod: dict,
    auth: Optional[AuthType] = None,
) -> list[dict]:
    """Prepare and submit an order for scenes or composites covering a station"""

    def prepare_products(
        item_ids: list[str],
        item_type: str,
        product_bundle: str,
        fallback_bundle: str,
        max_items: int = 500,
    ) -> list[dict]:
        """Possibly split product ids to satisfy limit of 500 items / order"""

        splits = list(range(0, len(item_ids), max_items))
        products = []
        for i in splits:
            products.append(
                planet.order_request.product(
                    item_ids[i : i + 500],
                    item_type=item_type,
                    product_bundle=product_bundle,
                    fallback_bundle=fallback_bundle,
                )
            )

        return products

    orders = []
    items["asset_type"] = pd.Categorical(items["asset_type"], categories=ASSET_TYPES)
    for (instrument, item_type, asset_type), ids in items.groupby(
        ["instrument", "item_type", "asset_type"], observed=True
    )["id"]:
        # Identify data type of the asset
        data_type = ASSET_TO_DATA_TYPE[asset_type]

        # Define postprocessing
        # * Clip to AOI
        # * Possibly convert top-of-atmosphere radiance to reflectance
        # * Possibly composite by strip_id
        tools = [planet.order_request.clip_tool(as_geojson(aoi))]
        if asset_type == "ortho_analytic_4b":
            tools.append(planet.order_request.toar_tool(scale_factor=10000))
        if image_type == "composite":
            tools.append(dict(composite=dict(group_by="strip_id")))
            tools.append(planet.order_request.file_format_tool("COG"))

        # Define groups of product(s) to order (limited to 500 items each)
        products = prepare_products(
            item_ids=ids.to_list(),
            item_type=item_type,
            product_bundle=BUNDLES[asset_type],
            fallback_bundle=FALLBACK_BUNDLES[asset_type],
        )

        # Order each group of products
        for j, product in enumerate(products):
            # Name the order
            # e.g. GRE-cb PS2 PSScene ortho_analytic_4b cover>=0.9 2017-2021 scenes
            name = " ".join(
                [station, instrument, item_type, data_type, image_type]
                + [" to ".join([timeperiod["first_day"], timeperiod["last_day"]])]
            )
            if len(products) > 1:
                name += f" part {j + 1} of {len(products)}"

            logger.info(f"Ordering <{name}> ({len(product['item_ids'])} items)")
            order = asyncio.run(
                planet_order(
                    order=dict(name=name, products=[product], tools=tools), auth=auth
                )
            )
            order["station"] = station
            order["instrument"] = instrument
            order["item_type"] = item_type
            order["asset_type"] = asset_type
            order["data_type"] = data_type
            order["image_type"] = image_type
            orders.append(order)

    return orders


def parse_search_result(result: dict) -> pd.DataFrame:
    """Convert a Planet search response from JSON to a DataFrame"""

    # Get matching items
    items = result["features"]
    if not any(items):
        raise ValueError("No items matched search")

    # Convert to GeoDataFrame
    dat = gpd.GeoDataFrame.from_features(items, crs="EPSG:4326")
    dat["id"] = [i["id"] for i in items]
    dat["assets"] = [i["assets"] for i in items]
    dat["acquired"] = pd.to_datetime(dat["acquired"])
    dat["published"] = pd.to_datetime(dat["published"])
    dat["updated"] = pd.to_datetime(dat["updated"])

    # Sort
    dat["date"] = pd.to_datetime(dat["acquired"].dt.date)
    dat = dat.sort_values(["instrument", "item_type", "date"]).reset_index(drop=True)

    # Reorder columns
    first_cols = ["id", "instrument", "item_type", "date"]
    new_order = (
        first_cols
        + [col for col in dat.columns if col not in first_cols + ["geometry"]]
        + ["geometry"]
    )
    return dat[new_order]


async def planet_download(
    order: dict, out_dir: Path, auth: Optional[AuthType] = None
) -> Path:
    """Download an order"""

    async with planet.Session(auth=auth) as session:
        client = planet.OrdersClient(session)

        # Wait until the order is ready
        with planet.reporting.StateBar(
            order_id=order["id"], state="checking"
        ) as statebar:
            state = await client.wait(order["id"], callback=statebar.update_state)
        if state != "success":
            raise ValueError(
                f"Cannot download `order` {order['name']}; state is '{state}'"
            )

        # Download the order if not already on disk
        check = check_download(out_dir, order["id"])
        if isinstance(check, Path) and check.name == "manifest.json":
            await client.download_order(
                order["id"], directory=out_dir, overwrite=False, progress_bar=True
            )
            check = check_download(out_dir, order["id"])

        if isinstance(check, Path):
            order = await client.get_order(order["id"])
            links = order["_links"]["results"]
            paths = [out_dir / l["name"] for l in links]

        while check != "Download complete":
            idx = paths.index(check)
            await client.download_asset(
                location=links[idx]["location"],
                filename=paths[idx].name,
                directory=paths[idx].parent,
                overwrite=True,
                progress_bar=True,
            )
            check = check_download(out_dir, order_id=order["id"])

        return out_dir


async def planet_get_order(order_id: str, auth: Optional[AuthType] = None):
    """Get the details of a Planet order"""

    async with planet.Session(auth=auth) as session:
        client = planet.OrdersClient(session)
        return await client.get_order(order_id)


async def planet_list_orders(auth: Optional[AuthType] = None) -> dict:
    """List submitted orders"""

    async with planet.Session(auth=auth) as session:
        client = planet.OrdersClient(session)
        return await planet.collect(client.list_orders())


async def planet_order(order: dict, auth: Optional[AuthType] = None) -> dict:
    """Submit an order to the Planet API"""

    async with planet.Session(auth=auth) as session:
        client = planet.OrdersClient(session)
        request = planet.order_request.build_request(
            name=order["name"], products=order["products"], tools=order["tools"]
        )
        return await client.create_order(request)


async def planet_search(
    item_types: list[str], criteria: dict, limit: int = 0, auth: Optional[AuthType] = None
) -> dict:
    """Search for Planet items matching some criteria"""

    async with planet.Session(auth=auth) as session:
        client = planet.DataClient(session)
        request = client.search(
            item_types=item_types, search_filter=criteria, limit=limit
        )
        return await planet.collect(request)


# Main ---


def prepare_orders(
    aoi: gpd.GeoDataFrame, timeperiod: dict, auth: Optional[AuthType] = None
) -> list[dict]:
    """Order Planet images covering an AOI during a timeperiod"""

    if len(aoi.index) > 1:
        raise ValueError(f"`aoi` should have 1 row; got {len(aoi.index)}")
    station = aoi["station"].iloc[0]

    # Define query to find matching data
    criteria = planet.data_filter.and_filter(
        [
            # Must intersect AOI
            planet.data_filter.geometry_filter(as_geojson(aoi)),
            # Must be during timeperiod
            planet.data_filter.date_range_filter(
                field_name="acquired",
                gte=pd.to_datetime(timeperiod["first_day"]),
                lt=pd.to_datetime(timeperiod["last_day"]) + pd.Timedelta(1, "day"),
            ),
            # Must have one of the desired assets
            planet.data_filter.asset_filter(ASSET_TYPES),
            # Must be standard quality (exclude test data)
            planet.data_filter.std_quality_filter(),
            # Must be finalized
            planet.data_filter.string_in_filter("publishing_stage", ["finalized"]),
            # Only items we have permission to download
            planet.data_filter.permission_filter(),
        ]
    )

    # Query Planet API for list of data matching criteria
    # Parse the response JSON to a GeoDataFrame
    logger.info(
        f"{station}: searching for Planet data from "
        + " to ".join([timeperiod["first_day"], timeperiod["last_day"]])
    )
    result = asyncio.run(
        planet_search(item_types=ITEM_TYPES, criteria=criteria, auth=auth)
    )
    items = parse_search_result(result)

    # Restrict to items whose strip covers at least MIN_COVER of aoi
    items = calc_cover(aoi=aoi, items=items).query("strip_cover >= @MIN_COVER")

    # Count the number of desired data types (RGB, TOAR) available for each item
    n_assets = (
        items.explode("assets")
        .query("assets.isin(@ASSET_TYPES)")
        .groupby("id")["assets"]
        .count()
        .rename("n_assets")
    )
    items = items.join(n_assets, on="id")
    del n_assets

    # Get best item(s) for each instrument, item type, and day
    #   scenes = individual items
    #   composites = multiple items to composite
    best = (
        items.sort_values(
            ["n_assets", "strip_cover", "scene_cover", "cloud_cover", "acquired"],
            ascending=[False, False, False, True, False],
        )
        .groupby(["instrument", "item_type", "date"])
        .first()
    )
    scenes = items.loc[items["id"].isin(best.query("scene_cover == strip_cover")["id"])]
    composites = items.loc[
        items["strip_id"].isin(best.query("scene_cover != strip_cover")["strip_id"])
    ]
    del best

    # Identify assets to download (exclude assets already on disk)
    dl_scenes, dl_composites = list_planet_files(PLANET_DIR, station=station)
    scenes = (
        scenes.explode("assets")
        .rename(columns=dict(assets="asset_type"))
        .query("asset_type.isin(@ASSET_TYPES)")
        .join(dl_scenes.set_index(["id", "asset_type"]), on=["id", "asset_type"])
        .query("path.isna()")
    )
    composites = (
        composites.explode("assets")
        .rename(columns=dict(assets="asset_type"))
        .query("asset_type.isin(@ASSET_TYPES)")
        .join(
            dl_composites.set_index(["instrument", "date", "strip_id", "asset_type"]),
            on=["instrument", "date", "strip_id", "asset_type"],
        )
        .query("path.isna()")
    )
    del dl_scenes, dl_composites

    # Remove bad items
    # * GRE-fr 2019-03-09 scene that only covers 18% of AOI and cannot be composited by
    #   strip b/c other scene in strip has only RGB not TOAR data
    composites = composites.drop(
        index=composites.query(
            "id == '20190309_100849_0f42' and asset_type != 'ortho_visual'"
        ).index
    )

    # TODO: VIF has 15 assets to order (5 items with 3 assets each); previous order
    # d4993e89-b70b-4524-b327-6944d33459c9 contains alternate items from the same days but
    # for some reason the "best" item was not ordered (maybe sort by cloud cover failed?)

    orders = [
        dict(
            items=scenes,
            image_type="scene",
            station=station,
            aoi=aoi,
            timeperiod=timeperiod,
        ),
        dict(
            items=composites.loc[composites["date"].dt.year == 2018],
            image_type="composite",
            station=station,
            aoi=aoi,
            timeperiod=timeperiod,
        ),
    ]
    return [o for o in orders if len(o["items"]) > 0]


def download_order(order: dict, auth: Optional[AuthType] = None) -> Path:
    """Download a Planet order

    Data structure:
    PLANET_DIR
    └── {data type}
        └── {instrument}
            └── {station}
                └── {image type}
                    └── {order ID}
                        └── **/<images>
    """

    out_dir = (
        PLANET_DIR
        / order["data_type"]
        / order["instrument"]
        / order["station"]
        / order["image_type"]
    )
    out_dir.mkdir(parents=True, exist_ok=True)
    logger.info(f"Downloading <{order['name']}>")
    asyncio.run(planet_download(order=order, out_dir=out_dir, auth=auth))
    logger.info(f"-> {out_dir}")
    return out_dir


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(message)s", datefmt="%Y-%m-%d %H:%M:%S", level=logging.INFO
    )
    logger.info("".join(["-"] * 60))
    logger.info("Acquiring Planet data")
    logger.info("".join(["-"] * 60))

    # Make AOIs
    # * Read OP stations from data/raw/pmall/op-stations.csv
    logger.info("Preparing AOIs")
    stations = pd.read_csv(RAW_DIR / "pmall" / "pmall_stations.csv")
    areas = gpd.GeoDataFrame(
        data=stations[["station"]],
        geometry=gpd.points_from_xy(stations["longitude"], stations["latitude"]),
        crs="EPSG:4326",
    )
    # * Buffer (square) by BUFFER_DIST
    #   Use CRS of UTM zone of each station to match Planet data
    areas = areas.set_geometry(
        areas.to_crs(get_utm_zone(areas))
        .buffer(BUFFER_DIST, cap_style="square")
        .to_crs("EPSG:4326")
    )

    # Authenticate using the Planet API key stored at ~/.planet.json
    # You can use the planet cli to create this file:
    # `planet auth init`
    auth = planet.Auth.from_file()

    # Order data for all aois and timeperiods
    logger.info("Searching for new scenes")
    order_specs = []
    for idx in areas.index:
        for period in TIMEPERIODS:
            order_specs += prepare_orders(areas.loc[[idx], :], period, auth=auth)

    # Place orders
    orders = []
    for spec in order_specs:
        orders += place_order(**spec, auth=auth)

    # Download orders once complete
    for order in orders:
        download_order(order, auth=auth)

    # # Download previously ordered data
    # orders = asyncio.run(planet_list_orders(auth))
    # for order in orders:
    #     if order["state"] == "failed":
    #         print(f"Skipping failed order <{order['name']}>")
    #         continue
    #     n_items = len(order["products"][0]["item_ids"])
    #     if n_items == 1:
    #         print(f"Skipping test order <{order['name']}> ({n_items} item)")
    #         continue
    #     (
    #         order["station"],
    #         order["instrument"],
    #         order["item_type"],
    #         order["data_type"],
    #     ) = order["name"].split(" ")[0:4]
    #     order["image_type"] = "scene" if "scene" in order["name"] else "composite"
    #     if order["data_type"] not in DATA_TYPE_TO_ASSET.keys():
    #         print(f"Skipping deprecated data type order <{order['name']}>")
    #         continue
    #     download_order(order, auth)
