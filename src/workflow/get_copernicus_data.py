"""Download Copernicus data from the Climate Data Store

Usage:
    1. Modify main() to define the data to retrieve

    2. Register an account at https://cds.climate.copernicus.eu/

    3. Add the following to a .cdsapirc file in your home dir:
    url: https://cds.climate.copernicus.eu/api/v2
    key: your-cds-user-id:your-cds-api-key

    4. Run:
    `python get_copernicus_data.py`
"""

import shutil  # for cross-filesystem rename on CIMENT cluster
import tempfile

import cdsapi

from constants import RAW_DIR


def get_copernicus_data() -> None:
    """Download Copernicus data from the Climate Data Store"""

    # Years for which to get data
    years = [2017, 2018, 2019, 2020, 2021]

    # Output dir
    # out_dir = RAW_DIR / "copernicus"
    out_dir = RAW_DIR / "copernicus"

    # Construct orders for annual data files
    orders = []
    for year in years:
        # ERA5-Land hourly (11 km reanalysis)
        # Request variables individually b/c requests limited to 12000 items
        # Request as zipped netcdf (GRIB adds time entries for previous day)
        variables = [
            "10m_u_component_of_wind",
            "10m_v_component_of_wind",
            "2m_temperature",
            "2m_dewpoint_temperature",
            "surface_pressure",
            "total_precipitation",
        ]
        for var in variables:
            orders.append(
                {
                    "description": f"ERA5-land 1h {year} {var}",
                    "product": "reanalysis-era5-land",
                    "request": {
                        "variable": var,
                        "year": year,
                        "month": [m for m in range(1, 13)],
                        "day": [d for d in range(1, 32)],
                        "time": [h for h in range(0, 24)],
                        "area": [45.4, 5.5, 45.0, 5.8],  # Grenoble + Vif
                        "format": "netcdf.zip",
                    },
                    "filename": f"era5-land_1h_{year}_{var}.nc.zip",
                }
            )

        # ERA5 hourly (28 km reanalysis)
        # Request as netcdf (smaller file + GRIB adds time entries for previous day)
        orders.append(
            {
                "description": f"ERA5 1h {year}",
                "product": "reanalysis-era5-single-levels",
                "request": {
                    "product_type": "reanalysis",
                    "variable": [
                        "boundary_layer_height",
                    ],
                    "year": year,
                    "month": [m for m in range(1, 13)],
                    "day": [d for d in range(1, 32)],
                    "time": [str(h).zfill(2) + ":00" for h in range(0, 24)],
                    "area": [45.4, 5.5, 45.0, 5.8],  # Grenoble + Vif
                    "format": "netcdf",
                },
                "filename": f"era5_1h_{year}.nc",
            }
        )

    # Submit and download all orders
    client = cdsapi.Client(delete=False)  # Don't delete order after download
    for order in orders:
        path = out_dir / order["filename"]
        if path.exists():
            print(f"Exists: {path}")
        else:
            print(f"Ordering {order['description']}")
            tmp = tempfile.mktemp()
            client.retrieve(order["product"], order["request"], tmp)
            path.parent.mkdir(exist_ok=True)
            shutil.move(tmp, path)  # shutil.move works across filesystems
            print(f"-> {path}")


if __name__ == "__main__":
    get_copernicus_data()
