"""Data processing scripts

* PM and oxidative potential (OP) measures
* Meteorological variables
* Planet images
* Build final DB
"""

# from itertools import combinations
import json
import logging
from pathlib import Path
import pickle
import tempfile
import zipfile

import metpy
from metpy.units import units as metpy_units
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomTreesEmbedding
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.linear_model import LinearRegression
import torch
from torchvision.transforms.functional import center_crop
from tqdm import tqdm
import xarray as xr

from constants import (
    RAW_DIR,
    PROCESSED_DIR,
    PROCESSED_IMAGE_DIR,
    TOAR_CLOUD_FILTER,
    RGB_CLOUD_FILTER,
    OP_STATIONS,
    SEED,
)
from datasets.planet_image import PlanetImage
from datasets.planet_raw import PlanetRaw
from datasets.planet_dataset import PlanetDataset
from datasets import (
    PlanetImageRGB,
    PlanetImageTOAR,
    PlanetRawRGB,
    PlanetRawTOAR,
)
from utils import split_train_val_test
from utils.embedding import load_met_embedder, embed_met
from utils.imputation import make_circular
from utils.meteo import wind_speed, wind_direction
from utils.planet import normalize_min_max

# ------------------------------
# Raw data file paths

# * ATMO-AURA: air quality data
RAW_ATMO_DIR = RAW_DIR / "atmo-aura"
RAW_ATMO_AQ_PATH = RAW_ATMO_DIR / "atmo_aq_journaliere_2017-01-01_2021-12-31.csv"
RAW_ATMO_STATIONS_PATH = RAW_ATMO_DIR / "atmo_stations.geojson"

# * Copernicus: ERA5 reanalysis
RAW_COPERNICUS_DIR = RAW_DIR / "copernicus"
RAW_ERA5_PATHS = dict(dir=RAW_COPERNICUS_DIR, pattern="era5_1h_*.nc")
RAW_ERA5_LAND_PATHS = dict(dir=RAW_COPERNICUS_DIR, pattern="era5-land_1h_*.nc.zip")

# * Planet: satellite images
RAW_PLANET_DIR = RAW_DIR / "planet"

# PMall: PM components and oxidative potential
RAW_PMALL_DIR = RAW_DIR / "pmall"
RAW_OP_PATH = RAW_PMALL_DIR / "pmall_pm10-op_qamecs_2023-10-05.xlsx"
RAW_PMALL_TO_ATMO_PATH = RAW_PMALL_DIR / "pmall_stations_atmo_sites.csv"


# ------------------------------
# Processed data file paths

PROCESSED_DB_PATH = PROCESSED_DIR / "db.csv"
PROCESSED_DB_IMPUTED_PATH = PROCESSED_DIR / "db_imputed.csv"
PROCESSED_MET_EMBEDDER_PATH = PROCESSED_DIR / "met-embedder_d3-n256.pkl"
PROCESSED_MET_EMBEDDED_PATH = PROCESSED_DIR / "met-embed_d3-n256.csv"
PROCESSED_IMAGE_LIST_PATH = PROCESSED_DIR / "image-list.csv"


logger = logging.getLogger(__name__)


def prepare_db() -> None:
    """Preprocess and join air quality and meteorological data"""

    def load_stations() -> pd.DataFrame:
        """Load quality monitoring station metadata"""

        # ATMO stations
        stations = []
        for feature in json.loads(RAW_ATMO_STATIONS_PATH.read_text())["features"]:
            info = feature["properties"]
            info["lon"], info["lat"] = feature["geometry"]["coordinates"]
            stations.append(info)
        stations = pd.DataFrame(stations)

        # Correspondence with PMall stations
        pmall_stations = pd.read_csv(RAW_PMALL_TO_ATMO_PATH, index_col="label")["station"]
        stations["station"] = stations["label"].replace(pmall_stations.to_dict())

        # Subset columns
        stations = (
            stations.set_index("station")
            .sort_index()[["id", "label", "typologie", "lat", "lon"]]
            .rename(columns=dict(typologie="type"))
        )

        # Exclude 'Grenoble Rocade Sud' and 'Rocade Sud Eybens' stations
        # These stations were included in case they were useful for imputing missing air
        # quality measures at the main stations (GRE-cb, GRE-fr, and VIF). But since the
        # 'Grenoble Rocade Sud' station closed in Dec 2020 and was replaced by the
        # 'Rocade Sud Eybens' station (at a new location), both are missing > 1 year of
        # data. This makes the missing value imputation unstable. Combining the two into a
        # single fictititious station reduces instability, but does not improve the
        # imputation at the main stations (GRE-cb, GRE-fr, and VIF).
        stations = stations.drop(["Grenoble Rocade Sud", "Rocade Sud Eybens"])

        return stations

        # Example:
        #                           id                    label         type    lat   lon
        # station
        # Champ-sur-Drac       FR15013           Champ-sur-Drac  Péri Urbain 45.080 5.729
        # GRE-cb               FR15049    Caserne de Bonne Parc       Urbain 45.183 5.725
        # GRE-fr               FR15043      Grenoble les Frênes       Urbain 45.162 5.736
        # Grenoble Boulevards  FR15046      Grenoble Boulevards       Urbain 45.181 5.721
        # St-Martin-d'Hères    FR15038        St-Martin-d'Hères       Urbain 45.183 5.753
        # VIF                  FR15045  Grenoble Periurbain Sud  Péri Urbain 45.058 5.677

    def load_atmo_aq() -> pd.DataFrame:
        """Load ATMO-AURA air quality data"""

        logger.info("  loading ATMO-AURA air quality data")

        # Load data
        atmo = pd.read_csv(RAW_ATMO_AQ_PATH, sep=";", parse_dates=["date"])

        # Drop invalid data (shouldn't be any but just in case)
        atmo = atmo.query("validation == 1")

        # Make column for each pollutant
        atmo = atmo.pivot(
            index=["date", "site_label"],
            columns="label_court_polluant",
            values="valeur",
        ).rename_axis(None, axis="columns")

        # Replace negative values with 0
        atmo[atmo.lt(0)] = 0

        # Replace site label with PMall station ID
        # * Drop data for 'Grenoble Rocade Sud' and 'Rocade Sud Eybens' as not useful
        #   (see comment in load_stations() for details).
        stations = load_stations().reset_index().set_index("label")["station"]
        atmo = (
            atmo.join(stations, on="site_label", how="right")  # drop unneeded data
            .reset_index("site_label", drop=True)
            .set_index("station", append=True)
            .sort_index()
        )

        # Make column names lowercase
        atmo = atmo.rename(columns=lambda x: x.lower())

        return atmo

        # Example:
        #                                   no   no2   o3  pm10  pm2.5
        # date       station
        # 2017-01-01 Champ-sur-Drac       33.0  33.0  4.5   NaN    NaN
        #            GRE-cb               20.0  36.0  1.4  51.0    NaN
        #            GRE-fr               25.0  27.0  2.5  50.0   47.0
        # ...                              ...   ...  ...   ...    ...
        # 2021-12-31 Grenoble Boulevards  78.8  36.6  NaN  30.3    NaN
        #            St-Martin-d'Hères    60.4  25.6  3.6  18.9   15.2
        #            VIF                   7.3  14.0  8.9  11.4    NaN
        # [10840 rows x 5 columns]

    def load_meteorology() -> pd.DataFrame:
        """Load ERA5 hourly meteorology and interpolate to station locations

        ERA5 variables (spatial resolution: 0.25 degrees):
            blh: boundary layer height [m]
            tcc: total cloud cover [0 to 1]

        ERA5-Land variables (spatial resolution: 0.1 degrees):
            u10: 10m wind u component (zonal = eastwards) [m/s]
            v10: 10m wind v component (meridional = northwards) [m/s]
            d2m: 2m dewpoint temperature [K]
            t2m: 2m air temperature [K]
            msl: mean sea level pressure [Pa]
            sd:  snow depth [m of water equivalent]
            sp:  surface pressure [Pa]
            tp:  total precipitation [m]
        """

        logger.info("  loading meteorological data")

        # Load air quality stations
        stations = load_stations()

        # Load ERA5 and interpolate bilinearly to station locations
        era5_vars = ["blh", "tcc"]
        era5 = []
        for path in sorted(RAW_ERA5_PATHS["dir"].glob(RAW_ERA5_PATHS["pattern"])):
            logger.info("    reading %s from %s", era5_vars, path.name)
            with xr.open_dataset(path, cache=False) as data:
                era5.append(
                    data[era5_vars]
                    .interp(
                        latitude=stations["lat"].to_xarray(),
                        longitude=stations["lon"].to_xarray(),
                    )
                    .to_dataframe()
                )
        era5 = (
            pd.concat(era5)
            .rename(columns={"latitude": "lat", "longitude": "lon"})
            .sort_index()
        )

        # Load ERA5-Land and interpolate bilinearly to station locations
        era5_land = []
        for path in sorted(
            RAW_ERA5_LAND_PATHS["dir"].glob(RAW_ERA5_LAND_PATHS["pattern"])
        ):
            logger.info("    reading %s", path.name)
            with zipfile.ZipFile(path, "r") as zip_file:
                with tempfile.TemporaryDirectory() as tempdir:
                    tmp = zip_file.extract("data.nc", tempdir)
                    with xr.open_dataset(tmp, cache=False) as data:
                        era5_land.append(
                            data.interp(
                                latitude=stations["lat"].to_xarray(),
                                longitude=stations["lon"].to_xarray(),
                            ).to_dataframe()
                        )
        era5_land = (
            pd.concat(era5_land)
            .groupby(["time", "station"])
            .min()
            .rename(columns={"latitude": "lat", "longitude": "lon"})
            .sort_index()
        )

        # Merge the data
        # * ERA5 Land: temperature, dewpoint, pressure, precipitation, wind, snow
        # * ERA5: boundary layer height, total cloud cover
        meteo = era5_land.join(era5[era5_vars])
        del era5, era5_land

        # Get *scalar* mean wind speed (magnitude) from wind components
        # e.g. 5m/s N + 5m/s S -> 5m/s
        # Must do this before aggregating
        meteo["ws_scal"] = wind_speed(meteo["u10"], meteo["v10"])

        # Calculate relative humidity from t2m and d2m
        # Ensure d2m is never > t2m (this only occurs due to interpolation)
        meteo["rh"] = metpy.calc.relative_humidity_from_dewpoint(
            temperature=meteo["t2m"].to_numpy() * metpy_units.degK,
            dewpoint=meteo[["d2m", "t2m"]].min(axis="columns").to_numpy()
            * metpy_units.degK,
        )

        # Convert some units
        meteo["t2m"] = meteo["t2m"] - 273.15  # Kelvin -> degrees C
        meteo["sp"] = meteo["sp"] / 1000  #  pascals -> kPa
        meteo["tp"] = meteo["tp"] * 1000  # m -> mm

        # Aggregate daily summaries
        # * Lat / lon: first value
        # * Total precipitation: sum and number of hours with > 1mm
        # * Snow cover: mean only
        # * Wind components: mean only
        # * Other variables: mean and standard deviation
        aggs = dict(
            lat=("lat", "first"),
            lon=("lon", "first"),
            tp=("tp", "sum"),
            tp_count=("tp", lambda x: sum(x >= 0.001)),  # number of hours with >=1mm
            snowc=("snowc", "mean"),
            u10=("u10", "mean"),
            v10=("v10", "mean"),
        )
        for col in meteo.columns.difference(aggs.keys()):
            aggs |= {col: (col, "mean"), col + "_sd": (col, "std")}
        meteo = (
            meteo.groupby([pd.Grouper(level="time", freq="D"), "station"])
            .agg(**aggs)
            .rename_axis("date", axis="index", level="time")
        )

        # Get *vector* mean wind speed (velocity) and direction from mean wind components
        # e.g. 5m/s N + 5m/s S -> 0m/s W
        meteo["ws"] = wind_speed(meteo["u10"], meteo["v10"])
        meteo["wd"] = wind_direction(meteo["u10"], meteo["v10"])

        # Remove wind components
        meteo.drop(columns=["u10", "v10"], inplace=True)

        # Drop lat + lon and sort columns alphabetically Rearrange columns - difference() sorts alphabetically
        cols = ["lat", "lon"] + meteo.columns.difference(["lat", "lon"]).to_list()
        meteo = meteo[cols]

        return meteo

        # Example:
        #                                    lat   lon    blh ...    ws ws_scal ws_scal_sd
        # date       station                                  ...
        # 2017-01-01 Champ-sur-Drac       45.080 5.729 53.764 ... 1.166   1.274      0.370
        #            GRE-cb               45.183 5.725 44.442 ... 1.106   1.210      0.453
        #            GRE-fr               45.162 5.736 46.265 ... 1.138   1.239      0.449
        # ...                                ...   ...    ... ...   ...     ...        ...
        # 2021-12-31 Grenoble Boulevards  45.181 5.721 20.865 ... 1.299   1.310      0.516
        #            St-Martin-d'Hères    45.183 5.753 19.623 ... 1.323   1.333      0.498
        #            VIF                  45.058 5.677 22.141 ... 1.176   1.215      0.536
        # [10956 rows x 21 columns]

    def load_op() -> pd.DataFrame:
        """Load oxidative potential (OP) measures"""

        # Load OP measures
        logger.info("  loading oxidative potential measures")
        op = (
            pd.read_excel(RAW_OP_PATH, parse_dates=["Date"])
            # Ignore labels (always blank)
            .drop(columns=["Particle_size", "Sample_ID", "Labels"]).rename(
                columns=lambda x: x.lower()
            )
        )

        # Drop time part of datetime
        op["date"] = op["date"].dt.floor("D")

        # Drop duplicate observations (assumed mix-up resulting in multiple filters)
        # GRE-fr 2018-03-13 2018-03-16 2021-11-01 2021-11-04
        op = op.set_index(["date", "station"]).sort_index()
        op = op.loc[~op.index.duplicated(keep=False), :]

        # Drop PM columns (we will use ATMO-AURA's PM measures)
        op = op.drop(columns=["pm10aasqa", "pm10recons"])

        return op

        # Example:
        #                     op_aa25_m3  op_aa25_µg  ...  sd_op_dtt25_m3  sd_op_dtt25_µg
        # date       station                          ...
        # 2017-01-02 GRE-fr      3.28272     0.09119  ...         0.27902         0.00775
        # 2017-01-05 GRE-fr      0.99128     0.07625  ...         0.07205         0.00554
        # 2017-01-08 GRE-fr      3.53100     0.08827  ...         0.32310         0.00808
        # ...                        ...         ...  ...             ...             ...
        # 2021-12-25 GRE-fr      7.92973     0.22787  ...         0.17464         0.00502
        # 2021-12-28 GRE-fr      2.59157     0.16299  ...         0.07160         0.00450
        # 2021-12-31 GRE-fr      3.53536     0.20554  ...         0.05690         0.00331
        # [1419 rows x 8 columns]

    logger.info("Preparing air quality and meteorological data")

    # Load air quality data and meteorology
    op = load_op()
    atmo = load_atmo_aq()
    meteo = load_meteorology()

    # Merge all data
    # * outer merge for daily air quality and op to keep all data
    # * right merge for meteorology (never missing) to ensure row for each day
    logger.info("  joining")
    data = pd.merge(atmo, op, how="outer", on=["date", "station"])
    data = pd.merge(data, meteo, how="right", on=["date", "station"])
    data.sort_index(inplace=True)

    # Add day of week, day of year, and yera
    data["dow"] = data.index.get_level_values("date").day_of_week
    data["doy"] = data.index.get_level_values("date").day_of_year
    data["year"] = data.index.get_level_values("date").year

    # Identify study periods
    data["period"] = "Other"
    data.loc["2017-02-28":"2018-03-11", "period"] = "QAMECS"
    data.loc["2020-06-30":"2021-07-11", "period"] = "QAMECS"
    data.loc["2019", "period"] = "2019"

    # Save
    out_path = PROCESSED_DB_PATH
    data.to_csv(out_path)
    logger.info("  -> %s", out_path)

    #                                   no   no2   o3  pm10  ...  dow  doy  year  period
    # date       station                                     ...
    # 2017-01-01 Champ-sur-Drac       33.0  33.0  4.5   NaN  ...    6    1  2017   Other
    #            GRE-cb               20.0  36.0  1.4  51.0  ...    6    1  2017   Other
    #            GRE-fr               25.0  27.0  2.5  50.0  ...    6    1  2017   Other
    # ...                              ...   ...  ...   ...  ...  ...  ...   ...     ...
    # 2021-12-31 Grenoble Boulevards  78.8  36.6  NaN  30.3  ...    4  365  2021   Other
    #            St-Martin-d'Hères    60.4  25.6  3.6  18.9  ...    4  365  2021   Other
    #            VIF                   7.3  14.0  8.9  11.4  ...    4  365  2021   Other
    # [10956 rows x 38 columns]


def load_db() -> pd.DataFrame:
    """Load prepared OP and meteo data"""

    return pd.read_csv(
        PROCESSED_DB_PATH, index_col=["date", "station"], parse_dates=["date"]
    )


def prepare_met_embedder(ncores: int = -1) -> None:
    """Train an unsupervised RandomTreesEmbedding for meteorology"""

    # Met variables to consider
    vars = ["t2m", "blh", "rh", "sp", "wd", "ws"]
    logger.info("Training high-dimensional embedder for met vars %s", vars)

    # Fit unsupervised forest
    meteo = load_db().loc[pd.IndexSlice[:, OP_STATIONS], vars]
    forest = RandomTreesEmbedding(
        n_estimators=256,
        max_depth=3,
        sparse_output=False,
        n_jobs=ncores,
        random_state=SEED,
    )
    forest.fit(meteo)

    # Save
    out_path = PROCESSED_MET_EMBEDDER_PATH
    out_path.write_bytes(pickle.dumps(forest))
    logger.info("  -> %s", out_path)


def impute_air_quality(verbose=1) -> None:
    """Impute missing air quality measures by iterative linear regression

    See reports/impute_aq.ipynb for model development and estimated accuracy
    """

    logger.info("Imputing missing PM10, NO2, and O3")

    # Load data and save original column order
    db = load_db()
    db_cols = db.columns
    db.columns.name = "variable"

    # Add circular transform of wind direction
    wd_circ = make_circular(db["wd"], scale=360)
    db[wd_circ.columns] = wd_circ
    wd_circ = wd_circ.columns.to_list()

    # Define which variables to impute for each station
    all_stations = (
        OP_STATIONS + db.index.unique("station").difference(OP_STATIONS).to_list()
    )
    to_impute = (
        pd.MultiIndex.from_product(
            [all_stations, ["pm10", "no2", "no", "o3"]], names=["station", "variable"]
        )
        .insert(4, ("GRE-fr", "pm2.5"))
        .drop(
            [
                ("Champ-sur-Drac", "pm10"),
                ("GRE-cb", "o3"),
                ("Grenoble Boulevards", "o3"),
            ]
        )
    )

    # Get air quality wide = column per station-var
    aq_wide = db.unstack("station").reorder_levels(
        ["station", "variable"], axis="columns"
    )[to_impute]

    # Get lagged air quality wide = column per station-var
    aq_lag = (
        aq_wide.shift(1, freq="D")
        .rename(columns=lambda x: x + "_lag1", level="variable")
        .iloc[:-1]  # don't include 2022-01-01
    )

    # Get meteo vars wide = column per station-meteo var
    meteo_vars = (
        ["t2m", "blh", "sp", "rh", "tp", "ws_scal"] + wd_circ + ["t2m_sd", "ws_scal_sd"]
    )
    meteo = (
        db[meteo_vars]
        .unstack("station")
        .reorder_levels(["station", "variable"], axis="columns")
    )

    # Get temporal variables = column per var, 1 station only b/c same for all stations
    temporal = (
        pd.concat(
            [
                make_circular(db["dow"], scale=7),
                make_circular(db["doy"], scale=365),
                pd.get_dummies(db["year"], prefix="year", drop_first=True),
            ],
            axis="columns",
        )
        .loc[(slice(None), OP_STATIONS[0]), :]
        .rename_axis(columns="variable")
        .unstack("station")
        .reorder_levels(["station", "variable"], axis="columns")
    )

    # Make wide table
    # -> One row per day with air quality, lagged air quality, and meteo for all stations
    wide = pd.concat([aq_wide, aq_lag, meteo, temporal], axis="columns")
    del aq_wide, aq_lag, meteo, temporal

    # Impute for all stations simultaneously
    imputer = IterativeImputer(
        LinearRegression(),
        max_iter=20,
        tol=0.0057,
        initial_strategy="mean",
        imputation_order="ascending",
        random_state=SEED,
        verbose=verbose,
    )
    imputed = pd.DataFrame(
        imputer.fit_transform(wide), index=wide.index, columns=wide.columns
    )

    # Update original data
    # * Drop temporary columns added for imputing
    # * Suffix imputed columns with "_imp"
    # * Add imputed columns to db
    db = db[db_cols]
    imputed = imputed[to_impute].stack("station").round(1)
    imputed.rename(columns=lambda x: x + "_imp", inplace=True)
    db.loc[imputed.index, imputed.columns] = imputed

    # Rearrange columns
    aq_vars = db.columns.intersection(to_impute.unique("variable"), sort=False).to_list()
    aq_vars += [var + "_imp" for var in aq_vars]
    db = db[aq_vars + db.columns.difference(aq_vars, sort=False).to_list()]

    # Save
    out_path = PROCESSED_DB_IMPUTED_PATH
    db.to_csv(out_path)
    logger.info("  -> %s", out_path)

    # Example:
    # variable                      pm2.5 pm2.5_imp pm10 pm10_imp ... dow doy year period
    # date       station                                          ...
    # 2017-01-01 Champ-sur-Drac       NaN       NaN  NaN      NaN ...   6   1 2017  Other
    #            GRE-cb               NaN       NaN 51.0     51.0 ...   6   1 2017  Other
    #            GRE-fr              47.0      47.0 50.0     50.0 ...   6   1 2017  Other
    # ...                             ...       ...  ...      ... ... ... ...  ...    ...
    # 2021-12-31 Grenoble Boulevards  NaN       NaN 30.3     30.3 ...   4 365 2021  Other
    #            St-Martin-d'Hères   15.2       NaN 18.9     18.9 ...   4 365 2021  Other
    #            VIF                  NaN       NaN 11.4     11.4 ...   4 365 2021  Other
    # [10956 rows x 43 columns]


def load_db_imputed() -> pd.DataFrame:
    """Load imputed pm10atmo"""

    return pd.read_csv(
        PROCESSED_DB_IMPUTED_PATH, index_col=["date", "station"], parse_dates=["date"]
    )


def list_planet_images() -> None:
    """Create a CSV listing all downloaded Planet images"""

    planet_dir = RAW_PLANET_DIR
    logger.info("Listing all Planet images in %s", planet_dir)

    # Load metadata of all downloaded Planet orders
    images = []
    for path in planet_dir.rglob("manifest.json"):
        # Load manifest
        manifest = pd.DataFrame(json.loads(path.read_text())["files"])

        # Keep only image files (drop metadata and udm2 files)
        manifest = manifest.loc[
            manifest["path"].str.match(
                r".+("
                + r"|".join(
                    [
                        PlanetImageRGB.FILE_SUFFIX,
                        PlanetImageTOAR.FILE_SUFFIX,
                    ]
                )
                + r"|_composite)\.tif$"
            )
        ]

        # Get station, instrument, data_type, image_type, order_id from manifest path
        relpath = path.relative_to(planet_dir)
        manifest["data_type"] = relpath.parts[0]
        manifest["instrument"] = relpath.parts[1]
        manifest["station"] = relpath.parts[2]
        manifest["image_type"] = relpath.parts[3]
        manifest["order_id"] = relpath.parts[4]

        # Make item paths absolute
        manifest["path"] = path.parent / manifest["path"]
        images.append(manifest)
    images = pd.concat(images).reset_index(drop=True)
    del manifest

    # Parse columns containing JSON
    annotations = pd.DataFrame(images.pop("annotations").to_list()).rename(
        columns=lambda x: x.removeprefix("planet/")
    )
    digests = pd.DataFrame(images.pop("digests").to_list())

    # Combine and rearrange columns
    # fmt:off
    cols = ["data_type", "instrument", "station", "image_type", "item_type", "item_id",
            "order_id", "asset_type", "bundle_type", "path", "size"]
    # fmt:on
    images = pd.concat([images, annotations], axis="columns")[cols]
    images = pd.concat([images, digests], axis="columns")

    # Set item ID for composite images
    composite_mask = images["image_type"] == "composite"
    images.loc[composite_mask, "item_id"] = [
        path.stem.removesuffix("_composite")
        for path in images.loc[composite_mask, "path"]
    ]

    # Parse date from item ID
    images.loc[composite_mask, "date"] = pd.to_datetime(
        images.loc[composite_mask, "item_id"].str.slice(0, 10)
    )
    scene_mask = images["image_type"] == "scene"
    images.loc[scene_mask, "date"] = pd.to_datetime(
        images.loc[scene_mask, "item_id"].str.slice(0, 8)
    )

    # Set index
    images = images.set_index(["data_type", "instrument", "station", "date"]).sort_index()

    # Save
    out_path = PROCESSED_IMAGE_LIST_PATH
    out_path.parent.mkdir(parents=True, exist_ok=True)
    images.to_csv(out_path)
    logger.info("  -> %s", out_path)


def prepare_raw_images(
    dataset: PlanetRaw,
    out_path: str | Path,
    cloud_stats: bool = True,
    band_stats: bool = True,
) -> None:
    """Preprocess raw Planet images

    Image data:
    * If reflectance, scale to range [0.0, 1.0] (sets oversaturated pixels to 1.0)
    * Crop to 334x334 pixels (1 x 1 km)
    -> Save as torch.uint8 or torch.float32 of shape Images x Channels x Height x Width

    Image labels:
    * Image metadata e.g. location, date, sun azimuth
    * Band statistics e.g. mean, sd, quantiles
    -> Save as CSV
    """

    def compute_band_stats(data: torch.Tensor, bands: list[str]) -> dict[str, float]:
        """Compute summary statistics of Planet data bands"""

        # Convert data to np.ndarray
        data = data.numpy()

        # Dict to store band statistics
        stats = dict()

        # Mean and sd of each band
        # e.g. 'blue' = mean of blue band; 'red_sd' = standard deviation of red band
        stats |= dict(zip(bands, data.mean(axis=(1, 2))))
        stats |= dict(zip((b + "_sd" for b in bands), data.std(axis=(1, 2))))

        # Quantiles of each band (0.05, 0.25, 0.50, 0.75, 0.95)
        # e.g. 'blue_q05' = 5th percentile of blue band
        qtls = [5, 25, 50, 75, 95]
        for qtl in qtls:
            stats |= dict(
                zip(
                    [b + "_q" + str(qtl).zfill(2) for b in bands],
                    np.quantile(data, qtl / 100, axis=(1, 2)),
                )
            )

        return stats

    def compute_cloud_percent(img: PlanetImage, crop_size: int) -> float:
        cloud_mask = torch.from_numpy(img.cloud_mask)
        if cloud_mask.ndim == 2:
            cloud_mask = center_crop(cloud_mask, crop_size)
        return round(100 * (1 - cloud_mask.sum() / cloud_mask.numel()).item())

    def prepare_labels(labels: list[dict[str]]) -> pd.DataFrame:
        # Parse dates and rename index
        labels = pd.DataFrame(labels)
        labels["date"] = pd.to_datetime(labels["date"])
        labels.index.name = "file_idx"

        # Reorder the columns
        id_cols = dataset.labels.columns.to_list()
        stat_cols = ["cover", "cloud_cover"]
        if cloud_stats:
            stat_cols += ["cloud_percent"]  # , "visible_percent"]
        if band_stats:
            band_names = dataset[0].CHANNELS
            for band in band_names:
                stat_cols += labels.filter(regex=f"^{band}").columns.to_list()
            # nd_names = [
            #     "nd_" + "_".join(x) for x in combinations(reversed(band_names), 2)
            # ]
            # for nd_name in nd_names:
            #     stat_cols += [nd_name, nd_name + "_sd"]
        other_cols = sorted(set(labels.columns) - set(id_cols) - set(stat_cols))
        labels = labels[id_cols + stat_cols + other_cols]

        # Add OP and meteo data
        db = load_db()
        db_cols = db.columns.to_list()
        labels = labels.join(db, on=["date", "station"])

        # Add air quality from ATMO (with missing values imputed)
        aq_imputed = load_db_imputed()[["pm10_imp", "no2_imp", "o3_imp"]]
        aq_imputed_cols = aq_imputed.columns.to_list()
        labels = labels.join(aq_imputed, on=["date", "station"])

        # Reorder columns
        labels = labels[id_cols + stat_cols + other_cols + aq_imputed_cols + db_cols]

        return labels

    logger.info("  processing %s raw Planet images", len(dataset))

    # Preprocess each image
    crop_size = 334  # ensure images are 334 x 334 pixels (VIF are 335 x 335)
    processed = []
    labels = []
    warns = []
    for i in tqdm(range(0, len(dataset)), leave=True):
        img = dataset[i]

        # Skip if image has no data
        if img.data.max() == 0:
            warns.append(str(img))
            continue

        # * Get data as tensor
        # * Crop to consistent size
        # * If reflectance, scale to [0.0, 1.0]; this sets any oversaturated pixels to 1.0
        data = center_crop(img.as_tensor(), crop_size)
        if img.data_type in ["TOAR", "SR"]:
            data = normalize_min_max(data, img.VALUE_RANGE)
        processed.append(data)

        # Get image attributes and metadata
        info = dataset.labels.iloc[i].to_dict()
        info |= img.properties

        # Compute fraction of image missing data (all bands 0)
        info["cover"] = (data.amax(0).count_nonzero() / data[0].numel()).item()

        # Possibly calculate percent of image cloudy or visible based on udm2
        if cloud_stats:
            # Crop masks to same extent as preprocessed image
            info["cloud_percent"] = compute_cloud_percent(img, crop_size=crop_size)
            # info["visible_percent"] = compute_visible_percent(img, crop_size=crop_size)

        # Possibly add band summary statistics
        if band_stats:
            info |= compute_band_stats(data, bands=img.CHANNELS)

        # Store the stats and image attributes
        labels.append(info)

        # Cleanup
        del img, data, info

    # Report any warnings
    if any(warns):
        logger.warning("Skipped %s empty images:\n  %s", len(warns), "\n  ".join(warns))

    # Convert preprocessed data to torch.Tensor of shape Images x Bands x Height x Width
    processed = torch.stack(processed)

    # Save preprocessed data
    out_path = Path(out_path).resolve()
    out_path.parent.mkdir(parents=True, exist_ok=True)
    torch.save(processed, out_path)
    logger.info("  -> %s", out_path)
    del processed

    # Process labels -> DataFrame of image properties matching OP + meteo data
    labels = prepare_labels(labels)

    # Save labels
    labels_path = out_path.with_suffix(".csv")
    labels.to_csv(labels_path)
    logger.info("  -> %s", labels_path)

    # Example preprocessed TOAR data:
    # tensor([[[[0.2291, 0.2349, 0.2328,  ..., 0.2319, 0.2307, 0.2442],
    #           ...,
    #           [0.2532, 0.2494, 0.2489,  ..., 0.2470, 0.2747, 0.2932]],
    #
    #          [[0.2128, 0.2174, 0.2164,  ..., 0.2329, 0.2311, 0.2380],
    #           ...,
    #           [0.2284, 0.2309, 0.2368,  ..., 0.2427, 0.2674, 0.2896]],
    #
    #          [[0.1965, 0.2128, 0.2062,  ..., 0.2411, 0.2320, 0.2313],
    #           ...,
    #           [0.2153, 0.2224, 0.2302,  ..., 0.2376, 0.2620, 0.2927]],
    #
    #          [[0.2184, 0.2206, 0.2180,  ..., 0.2510, 0.2426, 0.2357],
    #           ...,
    #           [0.2053, 0.2213, 0.2442,  ..., 0.2434, 0.2737, 0.2791]]],
    #
    #         ...,
    #
    #         [[[0.1392, 0.1399, 0.1430,  ..., 0.1408, 0.1377, 0.1384],
    #           ...,
    #           [0.1173, 0.1193, 0.1234,  ..., 0.2773, 0.2703, 0.2623]],
    #
    #          [[0.1380, 0.1416, 0.1492,  ..., 0.1211, 0.1193, 0.1176],
    #           ...,
    #           [0.1075, 0.1106, 0.1130,  ..., 0.2677, 0.2630, 0.2580]],
    #
    #          [[0.1366, 0.1353, 0.1448,  ..., 0.1069, 0.1024, 0.1001],
    #           ...,
    #           [0.0960, 0.0954, 0.0987,  ..., 0.2779, 0.2700, 0.2630]],
    #
    #          [[0.3261, 0.3600, 0.3888,  ..., 0.2012, 0.2049, 0.2117],
    #           ...,
    #           [0.2683, 0.2865, 0.2903,  ..., 0.4977, 0.4928, 0.4778]]]])

    # Example labels
    #          asset_type instrument    item_type       date  ...  dow  doy  year  period
    # file_idx                                                ...
    # 0              TOAR        PS2      PSScene 2017-01-17  ...    1   17  2017   Other
    # 1              TOAR        PS2      PSScene 2017-01-19  ...    3   19  2017   Other
    # 2              TOAR        PS2      PSScene 2017-01-26  ...    3   26  2017   Other
    # 3              TOAR        PS2      PSScene 2017-03-11  ...    5   70  2017  QAMECS
    # 4              TOAR        PS2      PSScene 2017-03-11  ...    5   70  2017  QAMECS
    # ...             ...        ...          ...        ...  ...  ...  ...   ...     ...
    # 1867           TOAR     PSB.SD  PSOrthoTile 2021-06-09  ...    2  160  2021  QAMECS
    # 1868           TOAR     PSB.SD  PSOrthoTile 2021-06-09  ...    2  160  2021  QAMECS
    # 1869           TOAR     PSB.SD  PSOrthoTile 2021-06-22  ...    1  173  2021  QAMECS
    # 1870           TOAR     PSB.SD  PSOrthoTile 2021-06-22  ...    1  173  2021  QAMECS
    # 1871           TOAR     PSB.SD  PSOrthoTile 2021-06-22  ...    1  173  2021  QAMECS
    # [1872 rows x 120 columns]


def extract_images(dataset: PlanetDataset, query: str, out_path: str | Path) -> None:
    """Extract a subset of preprocessed Planet images and save to new file"""

    # Get subset
    subset = dataset.subset(query)

    # Skip if no clear images
    if len(subset) == 0:
        logger.info("  NONE")
        return None

    logger.info("  extracting %s images", len(subset))

    # Save subset
    out_path = Path(out_path).resolve()
    torch.save(subset.data, out_path)
    logger.info("  -> %s", out_path)

    # Update labels["file_idx"] to match new data file
    labels = subset.labels.drop(columns="file_idx").reset_index(drop=True)
    labels.index.name = "file_idx"

    # Save labels
    labels_path = out_path.with_suffix(".csv")
    labels.to_csv(labels_path)
    logger.info("  -> %s", labels_path)


def make_splits(
    labels: pd.DataFrame,
    test_frac: float,
    val_frac: float,
    groups: str | None = None,
    shuffle: bool = True,
    rand_seed: int = SEED,
) -> pd.Series:
    """Return a Series identifying the split set for each date and station"""

    # Split data
    train_idx, val_idx, test_idx = split_train_val_test(
        labels,
        test_frac=test_frac,
        val_frac=val_frac,
        groups=groups,
        shuffle=shuffle,
        rand_seed=rand_seed,
    )

    # Make pd.Series identifying split set for each date and station
    splits = labels[["date", "station"]].reset_index(drop=True)
    splits.loc[train_idx, "set"] = "train"
    splits.loc[val_idx, "set"] = "val"
    splits.loc[test_idx, "set"] = "test"
    splits["set"] = pd.Categorical(splits["set"], categories=["train", "val", "test"])
    splits = splits.set_index(["date", "station"])["set"]

    return splits

    # Example:
    # date        station
    # 2017-01-19  GRE-cb     train
    # 2017-01-26  GRE-cb      test
    # 2017-02-20  GRE-cb     train
    #                     ...
    # 2021-11-30  VIF        val
    # 2021-12-03  VIF        val
    # 2021-12-26  VIF        train
    # Name: set, Length: 1465, dtype: category
    # Categories (3, object): ['train', 'val', 'test']


def prepare_data():
    # Ensure the processed data dir exists
    PROCESSED_DIR.mkdir(parents=True, exist_ok=True)

    # --------------------------------------------------------------------------
    # Preprocess and join air quality and meteorological data
    #
    prepare_db()

    # --------------------------------------------------------------------------
    # Impute missing pm10 observations
    #
    impute_air_quality()

    # --------------------------------------------------------------------------
    # Create high-dimensional embedding of six meteorological variables
    #
    prepare_met_embedder()
    embedded = embed_met(
        met_data=load_db().loc[(slice(None), OP_STATIONS), :],
        embedder=load_met_embedder(PROCESSED_MET_EMBEDDER_PATH),
    )
    out_path = PROCESSED_MET_EMBEDDED_PATH
    embedded.to_csv(out_path)
    logger.info("  -> %s", out_path)
    del embedded, out_path

    # --------------------------------------------------------------------------
    # Preprocess Planet data
    #
    # Save a CSV listing all downloaded Planet images (for reference)
    list_planet_images()

    # Preprocess all RGB Planet images
    # These are used (along with TOAR images) to develop a cloud filter.
    logger.info("Preparing raw RGB (top-of-atmosphere visual) Planet images")
    rgb_path = PROCESSED_IMAGE_DIR / "RGB_all.pt"
    prepare_raw_images(
        dataset=PlanetRawRGB(),
        out_path=rgb_path,
        cloud_stats=False,  # RGB images don't have udm2 files
        band_stats=True,
    )

    # Preprocess all TOAR Planet images
    # These are used (along with RGB images) to develop a cloud filter.
    logger.info("Preparing raw TOAR (top-of-atmosphere reflectance Planet images")
    toar_path = PROCESSED_IMAGE_DIR / "TOAR_all.pt"
    prepare_raw_images(
        dataset=PlanetRawTOAR(), out_path=toar_path, cloud_stats=True, band_stats=True
    )

    # -----------------------------------------------------------------------
    #
    # The following can only run once TOAR and RGB CLOUD_FILTER have been defined
    #
    # -----------------------------------------------------------------------

    # Extract clear (mostly cloud-free) preprocessed TOAR images for each instrument.
    # This will simplify analyses using only clear images from a single instrument.
    toar = PlanetDataset(toar_path)
    toar_clear = toar.subset(TOAR_CLOUD_FILTER)
    for instrument in toar_clear.instruments:
        logger.info("Extracting clear TOAR %s images", instrument)
        extract_images(
            dataset=toar_clear,
            query=f"instrument == '{instrument}'",
            out_path=PROCESSED_IMAGE_DIR / f"TOAR_{instrument}_clear.pt",
        )

    # Extract clear (mostly cloud-free) preprocessed RGB images for each instrument.
    # This will simplify analyses using only clear images from a single instrument.
    rgb = PlanetDataset(rgb_path)
    rgb_clear = rgb.subset(
        rgb.labels.query("scene_id.isin(@toar_clear.labels['scene_id'])").index.union(
            rgb.labels.query("not scene_id.isin(@toar.labels['scene_id'])")
            .query(RGB_CLOUD_FILTER)
            .index
        )
    )
    for instrument in rgb_clear.instruments:
        logger.info("Extracting clear RGB %s images", instrument)
        extract_images(
            dataset=rgb_clear,
            query=f"instrument == '{instrument}'",
            out_path=PROCESSED_IMAGE_DIR / f"RGB_{instrument}_clear.pt",
        )

    # -----------------------------------------------------------------------
    # Create train / val / test splits
    #
    # 20% val, 20% test
    logger.info("Creating train / val / test splits")
    db = load_db().loc[(slice(None), OP_STATIONS), :].reset_index()[["date", "station"]]
    splits = make_splits(
        labels=db,
        test_frac=0.2,
        val_frac=0.2,
        groups="date",
        shuffle=True,
        rand_seed=SEED,
    )
    out_path = PROCESSED_DIR / "splits.csv"
    splits.to_csv(out_path)
    logger.info("-> %s", out_path)


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(message)s", datefmt="%Y-%m-%d %H:%M:%S", level=logging.INFO
    )
    logger.info("".join(["-"] * 60))
    logger.info("PREPARING DATA")
    logger.info("".join(["-"] * 60))

    prepare_data()
