"""Class to represent a raw (unprocessed) PlanetScope image"""

import json
from pathlib import Path
from typing import Optional

from dateutil.parser import isoparse
import numpy as np
from numpy import typing as npt
import rioxarray
import torch

from utils import validate_arg

PathType = Path | str


# Attributes of Planet data
PLANET_INSTRUMENTS = ("PS2", "PS2.SD", "PSB.SD")
PLANET_IMAGE_TYPES = ("scene", "composite")


class PlanetImage(object):
    """Base class for raw Planet images"""

    # To be set by child classes; include here for code hinting convenience
    CHANNELS: tuple[str, ...] = None
    FILE_SUFFIX: str = None
    VALUE_RANGE: tuple[float, float] = None

    def __init__(self, path: PathType, data_type: Optional[str] = None) -> None:
        """All images have been cropped to a 1x1 km square around a PM monitor (station).

        Args:
            path: Path to data file
            data_type: Optional data type for validation (default: `None`)

        Attributes:
            data_type:
                RGB: colour image optimized for human eye ("visual" asset)
                TOAR: top-of-atmosphere reflectance ("analytic" asset & TOAR tool)
                SR: surface reflectance ("analytic_sr" asset)
            instrument: the Planet instrument (PS2 / PS2.SD / PSB.SD)
            station: PM monitoring station the image is centered on
            image_type:
                scene: an image cropped from a single PlanetScope scene
                composite: an image composited from multiple scenes within a strip
            order_id: the ID of image's order
            date: the image acquisition date
            item_id: the ID of the Planet item the image is derived from
            data:
                RGB: np.uint8 [0-255]; shape [red, blue, green, alpha] x height x width
                TOAR or SR: np.uint16 [0-10000+] (may be >10000 due to oversaturation);
                    shape [blue, green, red, infrared] x height x width
        """

        # Ensure the path exists
        self.path = Path(path).resolve()
        if not self.path.exists():
            raise FileNotFoundError(f"No such `path`: {self.path}")

        # Get image type from path
        image_type = [x for x in self.path.parts[-4:-2] if x in PLANET_IMAGE_TYPES]
        if len(image_type) == 1:
            self.image_type = image_type[0]
        else:
            raise ValueError(
                f"Could not infer `image_type` of {self.path}. Expected parent dir with "
                f"name in {PLANET_IMAGE_TYPES}; got {image_type}."
            )

        # Get other attributes from path
        attr_slice = slice(-7, -2) if self.image_type == "scene" else slice(-6, -1)
        (
            self.data_type,
            self.instrument,
            self.station,
            self.image_type,
            self.order_id,
        ) = self.path.parts[attr_slice]
        if data_type is not None:
            self._ensure_data_type_is(data_type)
        validate_arg(self.instrument, PLANET_INSTRUMENTS, "instrument")
        validate_arg(self.image_type, PLANET_IMAGE_TYPES, "image_type")

        # Get acquisition date from filename
        try:
            date_slice = slice(0, 8) if self.image_type == "scene" else slice(0, 10)
            self.date = isoparse(self.path.stem[date_slice]).date()
        except ValueError as err:
            raise ValueError(
                f"Don't know how to parse acquisition date from {self.path.name}"
            ) from err

        # Get item ID from filename
        self.item_id = self.path.stem.removesuffix(self.FILE_SUFFIX)
        if self.image_type == "composite":
            self.item_id = self.item_id.removesuffix("_composite")

        # Assign an ID that identifies the station, instrument, date, and item; this can
        # be used to match RGB, TOAR, and SR images derived from the same Planet scene
        self.scene_id = "_".join([self.item_id, self.station])

        # Ensure there is a matching metadata file
        metadata_suffix = "_metadata.json"
        if self.image_type == "composite":
            metadata_suffix = "_composite" + metadata_suffix
        self.metadata_path = self.path.with_name(self.item_id + metadata_suffix)
        if not self.metadata_path.exists():
            raise FileNotFoundError(f"No metadata file: {self.metadata_path}")

        # Attributes assigned on access
        self._properties: dict = None
        self._udm2_path: Path = None

    def __init_subclass__(cls) -> None:
        super().__init_subclass__()
        for attribute in ["FILE_SUFFIX", "CHANNELS", "VALUE_RANGE"]:
            if getattr(cls, attribute) is None:
                raise TypeError(f"Subclass must set class attribute `{attribute}`")

    def __repr__(self) -> str:
        # e.g. <PlanetImage TOAR PS2 GRE-cb 2017-01-12 20170112_094417_0e19>
        desc = "<" + self.__class__.__name__
        if not self.__class__.__name__.endswith(self.data_type):
            desc += f" {self.data_type}"
        desc += f" {self.instrument} {self.station} {self.date} {self.item_id}"
        desc += ">"
        return desc

    def _ensure_data_type_is(self, data_type: str):
        """Ensure proper data type"""

        if self.data_type != data_type:
            raise ValueError(
                f"Invalid `data_type`: expected {data_type}; got {self.data_type}"
            )

    def as_tensor(self) -> torch.Tensor:
        """Return data as tensor; implemented by child classes"""

        raise NotImplementedError()

    @property
    def cloud_mask(self) -> npt.NDArray[np.bool_]:
        """Return a cloud mask based on udm2 cloud map (band 6)

        False for cloud pixels; True for all others. Multiply by `self.data` to
        mask (set to 0) clouds.

        Note that non-cloudy pixels are not necessarily "clear"; they may be
        affected by snow, haze, shadow, etc.
        """

        if self.udm2_path is None:
            return np.asarray(True)  # assume all pixels are not cloudy

        with rioxarray.open_rasterio(
            self.udm2_path, parse_coordinates=False, cache=False
        ) as udm:
            cloud_map = udm[5].to_numpy()  # band 6 has python list index 5
        return cloud_map == 0  # convert to boolean mask

    @property
    def data(self) -> npt.NDArray[np.uint8 | np.uint16]:
        """The image data channels x height x width

        * RGB has channels red, green, blue, alpha with uint8 data [0, 255]
        * TOAR and SR have channels blue, green, red, infrared with uint16 data [0, 10000]
            NOTE: data may be >10000 due to oversaturation; may overflow if cast to int16
        """

        # Don't cache data. PlanetImage data is only be used for preprocessing so should
        # only be accessed once.
        with rioxarray.open_rasterio(
            self.path, parse_coordinates=False, cache=False
        ) as rast:
            data = rast.to_numpy()
        return data

    @property
    def not_visible_mask(self) -> npt.NDArray[np.bool_]:
        """Return not visible mask based on udm2 heavy haze + cloud (bands 5+6)

        False for heavy haze and cloud pixels; True for all others. Multiply by
        `self.data` to mask (set to 0) pixels that are not visible.
        """

        if self.udm2_path is None:
            return np.asarray(True)  # assume all pixels are visible

        with rioxarray.open_rasterio(
            self.udm2_path, parse_coordinates=False, cache=False
        ) as udm:
            not_visible_map = (udm[4] | udm[5]).to_numpy()  # bands 5 and 6
        return not_visible_map == 0  # convert to boolean mask

    @property
    def properties(self) -> dict:
        """Image properties loaded from metadata.json e.g. view_angle"""

        if self._properties is None:
            props = json.loads(self.metadata_path.read_text("utf-8"))["properties"]
            self._properties = props

        return self._properties

    @property
    def udm2_path(self) -> Path:
        """Return the path to the corresponding udm2 = usable data mask v2"""

        # NOTE: RGB images have no udm v2; may also be missing for some TOAR and SR images
        if self._udm2_path is None:
            suffix = "_3B_udm2_clip" if self.image_type == "scene" else "_composite_udm2"
            udm2_path = self.path.with_stem(self.item_id + suffix)
            self._udm2_path = udm2_path if udm2_path.exists() else None
        return self._udm2_path


class PlanetImageRGB(PlanetImage):
    """A PlanetImage with RGB (visual red, green, blue) data"""

    CHANNELS = ("red", "green", "blue")
    FILE_SUFFIX = "_3B_Visual_clip"
    VALUE_RANGE = (0, 255)

    def __init__(self, path: PathType) -> None:
        super().__init__(path, data_type="RGB")

    def as_tensor(self) -> torch.Tensor:
        """Return data as uint8 tensor [red, green, blue] x height x width"""

        return torch.from_numpy(self.data[0:3]).contiguous()  # drop alpha channel


class PlanetImageTOAR(PlanetImage):
    """A PlanetImage with TOAR (top-of-atmosphere reflectance) data"""

    CHANNELS = ("blue", "green", "red", "infrared")
    FILE_SUFFIX = "_3B_AnalyticMS_toar_clip"
    VALUE_RANGE = (0, 10000)

    def __init__(self, path: PathType) -> None:
        super().__init__(path, data_type="TOAR")

    def as_tensor(self) -> torch.Tensor:
        """Return data as int32 tensor (int16 could overflow for oversaturated pixels)"""

        return torch.from_numpy(self.data.astype(np.int32)).contiguous()


def _sanity_check():
    """Sanity checks"""

    print("PlanetImageRGB")
    print("--------------")

    # RGB scene
    img = PlanetImageRGB(
        "data/raw/planet/RGB/PS2/GRE-cb/scene/5ce50f63-a5f3-4d85-ab82-0151f9784202"
        "/PSScene/20170112_094417_0e19_3B_Visual_clip.tif"
    )
    print("RGB img:", img)
    print("RGB img.properties:", img.properties)
    print("RGB img.udm2_path:", img.udm2_path)
    print("RGB img.CHANNELS:", img.CHANNELS)
    assert img.CHANNELS == ("red", "green", "blue")
    print("RGB img.VALUE_RANGE:", img.VALUE_RANGE)
    assert img.VALUE_RANGE == (0, 255)
    print("RGB img.data:", img.data.dtype, img.data.shape)
    assert img.data.dtype == np.uint8
    assert img.data.shape == (4, 334, 334)
    # print(img.data[:, 0:3, 0:3])
    print("RGB img.as_tensor():", img.as_tensor().dtype, img.as_tensor().shape)
    assert img.as_tensor().dtype == torch.uint8
    assert img.as_tensor().shape == (3, 334, 334)
    # print(img.as_tensor()[:, 0:3, 0:3])

    # RGB composite
    img = PlanetImageRGB(
        "data/raw/planet/RGB/PS2/GRE-fr/composite/d55d52da-19de-45a9-b843-35e78f80f313"
        "/2017-01-12_strip_360452_composite.tif"
    )
    print()
    print("RGB composite img:", img)
    assert img.CHANNELS == ("red", "green", "blue")
    assert img.VALUE_RANGE == (0, 255)
    assert img.data.dtype == np.uint8
    assert img.data.shape == (4, 334, 334)
    # print(img.data[:, 0:3, 0:3])
    assert img.as_tensor().dtype == torch.uint8
    assert img.as_tensor().shape == (3, 334, 334)
    # print(img.as_tensor()[:, 0:3, 0:3])

    # Fails if path is not an RGB image
    try:
        PlanetImageRGB(next(Path("data/raw/planet/TOAR").rglob("*.tif")))
    except ValueError as err:
        if "expected RGB; got TOAR" not in str(err):
            raise err
    else:
        raise AssertionError

    print()
    print("PlanetImageTOAR")
    print("---------------")

    # TOAR scene
    img = PlanetImageTOAR(
        "data/raw/planet/TOAR/PS2/GRE-cb/scene/5ec7cf6b-c3be-4702-9f67-dd2f8ff5a07b"
        "/PSScene/20170112_094417_0e19_3B_AnalyticMS_toar_clip.tif"
    )
    print("TOAR img:", img)
    print("TOAR img.properties:", img.properties)
    print("TOAR img.udm2_path:", img.udm2_path)
    print("TOAR img.CHANNELS:", img.CHANNELS)
    assert img.CHANNELS == ("blue", "green", "red", "infrared")
    print("TOAR img.VALUE_RANGE:", img.VALUE_RANGE)
    assert img.VALUE_RANGE == (0, 10000)
    print("TOAR img.data:", img.data.dtype, img.data.shape)
    assert img.data.dtype == np.uint16
    assert img.data.shape == (4, 334, 334)
    # print(img.data[:, 0:3, 0:3])
    print("TOAR img.as_tensor():", img.as_tensor().dtype, img.as_tensor().shape)
    assert img.as_tensor().dtype == torch.int32
    assert img.as_tensor().shape == (4, 334, 334)
    # print(img.as_tensor()[:, 0:3, 0:3])
    print("TOAR img.cloud_mask:", img.cloud_mask.shape)
    assert img.cloud_mask.shape == (334, 334)
    print("TOAR img.not_visible_mask:", img.not_visible_mask.shape)
    assert img.not_visible_mask.shape == (334, 334)

    # TOAR composite
    img = PlanetImageTOAR(
        "data/raw/planet/TOAR/PS2/GRE-fr/composite/03470495-38b3-4cd7-80a8-b528f88720cd"
        "/2017-01-12_strip_360452_composite.tif"
    )
    print()
    print("TOAR composite img:", img)
    assert img.CHANNELS == ("blue", "green", "red", "infrared")
    assert img.VALUE_RANGE == (0, 10000)
    assert img.data.dtype == np.uint16
    assert img.data.shape == (4, 334, 334)
    # print(img.data[:, 0:3, 0:3])
    assert img.as_tensor().dtype == torch.int32
    assert img.as_tensor().shape == (4, 334, 334)
    # print(img.as_tensor()[:, 0:3, 0:3])
    assert img.cloud_mask.shape == (334, 334)
    assert img.not_visible_mask.shape == (334, 334)

    # Fails if path is not a TOAR image
    try:
        PlanetImageTOAR(next(Path("data/raw/planet/RGB").rglob("*.tif")))
    except ValueError as err:
        if "expected TOAR; got RGB" not in str(err):
            raise err
    else:
        raise AssertionError


if __name__ == "__main__":
    _sanity_check()
