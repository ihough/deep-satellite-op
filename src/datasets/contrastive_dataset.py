"""Dataset for contrastive learning"""

from typing import Callable, Optional

import torch
from torch import Tensor
from torch.utils.data import Dataset

from datasets import PlanetDataset


class ContrastiveDataset(Dataset):
    """Dataset for contrastive learning"""

    def __init__(
        self,
        image_dataset: PlanetDataset,
        transform: Callable,
        normalize: Optional[Callable] = None,
    ) -> None:
        """
        Args:
            image_dataset: Dataset of prepared images
            transform: Transform that returns two augmented views of an image
            normalize: Optional transform to normalize the views (default: `None`)

        Returns:
            (view1, view2, idx):
                view1, view2: Augmented (and possibly normalized) views of an image
                idx: The image's index in the image_dataset
        """

        super().__init__()
        self.image_dataset = image_dataset
        self.transform = transform
        self.normalize = normalize

    def __len__(self) -> int:
        return len(self.image_dataset)

    def __repr__(self) -> str:
        desc = f"{self.__class__.__name__}("
        desc += "\n  image_dataset: " + str(self.image_dataset)
        desc += "\n  transform: " + str(self.transform).replace("\n)", "\n  )")
        if self.normalize is not None:
            desc += "\n  normalize: " + str(self.normalize).replace("\n)", "\n  )")
        desc += "\n)"
        return desc

    def __getitem__(self, idx: int | list[int]) -> tuple[Tensor, Tensor, int | list[int]]:
        """Returns two augmented views of an image and the image's index"""

        # Apply augmentation twice to same image
        x1 = self.transform(self.image_dataset[idx])
        x2 = self.transform(self.image_dataset[idx])

        # Possibly normalize
        # Normalizing after transform is safer b/c transform might modify pixel values.
        # Also, it turns out to be faster (!?) than assigning normalization transform to
        # image_dataset so that it is applied only once on initial data load
        if self.normalize:
            x1 = self.normalize(x1)
            x2 = self.normalize(x2)

        # Return idx as tensor so that lightning can automatically infer batch size
        return x1, x2, torch.as_tensor(idx)

    # Allow loading of entire batch at once
    __getitems__ = __getitem__


def _sanity_check():
    """Sanity checks"""

    from torch.utils.data import DataLoader
    from torchvision.transforms import CenterCrop

    from constants import PROCESSED_IMAGE_DIR
    from transforms import Augment

    print("ContrastiveDataset")
    print("------------------")
    planet = PlanetDataset(PROCESSED_IMAGE_DIR / "RGB_PS2.SD_clear.pt", idx=range(10))
    contrastive = ContrastiveDataset(planet, transform=Augment(96))
    print(contrastive)

    # Images are not loaded until accessed
    assert contrastive.image_dataset.is_loaded() is False
    view1, view2, idx = contrastive[0]
    assert view1.shape == (3, 96, 96)
    assert view1.dtype == torch.float32
    assert view2.shape == view1.shape
    assert view2.dtype == view1.dtype
    assert not torch.equal(view1, view2)
    assert idx == torch.tensor(0)
    print("contrastive[0]:", view1.shape, view2.shape, idx)

    dl = DataLoader(contrastive, batch_size=4, collate_fn=lambda x: x)
    batch = next(iter(dl))
    print("contrastive batch:", batch[0].shape, batch[1].shape, batch[2])

    # Any normalization is applied *after* augmentation (safer + faster!)
    contrastive.transform = CenterCrop(10)
    contrastive.normalize = lambda x: (x - 1) / 2
    view1, view2, idx = contrastive[0]
    alt = contrastive.normalize(contrastive.transform(contrastive.image_dataset[0]))
    assert torch.equal(view1, alt)
    assert torch.equal(view2, alt)
    print()
    print(contrastive)
    print("contrastive[0]:", view1.shape, view1.shape, idx)

    dl = DataLoader(contrastive, batch_size=4, collate_fn=lambda x: x)
    batch = next(iter(dl))
    print("contrastive batch:", batch[0].shape, batch[1].shape, batch[2])


if __name__ == "__main__":
    _sanity_check()
