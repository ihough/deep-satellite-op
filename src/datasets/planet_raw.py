"""Dataset of raw (unprocessed) PlanetScope images"""

from pathlib import Path
from typing import Optional

import pandas as pd
from torch.utils.data import Dataset

from constants import RAW_DIR
from datasets import PlanetImageRGB, PlanetImageTOAR, PlanetImageSR
from datasets.planet_image import PlanetImage, PLANET_INSTRUMENTS
from utils import validate_arg

IndexType = int | list[int] | slice


class PlanetRaw(Dataset):
    """Base class for datasets that are collections of raw Planet images"""

    # To be set by child classes; include here for code hinting convenience
    DATA_TYPE: str = None
    FILE_SUFFIX: str = None

    def __init__(
        self,
        instruments: Optional[str | list[str]] = None,
        stations: Optional[str | list[str]] = None,
    ) -> None:
        """
        Args:
            instruments: Instrument(s) to include (default: `None` = all instruments)
            stations: Station(s) to include (default: `None` = all stations)

        Attributes:
            root: Directory that contains the Planet images
            instruments: Included instrument(s) e.g. ["PS2", "PS2.SD", "PSB.SD"]
            stations: Included station(s) e.g. ["GRE-cb", "GRE-fr", "VIF"]
            images: List all PlanetImage in the dataset
            labels: DataFrame listing image identifiers and metadata e.g. data_type,
                instrument, station, date, item_id, scene_id
        """

        # Directory containing Planet images
        self.root = RAW_DIR / "planet" / self.DATA_TYPE
        if not self.root.exists():
            raise FileNotFoundError(f"`root` does not exist: {self.root}")

        # Instrument(s) and item type(s) to include
        # Store these as lists even if singular so they can be iterated over
        # e.g. instrument = ["PS2"] not "PS2"
        if instruments is None:
            instruments = list(PLANET_INSTRUMENTS)
        self.instruments = validate_arg(
            instruments, allowed=PLANET_INSTRUMENTS, arg_name="instrument", as_list=True
        )

        # Stations to include; if None specified include all
        known_stations = sorted(set(d.name for d in self.root.glob("*/*") if d.is_dir()))
        if not known_stations:
            raise FileNotFoundError(f"No station subdirs found in {self.root}")
        if stations is None:
            self.stations = known_stations
        else:
            self.stations = validate_arg(
                stations, allowed=known_stations, arg_name="station", as_list=True
            )

        # Find images, make labels, and sort images by labels
        self.images = self._find_images()
        self.labels = self._make_labels()
        self._sort_images_and_labels()

    def __init_subclass__(cls) -> None:
        super().__init_subclass__()
        for attribute in ["DATA_TYPE", "FILE_SUFFIX"]:
            if getattr(cls, attribute) is None:
                raise TypeError(f"Subclass must set class attribute `{attribute}`")

    def __getitem__(self, idx: IndexType) -> PlanetImage | list[PlanetImage]:
        if isinstance(idx, int) or isinstance(idx, slice):
            return self.images[idx]
        return [self.images[i] for i in idx]

    def __len__(self) -> int:
        return len(self.images)

    def __repr__(self) -> str:
        # e.g. <PlanetRaw 612 images TOAR ['PS2'] ['GRE-fr'] from >
        desc = "<" + self.__class__.__name__
        desc += f" {len(self)} images {self.instruments}"
        if len(self.stations) < 4:
            desc += f" {self.stations}"
        else:
            desc += f" [{len(self.stations)} stations]"
        desc += f" in {self.root}"
        desc += ">"
        return desc

    def _find_images(self) -> list[PlanetImage]:
        """Return a list of all raw Planet images matching dataset criteria"""

        images = []
        for instrument in self.instruments:
            for station in self.stations:
                subdir = self.root / instrument / station

                # Scene images
                for path in subdir.rglob(f"**/*{self.FILE_SUFFIX}.tif"):
                    images.append(self._init_image(path))

                # Composite images
                for path in subdir.rglob("**/*_composite.tif"):
                    images.append(self._init_image(path))

        return images

    def _init_image(self, path: Path) -> PlanetImage:
        """To be implemented by child classes: construct a PlanetImage from a path"""

        raise NotImplementedError()

    def _make_labels(self) -> pd.DataFrame:
        """Make labels for images -> pd.DataFrame for easy filtering"""

        # Image attributes to use as labels
        # fmt:off
        keys = ["data_type", "instrument", "station", "date", "item_id", "scene_id",
                "order_id", "image_type"]
        # fmt:on
        if not any(self.images):
            return pd.DataFrame(columns=keys)

        # Get labels from image attributes
        labels = []
        for img in self.images:
            labels.append(dict(zip(keys, [getattr(img, k) for k in keys])))
        labels = pd.DataFrame(labels)
        labels["date"] = pd.to_datetime(labels["date"])

        return labels

    def _sort_images_and_labels(self) -> None:
        """Sort labels and images"""

        # Paranoia: ensure labels are indexed by row number
        idx = self.labels.reset_index().sort_values(self.labels.columns.to_list()).index
        self.images = [self.images[i] for i in idx]
        self.labels = self.labels.iloc[idx].reset_index(drop=True)


class PlanetRawRGB(PlanetRaw):
    """Dataset of raw (unprocessed) RGB Planet images"""

    DATA_TYPE = "RGB"
    FILE_SUFFIX = "_3B_Visual_clip"

    def _init_image(self, path: Path) -> PlanetImageRGB:
        return PlanetImageRGB(path)


class PlanetRawTOAR(PlanetRaw):
    """Dataset of raw (unprocessed) TOAR Planet images"""

    DATA_TYPE = "TOAR"
    FILE_SUFFIX = "_3B_AnalyticMS_toar_clip"

    def _init_image(self, path: Path) -> PlanetImageTOAR:
        return PlanetImageTOAR(path)


class PlanetRawSR(PlanetRaw):
    """Dataset of raw (unprocessed) SR Planet images"""

    DATA_TYPE = "SR"
    FILE_SUFFIX = "_3B_AnalyticMS_SR_clip"

    def _init_image(self, path: Path) -> PlanetImageSR:
        return PlanetImageSR(path)


def _sanity_check():
    """Sanity checks"""

    print("PlanetRawRGB")
    print("------------")
    raw = PlanetRawRGB(instruments="PS2", stations=["GRE-cb", "VIF"])
    print("rgb:", raw)
    print("rgb.images:", len(raw), raw.images[0].__class__.__name__)
    print("rgb.images[0]:", raw.images[0])
    print("rgb.labels:", raw.labels, sep="\n")

    print()
    print("PlanetRawTOAR")
    print("-------------")
    raw = PlanetRawTOAR(instruments="PS2.SD", stations=["GRE-fr"])
    print("toar:", raw)
    print("toar.images:", len(raw), raw.images[0].__class__.__name__)
    print("toar.images[0]:", raw.images[0])
    print("toar.labels:", raw.labels, sep="\n")

    print()
    print("PlanetRawSR")
    print("-----------")
    raw = PlanetRawSR()
    print("sr:", raw)
    print("sr.images:", len(raw), raw.images[0].__class__.__name__)
    print("sr.images[0]:", raw.images[0])
    print("sr.labels:", raw.labels, sep="\n")


if __name__ == "__main__":
    _sanity_check()
