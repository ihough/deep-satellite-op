"""Dataset of preprocessed Planet data"""

import logging
from pathlib import Path
from typing import Callable, Optional, Self

import pandas as pd
import torch
from torch.utils.data import Dataset

IdxType = pd.Index | pd.Series | slice | list[int] | int

logger = logging.getLogger(__name__)


class PlanetDataset(Dataset):
    """Base dataset for preprocessed Planet data"""

    def __init__(
        self,
        data_path: str | Path,
        transform: Optional[Callable] = None,
        idx: Optional[IdxType] = None,
    ) -> None:
        """The prepared data must be saved as a torch.Tensor in a *.pt file with an
        accompanying *.csv file containing labels. The entire data tensor will be loaded
        into memory the first time data is accessed.

        Args:
            data_path: Path to a data file containing a `torch.Tensor`
            transform: Optional transform to apply to the data on load (default: `None`)
            idx: Optional index identifying images to load from `data_file`
                (default: `None` = load all images)

        Attributes:
            data_types: Included data types e.g. ["TOAR"]
            instruments: Included instruments e.g. ["PS2"]
            stations: Included stations e.g. ["GRE-cb", "GRE-fr", "VIF"]
            transform: Transform for the data e.g. to subset channels (default: None)
            data: torch.float32 containing the data; loaded on first access
            labels: pd.DataFrame of labels (metadata); loaded on first access e.g. date,
                station, item_id, OP measure
            data_path: Path to a `*.pt` file containing data
            labels_path: Path to a `*.csv` file containing labels
        """

        super().__init__()

        # Ensure data file exists
        self.data_path = Path(data_path).resolve()
        if not self.data_path.exists():
            raise FileNotFoundError(f"`data_file` does not exist: {self.data_path}")

        # Path to labels
        self.labels_path = self.data_path.with_suffix(".csv")
        if not self.labels_path.exists():
            raise FileNotFoundError(f"`labels_path` does not exist: {self.labels_path}")

        # Optional transform for the data (applied on data load)
        self._transform = transform

        # Possibly restrict to a subset of the images in the data file
        # Store as list for indexing of both torch.Tensor (data) and pd.DataFrame (labels)
        if idx is not None:
            idx = self._as_list_idx(idx)
        self.file_idx = idx

        # Attributes that will be set on first access (to avoid loading all data on init)
        self._data: torch.Tensor = None
        self._labels: pd.DataFrame = None

    def __getitem__(self, idx: int | list[int]) -> torch.Tensor:
        items = self.data[idx]  # self.transform is applied when data is loaded
        return items

    def __len__(self) -> int:
        return len(self.labels.index)

    def __repr__(self) -> str:
        # e.g. <458 images from planet_RGB_PS2_clear.pt>
        desc = f"{self.__class__.__name__}({len(self)} images"
        if self.transform is not None:
            desc += f" {self.transform}"
        desc += f" from {self.data_path.name}"
        desc += ")"
        return desc

    @property
    def data(self) -> torch.Tensor:
        """torch.float32 of preprocessed data (possibly subset and transformed)"""

        if self._data is not None:
            return self._data

        logger.debug(
            "%s <%s>: reading %s images from %s",
            self.__class__.__name__,
            id(self),
            "all" if self.file_idx is None else len(self.file_idx),
            self.data_path.name,
        )
        data = torch.load(self.data_path)

        # Possibly restrict to specified images
        if self.file_idx is not None:
            data = data[self.file_idx]

        # Transform data on load
        if self.transform is not None:
            data = self.transform(data)

        # Ensure float32 for mps compatibility
        self._data = data.float()

        return self._data

    @property
    def data_types(self) -> str:
        """The included data types e.g. ['TOAR']"""

        return sorted(self.labels["data_type"].unique())

    @property
    def device(self) -> torch.device:
        """Device on which the data is located; None if not in memory"""

        if not self.is_loaded():
            return None
        return self.data.device

    @property
    def instruments(self) -> list[str]:
        """The included Planet instruments e.g. ['PS2']"""

        return sorted(self.labels["instrument"].unique())

    @property
    def labels(self) -> pd.DataFrame:
        """DataFrame containing labels (attributes) for each item e.g. date,
         station, instrument, OP measure

        `labels.index` identifies each item's position in `self.data`
        `labels["file_idx"]` identifies each item's position in `self.data_path`
        """

        if self._labels is not None:
            return self._labels

        logger.debug(
            "%s <%s>: reading %s labels from %s",
            self.__class__.__name__,
            id(self),
            "all" if self.file_idx is None else len(self.file_idx),
            self.labels_path.name,
        )
        labels = pd.read_csv(self.labels_path, parse_dates=["date", "acquired"])

        # Possibly restrict to labels of specified images
        if self.file_idx is not None:
            labels = labels.iloc[self.file_idx].reset_index(drop=True)

        # Use float32 not float64 for numeric columns -> mps compatibility
        float_cols = labels.select_dtypes("float64").columns
        labels[float_cols] = labels[float_cols].astype("float32")
        self._labels = labels

        return self._labels

    @property
    def stations(self) -> list[str]:
        """Station(s) included in the dataset e.g. ['GRE-cb', 'GRE-fr', 'VIF']"""

        return sorted(self.labels["station"].unique())

    @property
    def transform(self) -> Callable:
        """Transform applied to the data on load"""

        return self._transform

    @transform.setter
    def transform(self, transform: Callable | None) -> None:
        """Set a new transform and unload the data"""

        self._transform = transform
        self.unload()  # unload data

    def _as_list_idx(self, idx: IdxType) -> list[int]:
        """Convert idx to a list of integers"""

        try:
            idx = list(idx)
        except TypeError as err:
            if str(err).endswith("object is not iterable"):
                if isinstance(idx, slice):
                    idx = range(idx.stop)[idx]
                else:
                    idx = [idx]

        return idx

    def is_loaded(self) -> bool:
        """Whether data is loaded in memory"""

        return self._data is not None

    def query(self, query: str) -> pd.DataFrame:
        """Query the labels with a boolean expression"""

        return self.labels.query(query)

    def subset(self, query: IdxType | str) -> Self:
        """Return a `PlanetDataset` containing only the images identified by `query`

        NOTE: always returns a `PlanetDataset`, even if called on a child class; see
        PlanetClear for explanation of why.

        Args:
            query:
                Index-like: Index of positions in `self.data`
                    e.g [1, 2] -> subset containing `self.data[[1, 2]]`
                String: Boolean expression to apply to `self.labels`
                    e.g. "station == 'GRE-cb'" -> subset containing
                    `self.data[self.labels.query({query}).index]`
        """

        # Translate query to index
        if isinstance(query, str):
            idx = self.query(query).index
        else:
            idx = self._as_list_idx(query)

        # Get file_idx (position in data file) corresponding to idx
        file_idx = self.labels["file_idx"].iloc[idx].to_list()

        return PlanetDataset(
            data_path=self.data_path, transform=self.transform, idx=file_idx
        )

    def to(self, device: Optional[str] = None) -> None:
        """Transfer data to device"""

        self._data = self.data.to(device)

    def unload(self) -> None:
        """Remove data and labels from memory; they will be reloaded on access"""

        logger.debug("%s: clearing `data` and `labels` from memory", self)
        self._data = None
        self._labels = None


def _sanity_check():
    """Sanity checks"""

    from constants import PROCESSED_IMAGE_DIR

    print("PlanetDataset")
    print("-------------")

    # Creation + loading
    data_path = PROCESSED_IMAGE_DIR / "RGB_PS2.SD_clear.pt"
    prepared = PlanetDataset(data_path)
    assert not prepared.is_loaded()
    assert prepared._data is None
    assert prepared._labels is None
    print("prepared:", prepared)
    assert not prepared.is_loaded()
    assert prepared._data is None
    assert prepared._labels is not None
    print("prepared.data:", prepared.data.shape)
    assert prepared.data.shape == (318, 3, 334, 334)
    assert prepared.data.dtype == torch.float32
    assert prepared.is_loaded()
    assert prepared._data is not None
    assert prepared._labels is not None
    print("prepared.labels:\n", prepared.labels, sep=None)
    print("prepared[0, :, 0:3, 0:3]:\n", prepared[0, :, 0:3, 0:3])

    # Unloading
    prepared.unload()
    assert not prepared.is_loaded()
    assert prepared._data is None
    assert prepared._labels is None

    # Transform
    assert prepared[0:2].shape == (2, 3, 334, 334)
    prepared.transform = lambda x: x[:, 0:1, 0:3, 0:5]
    assert not prepared.is_loaded()
    assert prepared[0:2].shape == (2, 1, 3, 5)
    prepared.transform = None
    assert not prepared.is_loaded()
    assert prepared[0:2].shape == (2, 3, 334, 334)

    # Subset on creation by list
    print()
    idx = list(range(2, 12))
    subset = PlanetDataset(data_path, idx=idx)
    print("subset:", subset)
    assert not subset.is_loaded()
    print("subset.data:", subset.data.shape)
    assert subset.data.shape == (10, 3, 334, 334)
    assert subset.data.dtype == torch.float32
    assert subset.labels["file_idx"].to_list() == idx
    assert subset.file_idx == idx
    print("subset.labels:\n", subset.labels, sep=None)

    # Subset on creation by pd.Index, pd.Series, slice, or int
    subset = PlanetDataset(data_path, idx=pd.Index(idx))
    assert subset.labels["file_idx"].to_list() == idx
    subset = PlanetDataset(data_path, idx=pd.Series(idx))
    assert subset.labels["file_idx"].to_list() == idx
    subset = PlanetDataset(data_path, idx=slice(2, 12))
    assert subset.labels["file_idx"].to_list() == idx
    subset = PlanetDataset(data_path, idx=2)
    assert subset.labels["file_idx"].to_list() == [2]

    # Subset existing dataset
    print()
    query = "date.dt.month == 6"
    subset = PlanetDataset(data_path, idx=idx)
    new_file_idx = subset.labels.query(query)["file_idx"].to_list()
    subset2 = subset.subset(query)
    print("subset of subset:", subset2)
    assert not subset2.is_loaded()
    print("subset of subset data:", subset2.data.shape)
    assert subset2.data.shape == (5, 3, 334, 334)
    assert subset2.data.dtype == torch.float32
    assert subset2.labels["file_idx"].to_list() == new_file_idx
    assert torch.equal(subset2[0], subset[2])
    print("subset of subset labels:\n", subset2.labels, sep=None)

    # Subset existing dataset by range, pd.Index, pd.Series, slice, list[int], or int
    query = range(2, 7)
    new_file_idx = subset.labels["file_idx"].iloc[query].to_list()
    subset2 = subset.subset(query)
    assert subset2.labels["file_idx"].to_list() == new_file_idx
    subset2 = subset.subset(pd.Index(query))
    assert subset2.labels["file_idx"].to_list() == new_file_idx
    subset2 = subset.subset(pd.Series(query))
    assert subset2.labels["file_idx"].to_list() == new_file_idx
    subset2 = subset.subset(slice(2, 7))
    assert subset2.labels["file_idx"].to_list() == new_file_idx
    subset2 = subset.subset(list(query))
    assert subset2.labels["file_idx"].to_list() == new_file_idx
    subset2 = subset.subset(3)
    assert subset2.labels["file_idx"].to_list() == new_file_idx[1:2]

    # Transfer to a device
    assert subset.data.device.type == "cpu"
    subset.to("mps")
    assert subset.data.device.type == "mps"
    subset.to("cpu")
    assert subset.data.device.type == "cpu"


if __name__ == "__main__":
    _sanity_check()
