"""Dataset of mostly cloud-free Planet images"""

from pathlib import Path
from typing import Callable, Optional

from torchvision.transforms import Compose

from constants import PROCESSED_IMAGE_DIR
from datasets import PlanetDataset
from datasets.planet_dataset import IdxType
from transforms import GetChannels
from utils.planet import PLANET_RGB_CHANNELS, PLANET_REFLECTANCE_CHANNELS

PLANET_DATA_TYPES = ("RGB", "TOAR", "ATM")


class PlanetClear(PlanetDataset):
    """Dataset of mostly cloud-free Planet images

    For convienience: allows creating a PlanetDatset by specifying a data type and
    instrument rather than a path to a data file.
    """

    def __init__(
        self,
        data_type: str,
        instrument: str,
        *,
        channels: Optional[str] = None,
        transform: Optional[Callable] = None,
        idx: Optional[IdxType] = None,
    ):
        """
        Args:
            data_type: The Planet data type to use e.g. "RGB"
            instrument: The Planet instrument to use e.g. "PS2"
            channels: Channels to use (default: `None` = all data channels). Any other
                value sets a transform that selects the specified channels.
            transform: Optional transform to apply to the images (default: `None`).
                Applied after any channel selection.
            idx: Optional index identifying the items to load (default: `None`)
        """

        # Set self.channels so it can be used by CLI for naming experiment folders
        if channels is None:
            channels = self._get_data_channels(data_type)  # set for clarity
        self.channels = channels

        # Find data path
        data_path = self._get_data_path(data_type, instrument)

        # Combine channels transform (if any) with transform argument (if any)
        channels_transform = self._get_channels_transform(self.channels, data_type)
        if transform is None:
            transform = channels_transform
        elif channels_transform is not None:
            transform = Compose([channels_transform, transform])

        # Init dataset
        super().__init__(data_path=data_path, transform=transform, idx=idx)

    def _get_data_path(self, data_type: str, instrument: str) -> Path:
        """Return path to prepared data file of mostly cloud-free reflectance"""

        filename = f"{data_type}_{instrument}_clear.pt"
        return Path(PROCESSED_IMAGE_DIR / filename).resolve()

    def _get_channels_transform(
        self, channels: str | None, data_type: str
    ) -> Callable | None:
        """Return transform to select the specified channels, if needed"""

        if channels is None or channels == self._get_data_channels(data_type):
            return None

        return GetChannels(channels, data_type=data_type)

    def _get_data_channels(self, data_type: str) -> str:
        """Return the data channels based on self.data_type"""

        match data_type:
            case "RGB":
                return PLANET_RGB_CHANNELS
            case "TOAR":
                return PLANET_REFLECTANCE_CHANNELS
            case "ATM":
                return PLANET_REFLECTANCE_CHANNELS
        raise NotImplementedError(f"Don't know data channels of `data_type` {data_type}")

    # Do not override super().subset() to return a PlanetClear instance as this would
    # cause transform to be duplicated or dropped.
    #
    # If subset() calls self.__class__() and does not pass transform, then any transform
    # is dropped:
    # >>> bg = PlanetClear("TOAR", channels="RGB")
    # >>> bg.transform
    # >>> # -> GetChannels(RGB, data_type=TOAR)
    # >>> bg.transform = Compose([bg.transform, lambda x: 2 * x])
    # >>> bg.subset(range(5)).transform
    # >>> # -> GetChannels(RGB, data_type=TOAR)
    #
    # If subset() calls self.__class__() and passes transform, then __init__() needs to
    # allow transform arg and compose it with any channel selection, AND subsetting then
    # results in repeated channel selection:
    # >>> bg = PlanetClear("TOAR", channels="RGB")
    # >>> bg.subset(range(5)).transform
    # >>> # -> Compose(GetChannels(...), GetChannels(...))


def _sanity_check():
    """Sanity checks"""

    print("PlanetClear")
    print("-----------")
    print("RGB")
    rgb = PlanetClear("RGB", "PS2.SD", idx=range(10))
    assert len(rgb) == 10
    print("rgb:", rgb)
    assert rgb.data_path == PROCESSED_IMAGE_DIR / "RGB_PS2.SD_clear.pt"
    assert rgb.data_types == ["RGB"]
    assert rgb.instruments == ["PS2.SD"]
    assert rgb.transform is None
    assert not rgb.is_loaded()
    assert rgb.data.shape == (10, 3, 334, 334)
    del rgb

    print()
    print("TOAR")
    toar = PlanetClear("TOAR", "PS2.SD", idx=range(10))
    print("toar:", toar)
    assert toar.data_path == PROCESSED_IMAGE_DIR / "TOAR_PS2.SD_clear.pt"
    assert toar.data_types == ["TOAR"]
    assert toar.transform is None
    assert not toar.is_loaded()
    assert toar.data.shape == (10, 4, 334, 334)
    del toar

    print()
    print("ATM")
    atm = PlanetClear("ATM", "PS2.SD", idx=range(10))
    print("atm:", atm)
    assert atm.data_path == PROCESSED_IMAGE_DIR / "ATM_PS2.SD_clear.pt"
    assert atm.data_types == ["ATM"]
    assert atm.transform is None
    del atm

    # Channels are set for clarity
    assert PlanetClear("TOAR", "PS2.SD").channels == "BGRI"

    # No data transform if channels == DATA_CHANNELS
    assert PlanetClear("RGB", "PS2.SD", channels="RGB").transform is None
    assert PlanetClear("TOAR", "PS2.SD", channels="BGRI").transform is None
    assert PlanetClear("ATM", "PS2.SD", channels="BGRI").transform is None

    # Can easily subset / rearrange channels
    print()
    print("Selecting channels")
    bgr = PlanetClear("RGB", "PS2.SD", channels="BGR", idx=range(10))
    print("bgr:", bgr)
    assert isinstance(bgr.transform, GetChannels)
    assert bgr.transform.channels == "BGR"
    assert bgr.transform.data_type == "RGB"
    del bgr

    transform = PlanetClear("TOAR", "PS2.SD", channels="BGR").transform
    assert isinstance(transform, GetChannels)
    assert transform.channels == "BGR"
    assert transform.data_type == "TOAR"

    transform = PlanetClear("ATM", "PS2.SD", channels="BGR").transform
    assert isinstance(transform, GetChannels)
    assert transform.channels == "BGR"
    assert transform.data_type == "ATM"

    # Can specify a transform, which is applied after any channel selection
    mult2 = lambda x: x * 2
    transform = PlanetClear("RGB", "PS2.SD", transform=mult2).transform
    assert transform == mult2
    transform = PlanetClear("RGB", "PS2.SD", channels="BG", transform=mult2).transform
    assert isinstance(transform, Compose)
    assert len(transform.transforms) == 2
    assert transform.transforms[0].channels == "BG"
    assert transform.transforms[0].data_type == "RGB"
    assert transform.transforms[1] == mult2
    del transform

    # Can subset the dataset, returning PlanetDataset
    # Returning PlanetClear is hard to implement without dropping or repeating transforms
    print()
    subset = PlanetClear("RGB", "PS2.SD").subset(range(3))
    assert isinstance(subset, PlanetDataset)
    assert len(subset) == 3
    print("rgb.subset(range(3)):", subset)

    # Subsetting does not drop or repeat transforms
    subset = PlanetClear("RGB", "PS2.SD", channels="BG").subset(range(3))
    assert isinstance(subset.transform, GetChannels)
    assert subset.transform.channels == "BG"
    assert subset.transform.data_type == "RGB"
    subset = PlanetClear("RGB", "PS2.SD", transform=mult2).subset(range(3))
    assert subset.transform == mult2
    subset = PlanetClear("RGB", "PS2.SD", channels="BG", transform=mult2).subset(range(3))
    assert isinstance(subset.transform, Compose)
    assert len(subset.transform.transforms) == 2
    assert subset.transform.transforms[0].channels == "BG"
    assert subset.transform.transforms[0].data_type == "RGB"
    assert subset.transform.transforms[1] == mult2


if __name__ == "__main__":
    _sanity_check()
