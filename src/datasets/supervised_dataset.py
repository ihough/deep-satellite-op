"""Dataset for supervised learning"""

from typing import Callable, Optional

from pandas import DataFrame, Series
import torch
from torch import Tensor
from torch.utils.data import Dataset

from datasets import PlanetDataset


class SupervisedDataset(Dataset):
    """Dataset for supervised learning"""

    def __init__(
        self,
        target: Series,
        image_dataset: Optional[PlanetDataset] = None,
        transform: Optional[Callable] = None,
        normalize: Optional[Callable] = None,
        extra: Optional[DataFrame] = None,
    ) -> None:
        """
        Args:
            target: `Series` containing the values of the target air quality metric
            image_dataset: Optional `PlanetDataset` (default: `None`). If `None` then no
                images will be used, only `extra_features`.
            transform: Optional transform e.g. to augment images (default: `None`)
            normalize: Optional normalization for images (default: `None`). Applied after
                `transform`.
            extra: Optional `DataFrame` containing additional features for predicting
                `target` (default: `None`)

        Returns:
            (image, extra, target, idx):
                image: Image, possibly augmented, possibly normalized, or empty tensor
                extra: Extra data, or empty tensor
                target: The target value for the sample
                idx: The sample's index
        """

        super().__init__()

        if image_dataset is None and extra is None:
            raise ValueError("Specify one or both of `image_dataset`, `extra`")
        if image_dataset is None and (transform is not None or normalize is not None):
            raise ValueError(
                "Do not specify `normalize` or `transform` if `image_dataset` is `None`"
            )

        # Coerce target values to tensor; use float32 for MPS compatiblity
        self.target_name = target.name
        self.target = torch.as_tensor(target.to_numpy(dtype="float32"))

        # If no image_dataset, make it an empty tensor
        if image_dataset is None:
            image_dataset = torch.empty((len(self.target), 0))
        self.image_dataset = image_dataset

        self.transform = transform
        self.normalize = normalize

        # If no extra data, make it an empty tensor
        # Otherwise, coerce to tensor; use float32 for MPS compatibility
        if extra is None:
            self.extra = torch.empty((len(self.target), 0))
            self.extra_names = None
        else:
            self.extra_names = extra.columns.to_list()
            self.extra = torch.as_tensor(extra.to_numpy(dtype="float32"))

    def __len__(self) -> int:
        return len(self.target)

    def __repr__(self) -> str:
        desc = f"{self.__class__.__name__}("
        desc += f"\n  target: {self.target_name} ({len(self)} values)"
        desc += f"\n  image_dataset: {self.image_dataset}"
        if self.transform is not None:
            desc += "\n  transform: " + str(self.transform).replace("\n)", "\n  )")
        if self.normalize is not None:
            desc += f"\n  normalize: {self.normalize}"
        extra_desc = self.extra_names
        if extra_desc is not None and len(extra_desc) > 6:
            extra_desc = ", ".join(extra_desc[0:2] + ["..."] + extra_desc[-2:])
            extra_desc = f"{len(self.extra_names)} features [{extra_desc}]"
        desc += f"\n  extra: {extra_desc}"
        desc += "\n)"
        return desc

    def __getitem__(self, idx: int | list[int]) -> tuple[Tensor, Tensor, Tensor, Tensor]:
        """Returns image (if any), extra data (if any), target value, index"""

        # Get image(s), if any
        image = self.image_dataset[idx]
        if self.transform is not None:
            image = self.transform(image)  # possibly transform
        if self.normalize is not None:
            image = self.normalize(image)  # possibly normalize

        # Return idx as tensor so that lightning can automatically infer batch size
        return image, self.extra[idx], self.target[idx], torch.as_tensor(idx)

    # Allow loading of entire batch at once
    __getitems__ = __getitem__


def _sanity_check():
    """Sanity checks"""

    from torch.utils.data import DataLoader
    from torchvision.transforms import CenterCrop
    from datasets import PlanetClear

    print("SupervisedDataset")
    print("-----------------")
    image_dataset = PlanetClear("RGB", "PS2.SD", idx=range(10))
    image_dataset = image_dataset.subset(image_dataset.labels["pm10"].dropna().index)
    supervised = SupervisedDataset(
        target=image_dataset.labels["pm10"], image_dataset=image_dataset
    )
    print(supervised)

    # Images are not loaded until accessed
    assert supervised.image_dataset.is_loaded() is False
    torch.manual_seed(0)
    img, extra, target_val, idx = supervised[0]
    assert supervised.image_dataset.is_loaded() is True
    assert img.shape == (3, 334, 334)
    assert img.dtype == torch.float32
    assert torch.equal(extra, torch.empty(0))
    assert target_val.shape == ()
    assert target_val.dtype == torch.float32
    assert idx == 0
    print("supervised[0]:", img.shape, extra, target_val, idx)

    dl = DataLoader(supervised, batch_size=4, collate_fn=lambda x: x)
    batch = next(iter(dl))
    print("contrastive batch:", batch[0].shape, batch[1], batch[2].shape, batch[3])

    # Images can be augmented
    supervised.transform = CenterCrop(100)
    aug, _, _, _ = supervised[0]
    assert aug.shape == (3, 100, 100)

    # Any image normalization is applied *after* augmentation
    supervised.transform = lambda x: 2 * x
    supervised.normalize = lambda x: (x - 100) / 50
    torch.manual_seed(0)
    aug_norm, _, _, _ = supervised[0]
    assert torch.equal(aug_norm, supervised.normalize(supervised.transform(img)))

    # Augmentation and normalization do not affect extra data
    extra_feats = image_dataset.labels[["t2m", "rh"]]
    supervised = SupervisedDataset(
        target=image_dataset.labels["pm10"],
        image_dataset=image_dataset,
        transform=lambda x: 2 * x,
        normalize=lambda x: (x - 100) / 50,
        extra=extra_feats,
    )
    aug_norm, extra, _, _ = supervised[0]
    assert torch.equal(extra, torch.as_tensor(extra_feats.iloc[0].to_numpy()))

    # Can have images and extra data
    supervised = SupervisedDataset(
        target=image_dataset.labels["pm10"],
        image_dataset=image_dataset,
        extra=image_dataset.labels[["doy", "t2m"]],
    )
    print()
    print(supervised)
    img, extra, target_val, idx = supervised[0]
    assert supervised.image_dataset.is_loaded() is True
    assert img.shape == (3, 334, 334)
    assert img.dtype == torch.float32
    assert extra.shape == (2,)
    assert extra.dtype == torch.float32
    assert target_val.shape == ()
    assert target_val.dtype == torch.float32
    assert torch.equal(idx, torch.as_tensor(0))
    print("supervised[0]:", img.shape, extra, target_val, idx)

    dl = DataLoader(supervised, batch_size=4, collate_fn=lambda x: x)
    batch = next(iter(dl))
    print("supervised batch:", batch[0].shape, batch[1].shape, batch[2].shape, batch[3])

    # image dataset can be None
    supervised = SupervisedDataset(
        target=image_dataset.labels["pm10"],
        image_dataset=None,
        extra=image_dataset.labels[["doy"]],
    )
    print()
    print(supervised)
    img, extra, target_val, idx = supervised[0]
    assert torch.equal(img, torch.empty(0))
    assert extra.shape == (1,)
    assert extra.dtype == torch.float32
    assert target_val.shape == ()
    assert target_val.dtype == torch.float32
    assert torch.equal(idx, torch.as_tensor(0))
    print("supervised[0]:", img, extra, target_val, idx)

    dl = DataLoader(supervised, batch_size=4, collate_fn=lambda x: x)
    batch = next(iter(dl))
    print("supervised batch:", batch[0], batch[1].shape, batch[2].shape, batch[3])


if __name__ == "__main__":
    _sanity_check()
