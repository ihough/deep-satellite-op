"""Ensure an argument's value(s) is in a list of allowed values"""

from collections.abc import KeysView
from typing import TypeVar

T = TypeVar("T")


def validate_arg(
    arg: T, allowed: set | KeysView, arg_name: str = "arg", as_list: bool = False
) -> T | list[T]:
    """Ensure an argument's value(s) is in a set of allowed values

    Args:
        arg: The argument to be checked. If list, each element is checked.
        allowed: Allowed values. Use dict keys if order matters.
        arg_name: Name of the argument (for error message)
        as_list: Whether to return `arg` as a list (default: False)

    Returns:
        * `[arg]` if `ensure_list` is True and `arg` is not a list
        * `arg` otherwise

    Raises:
        ValueError: If `arg` or any element of it is not in `allowed`
    """

    def check(val) -> None:
        if val not in allowed:
            raise ValueError(f"Invalid `{arg_name}`: {val} is not in {allowed}")

    if isinstance(arg, list):
        for val in arg:
            check(val)
    else:
        check(arg)

    if as_list and not isinstance(arg, list):
        arg = [arg]

    return arg
