from typing import Optional

import numpy as np
from sklearn.metrics import mean_absolute_error

ArrayLike = np.ndarray


def mean_absolute_scaled_error(
    y_true: ArrayLike,
    y_pred: ArrayLike,
    *,
    sample_weight: Optional[ArrayLike] = None,
    multioutput: str = "uniform_average"
) -> float:
    """Mean absolute error divided by mean absolute deviation of true values"""

    mean_abs_err = mean_absolute_error(
        y_true, y_pred, sample_weight=sample_weight, multioutput=multioutput
    )
    mean_abs_dev = np.mean(np.abs(y_true - np.mean(y_true)))
    return mean_abs_err / mean_abs_dev


def normalized_mean_absolute_error(
    y_true: ArrayLike,
    y_pred: ArrayLike,
    *,
    sample_weight: Optional[ArrayLike] = None,
    multioutput: str = "uniform_average"
) -> float:
    """Mean absolute error divided by mean true value"""

    mean_abs_err = mean_absolute_error(
        y_true, y_pred, sample_weight=sample_weight, multioutput=multioutput
    )
    mean_y_true = np.mean(y_true)
    return mean_abs_err / mean_y_true
