"""Helpers for imputing missing data"""

import logging
from typing import Any, Callable, Optional
import warnings

import numpy as np
import optuna
from optuna.distributions import BaseDistribution
from optuna.integration import OptunaSearchCV
from optuna.samplers import TPESampler
from pandas import DataFrame, Series, concat
from sklearn.base import BaseEstimator, clone
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics._scorer import _BaseScorer, mean_squared_error
from sklearn.model_selection import BaseCrossValidator, cross_validate


_logger = logging.getLogger(__name__)


class MyOptunaSearchCV(OptunaSearchCV):
    """Wrapper to control logging"""

    def fit(self, X, y=None, groups=None, **fit_params) -> OptunaSearchCV:
        """Fit after setting optuna root logging verbosity"""

        # Set optuna root logging level based on verbosity
        old_level = optuna.logging.get_verbosity()
        if self.verbose > 1:
            optuna.logging.set_verbosity(optuna.logging.DEBUG)
        elif self.verbose > 0:
            optuna.logging.set_verbosity(optuna.logging.INFO)
        else:
            optuna.logging.set_verbosity(optuna.logging.WARNING)

        # Fit
        super().fit(X, y, groups, **fit_params)

        # Restore original logging level
        optuna.logging.set_verbosity(old_level)

        return self


def make_circular(values: Series, scale: int | float) -> DataFrame:
    """Circularize a pandas Series by sine + cosine transforming"""

    scale = 2 / scale * np.pi
    return DataFrame(
        {
            f"{values.name}_sin": np.sin(scale * values),
            f"{values.name}_cos": np.cos(scale * values),
        },
        index=values.index,
    )


def fit_per_station(
    estimator: BaseEstimator,
    name: str,
    target: str,
    features: list[str] | dict[str, list[str]],
    data: DataFrame,
    cv: Optional[BaseCrossValidator] = None,
    group_col: Optional[str] = None,
    scoring: Optional[dict[str, _BaseScorer]] = None,
    hparam_space: Optional[dict[str, BaseDistribution]] = None,
    tune_kwargs: Optional[dict[str, Any]] = None,
    refit: str | bool = True,
    n_trials: int = 1,
    n_jobs: int = 1,
    rand_seed: Optional[int] = None,
    verbose: int = 0,
) -> tuple[dict[str, BaseEstimator | OptunaSearchCV], DataFrame, DataFrame]:
    """Fit and score a model for each station; features may vary per station"""

    if "station" not in data.index.names:
        raise ValueError(
            f"`data.index` does not include a 'station' level: {data.index.names}"
        )

    models = {}
    scores = []
    preds = []
    for stn in data.index.unique("station"):
        stn_feats = features
        if isinstance(stn_feats, dict):
            try:
                stn_feats = stn_feats[stn]
            except KeyError as err:
                raise KeyError("No `features` were provided for station {stn}") from err
        mask = data.index.get_level_values("station") == stn
        model, score, pred = fit_score_predict(
            estimator=estimator,
            name=name,
            target=target,
            features=stn_feats,
            data=data.loc[mask, :],
            cv=cv,
            scoring=scoring,
            group_col=group_col,
            hparam_space=hparam_space,
            tune_kwargs=tune_kwargs,
            refit=refit,
            n_trials=n_trials,
            n_jobs=n_jobs,
            rand_seed=rand_seed,
            verbose=verbose,
        )
        models[stn] = model
        scores.append(score.assign(station=stn))
        preds.append(pred)

    return models, concat(scores), concat(preds)


def fit_score_predict(
    estimator: BaseEstimator,
    name: str,
    target: str,
    features: list[str],
    data: DataFrame,
    cv: Optional[BaseCrossValidator] = None,
    group_col: Optional[str] = None,
    scoring: Optional[dict[str, _BaseScorer]] = None,
    hparam_space: Optional[dict[str, BaseDistribution]] = None,
    tune_kwargs: Optional[dict[str, Any]] = None,
    refit: str | bool = True,
    n_trials: int = 1,
    n_jobs: int = 1,
    rand_seed: Optional[int] = None,
    verbose: int = 0,
) -> tuple[BaseEstimator | OptunaSearchCV, DataFrame, DataFrame]:
    """Fit a model for each station and return cross-validation scores and predictions"""

    if group_col is None:
        group_col = []
    else:
        group_col = [group_col]

    # Drop NAs and reindex index
    id_cols = data.index.names
    data = data[group_col + [target] + features].dropna().reset_index()

    # Get groups if any
    groups = None
    if any(group_col):
        groups = data[group_col]

    # Define tuner
    tuner = estimator
    fit_params = {}
    if hparam_space is not None:
        # Muffle optuna info logs and warning about experimental module
        optuna.logging.set_verbosity(optuna.logging.WARNING)
        warnings.filterwarnings(
            "ignore", "OptunaSearchCV", optuna.exceptions.ExperimentalWarning
        )
        study = optuna.create_study(
            direction="maximize", sampler=TPESampler(seed=rand_seed)
        )
        # OptunaSearchCV can only handle a single score
        optuna_scoring = scoring
        if scoring is not None:
            try:
                optuna_scoring = scoring[refit]
            except KeyError as err:
                raise ValueError(f"`refit` is not a key of `scoring`: {refit}") from err
        if tune_kwargs is None:
            tune_kwargs = {}
        tuner = MyOptunaSearchCV(
            estimator=estimator,
            param_distributions=hparam_space,
            cv=cv,
            error_score="raise",
            n_trials=n_trials,
            random_state=rand_seed,
            scoring=optuna_scoring,
            study=study,
            verbose=verbose,
            **tune_kwargs,
        )
        if groups is not None:
            fit_params["groups"] = groups

    # Cross-validate, tuning within each fold
    cv_result = cross_validate(
        estimator=tuner,
        X=data[features],
        y=data[target],
        groups=groups,
        scoring=scoring,
        cv=cv,
        n_jobs=n_jobs,
        verbose=verbose,
        fit_params=fit_params,
        return_train_score=True,
        return_estimator=True,
        return_indices=True,
        error_score="raise",
    )

    # Process scores
    cv_models = cv_result.pop("estimator")
    cv_splits = cv_result.pop("indices")
    scores = DataFrame(cv_result).assign(model=name).set_index("model")

    # Unflip sign of scores where lower is better
    if scoring is not None:
        for score_name, scorer in scoring.items():
            cols = scores.columns[scores.columns.str.endswith("_" + score_name)]
            scores[cols] = scorer._sign * scores[cols]
    scores.rename(columns=lambda x: x.replace("test_", ""), inplace=True)

    # Predict for each test set
    preds = data.loc[:, id_cols + group_col + [target]]
    for model, test_idx in list(zip(cv_models, cv_splits["test"])):
        preds.loc[test_idx, "pred"] = model.predict(data.loc[test_idx, features])
    preds = preds.assign(model=name).set_index(["model"] + id_cols)

    # Tune and fit on all data
    fitted = clone(tuner)
    fitted.fit(data[features], data[target], **fit_params)

    return fitted, scores, preds


def impute_per_station(
    estimators: dict[str, BaseEstimator],
    target: str,
    data: DataFrame,
    max_iters: int = 25,
    decimals: int = 5,
    # set_flag: bool = False,
    verbose: int = 0,
) -> Series:
    """Use a fitted model for each station to imput missing values"""

    imputed = []
    for station, imputer in estimators.items():
        imputed.append(
            impute_missing(
                imputer,
                target=target,
                data=data.loc[(slice(None), station), :],
                max_iters=max_iters,
                decimals=decimals,
                # set_flag=set_flag,
                verbose=verbose,
            )
        )

    return concat(imputed, axis="rows").sort_index()


def impute_missing(
    estimator: BaseEstimator,
    target: str,
    data: DataFrame,
    max_iters: int = 25,
    decimals: int = 5,
    # set_flag: bool = False,
    verbose: int = 0,
) -> Series:
    """Use a fitted model to impute missing values"""

    orig_log_level = _logger.getEffectiveLevel()
    if verbose > 1:
        _logger.setLevel(logging.DEBUG)
    elif verbose > 0:
        _logger.setLevel(logging.INFO)
    else:
        _logger.setLevel(logging.WARNING)

    orig_index = data.index.names
    data = data.reset_index()
    features = list(estimator.feature_names_in_)

    # Locate missing values
    missing_idx = data.loc[data[target].isna()].index
    # if set_flag:
    #     imp_flag = f"{target}_imp"
    #     data[imp_flag] = False

    # Impute until no more missing
    i = 0
    while len(missing_idx) > 0:
        # Avoid infinite loop
        if i == max_iters:
            raise TimeoutError(
                f"{target} still missing {len(missing_idx)} values after {max_iters}"
                " rounds of imputation. Try increasing `max_iters`."
            )

        # Impute target
        impute_data = data.loc[missing_idx, features].dropna()
        try:
            data.loc[impute_data.index, target] = estimator.predict(impute_data)
        except ValueError as err:
            if str(err).startswith("Found arary with 0 sample(s)"):
                raise ValueError("Some features are missing from `data`") from err
        # if set_flag:
        #     data.loc[impute_data.index, imp_flag] = True

        # Update any lag variables
        lag_cols = data.filter(regex=r"^lag\d").columns.to_list()
        for lag_col in lag_cols:
            period = int(lag_col.replace("lag", ""))
            lagged = (
                data.loc[impute_data.index, ["date", "station", target]]
                .set_index("date")
                .shift(period, freq="D")
                .set_index("station", append=True)
                .rename(columns={target: lag_col})
                .dropna()
            )
            lagged = data[["date", "station"]].join(
                lagged, on=["date", "station"], how="right"
            )
            data.loc[lagged.index, lag_col] = lagged[lag_col]

        # Update missing and increment counter
        missing_idx = data.loc[data[target].isna()].index
        i += 1
        _logger.info(
            "Round %s: imputed %s missing values for %s; %s remain",
            i,
            len(impute_data.index),
            target,
            len(missing_idx),
        )
        _logger.debug("Remaining missing:")
        _logger.debug(data.loc[missing_idx, ["date", "station", target] + lag_cols])

    _logger.setLevel(orig_log_level)

    return data.set_index(orig_index)[target].round(decimals)


def miss_forest(
    data: DataFrame,
    max_iters: int = 10,
    n_trees: int = 100,
    scoring: Optional[Callable] = None,
    n_jobs: int = 1,
    rand_seed: Optional[int] = None,
    x_true: Optional[DataFrame] = None,
    verbose: int = 0,
) -> tuple[DataFrame, Series, Series]:
    """Iteratively impute missing values by random forest

    Based on [missForest](https://github.com/stekhoven/missForest)

    Returns:
        Tuple(
            Imputed DataFrame,
            Out-of-bag score,
            True score (empty unless x_true was provided)
        )
    """

    if scoring is None:
        scoring = mean_squared_error  # Use MSE if no scoring specified

    # Define RF imputers for numeric and categorical features
    imputer = RandomForestRegressor(
        n_estimators=n_trees,
        max_features="sqrt",
        min_samples_leaf=5,
        oob_score=scoring,
        n_jobs=n_jobs,
        random_state=rand_seed,
    )

    # Identify columns with missing values; sort from low to high missingness
    cols_to_impute = data.isna().sum().replace(0, None).dropna().sort_values().index

    # Identify numerical and categorical columns to impute
    num_cols = data[cols_to_impute].select_dtypes(include="number").columns
    cat_cols = data[cols_to_impute].select_dtypes(exclude="number").columns

    # Don't allow imputing of categorical columns
    # For this to work would need to implement using RandomForestClassifier or translate
    # RandomForestRegressor predictions from numeric to categorical
    if cat_cols.any():
        raise NotImplementedError(
            f"Cannot handle missing values in categorical columns {cat_cols}"
        )

    # Locate missing values
    missing_masks = data[cols_to_impute].isna()

    # Replace missing values with column mean / mode
    imputed = data.copy()
    if num_cols.any():
        imputed.loc[:, num_cols] = imputed[num_cols].fillna(imputed[num_cols].mean())
    if cat_cols.any():
        imputed.loc[:, cat_cols] = imputed[cat_cols].fillna(imputed[cat_cols].mode().T[0])

    # Iterate
    oob_scores = []
    true_scores = []
    i = 0
    num_diff = 0
    cat_diff = 0
    num_diff_old = np.inf
    cat_diff_old = np.inf
    while (num_diff < num_diff_old or cat_diff < cat_diff_old) and i < max_iters:
        if verbose > 0:
            print(f"missForest iteration {i + 1} ...")

        # Remember previous imputation
        imp_old = imputed.copy()

        # Update convergence criteria
        if i > 0:
            num_diff_old = num_diff
            cat_diff_old = cat_diff

        # Impute each column
        oob_score = dict()
        true_score = dict()
        for col in cols_to_impute:
            missing_mask = missing_masks[col]
            other_cols = imputed.columns.difference([col])

            # Train the imputer
            imputer.fit(
                X=imputed.loc[~missing_mask, other_cols],
                y=imputed.loc[~missing_mask, col],
            )

            # Impute missing values
            imputed.loc[missing_mask, col] = imputer.predict(
                imputed.loc[missing_mask, other_cols]
            )

            # Store OOB error
            oob_score[col] = imputer.oob_score_

            # Store true error if x_true was provided
            if x_true is not None:
                true_mask = missing_mask & x_true[col].notna()
                true_score[col] = scoring(
                    y_true=x_true.loc[true_mask, col],
                    y_pred=imputed.loc[true_mask, col],
                )

        # Collect scores
        oob_score = Series(oob_score)
        oob_scores.append(oob_score)
        if verbose > 0:
            print(f"  mean oob score: {oob_score.mean()}")
        true_score = Series(true_score)
        true_scores.append(true_score)
        if verbose > 0 and x_true is not None:
            print(f"  mean true score: {true_score.mean()}")

        # Calculate the difference relative to previous iteration
        if num_cols.any():
            # fmt:off
            num_diff = (
                imputed[num_cols].sub(imp_old[num_cols]).pow(2).sum().sum()
                / imputed[num_cols].pow(2).sum().sum()
            )
            # fmt:on
            if verbose > 0:
                print(f"  num diff: {num_diff}")
        if cat_cols.any():
            # fmt:off
            cat_diff = (
                imputed[cat_cols].ne(imp_old[cat_cols]).sum().sum()
                / data[cat_cols].isna().sum().sum()
            )
            # fmt:on
            if verbose > 0:
                print(f"  cat diff: {cat_diff}")

        # Increment iteration counter
        i += 1

    # Warn if max_iters reached
    if num_diff < num_diff_old or cat_diff < cat_diff_old:
        warnings.warn("Halting before convergence; consider increasing max_iters")

    # Get best imputation and corresponding error estimates
    best_idx = -2 if i < max_iters else -1
    imputed = (imputed, imp_old)[best_idx]
    oob_score = oob_scores[best_idx]
    true_score = true_scores[best_idx]

    return imputed, oob_score, true_score


def check_it():
    return None


if __name__ == "__main__":
    check_it()
