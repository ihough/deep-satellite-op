"""Assign data samples to train and test sets"""

from typing import Optional
from warnings import warn

from pandas import DataFrame, Series


def split_train_test(
    data: DataFrame | Series,
    test_frac: float,
    groups: Optional[str | Series] = None,
    shuffle: bool = False,
    rand_seed: Optional[int] = None,
) -> tuple[list[int], list[int]]:
    """Assign data samples to train and test sets

    Arguments:
        data: A pandas DataFrame or Series to be split
        test_frac: The fraction of samples to assign to the test set
        groups: Optional, the name of a column or a Series identifying groups.
            If specified all samples in each group are assigned to the same set.
        shuffle: Whether to shuffle samples (or groups) before splitting
        rand_seed: A random seed for shuffling if shuffle = True

    Returns:
        Two lists identifying position (iloc) of train and test samples

    Raises:
        ValueError if data is not DataFrame or Series
        ValueError unless 0 <= test_frac < 1
        KeyError if groups is a string that is not the name of a column
        ValueError if no data assigned to train set

    Warns:
        If test set is substantially (> 5% of n_samples) larger than requested
    """

    # Sanity check
    if not isinstance(data, DataFrame) and not isinstance(data, Series):
        raise ValueError(f"`data` must be pandas DataFrame or Series; got {type(data)}")
    if len(data.index) == 0:
        raise ValueError("`data` has no samples")
    if test_frac < 0 or test_frac >= 1:
        raise ValueError(f"`test_frac` must be in range [0, 1); got {test_frac}")

    # Calculate number of total, test, and train samples
    n_samples = len(data.index)
    n_test = round(test_frac * n_samples)
    n_train = n_samples - n_test

    # Ensure some data is left for training
    if n_train == 0:
        raise ValueError("No data left for train set; try decreasing `test_frac`")

    # Shortcut: if n_test is 0 assign all data to train
    if n_test == 0:
        train_idx = list(range(len(data)))
        test_idx = list()
        return train_idx, test_idx

    # Get the row number of each sample
    samples = Series(range(n_samples), name="sample")

    # Calculate number of samples in each group
    if groups is None:
        # Each sample is a group of 1
        group_sizes = Series(n_samples * [1], name="group")
    else:
        # Get group of each sample; reset index to avoid possible alignment issues
        if isinstance(groups, str):
            try:
                groups = data[groups]
            except KeyError as exc:
                raise KeyError(f"`data` does not have `groups` column {groups}") from exc
        groups = Series(groups).reset_index(drop=True)

        # Calculate number of samples in each group
        grouped = samples.groupby(groups, sort=False)
        group_sizes = grouped.size().reset_index(drop=True)

        # Index samples by group number
        samples.index = grouped.ngroup().rename("group")

    # Possibly shuffle before splitting
    if shuffle:
        group_sizes = group_sizes.sample(frac=1, random_state=rand_seed)

    # Assign the last groups totaling at least n_test samples to the test set
    mask = group_sizes.cumsum() > n_train
    test_groups = group_sizes[mask].index
    train_groups = group_sizes[~mask].index

    # Assign samples to train and test
    test_idx = samples.loc[test_groups].to_list()
    train_idx = samples.loc[train_groups].to_list()

    # Raise if train set contains no smaples (due to grouping)
    if not len(train_idx) > 0:
        raise ValueError("No data left for train set; try decreasing `test_frac`")

    # Warn if test set is larger than requested by > 5% of n_samples (due to grouping)
    actual = len(test_idx) / n_samples
    if actual - test_frac > 0.05:
        warn(f"`test_frac` was {test_frac} but test set contains {actual:.1%} of samples")

    return train_idx, test_idx
