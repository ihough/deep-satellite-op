"""Utility functions for reports"""

from pathlib import Path
import re
from typing import Literal, Optional
import yaml

from matplotlib.figure import Figure
from matplotlib.ticker import PercentFormatter
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.metrics import r2_score, mean_squared_error
import torch
from torchvision.transforms.functional import pad, resize

from constants import SEED
from data_modules import ContrastiveDataModule
from utils import normalized_mean_absolute_error, planet_plot, mean_absolute_scaled_error
from utils.planet import normalize_min_max


def bootstrap_scores(
    data: pd.DataFrame,
    true_col: str,
    pred_col: str = "pred",
    n_boot: int = 100,
    seed: Optional[int] = None,
) -> pd.DataFrame:
    """Return scores of bootstrap samples"""

    np.random.seed(seed)
    boots = []
    for _ in range(n_boot):
        sample = data.sample(frac=1, replace=True)
        boots.append(compute_scores(sample, true_col=true_col, pred_col=pred_col))
    boots = pd.DataFrame(boots)
    boots.index.name = "bootstrap"

    return boots


def compute_scores(df: pd.DataFrame, true_col: str, pred_col: str = "pred") -> pd.Series:
    """Compute scores: R2, RMSE, NMAE, Pearson + Spearman correlation"""

    def wilmott_disagreement(y_true, y_pred) -> float:
        true_mean = np.average(y_true)
        abs_deviation = np.sum(np.abs(y_true - true_mean))
        abs_error = np.sum(np.abs(y_true - y_pred))
        if abs_error <= 2 * abs_deviation:
            d = 1 - abs_error / (2 * abs_deviation)
        else:
            d = (2 * abs_deviation) / abs_error - 1
        return d

    scores = pd.Series(
        {
            "R2": r2_score(y_true=df[true_col], y_pred=df[pred_col]),
            "RMSE": mean_squared_error(
                y_true=df[true_col], y_pred=df[pred_col], squared=False
            ),
            "NMAE": normalized_mean_absolute_error(
                y_true=df[true_col], y_pred=df[pred_col]
            ),
            "MASE": mean_absolute_scaled_error(y_true=df[true_col], y_pred=df[pred_col]),
            "Rs": df[true_col].corr(df[pred_col], method="spearman"),
            "d": wilmott_disagreement(y_true=df[true_col], y_pred=df[pred_col]),
        }
    )

    return scores


def load_contrastive_metrics(experiment_dir: str | Path) -> pd.DataFrame:
    """Load metrics logged during a contrastive pretraining experiment

    Args:
        experiment_dir: Directory containing the experiment artifacts

    Returns:
        DataFrame of metrics
    """

    experiment_dir = Path(experiment_dir)
    if not experiment_dir.exists():
        raise ValueError(f"`experiment_dir` does not exist: {experiment_dir}")

    results = []
    for path in sorted(experiment_dir.rglob("metrics.csv")):
        metrics = pd.read_csv(path)

        # Aggregate per epoch
        metrics["epoch"] = metrics["epoch"].bfill()
        lr_col = metrics.filter(like="lr-").columns[0]
        metrics = (
            metrics.rename(columns={lr_col: "lr", "train_loss_epoch": "train_loss"})
            .groupby("epoch", as_index=False)
            .agg(
                {
                    "lr": "last",
                    "train_loss": "last",
                    "val_loss": "last",
                    "z1_sd": "mean",
                }
            )
        )

        # Add metadata
        metrics["dataset"] = path.parts[-5]
        metrics[["pretrain", "channels"]] = path.parts[-4].split("_")
        metrics["experiment"] = path.parts[-3]
        metrics["version"] = path.parts[-2]
        metrics["hparams"] = list(path.parent.glob("*/events*"))[0].parent.name

        results.append(metrics)
        del metrics

    return pd.concat(results, axis="rows")


def load_supervised_metrics(experiment_dir: str | Path) -> pd.DataFrame:
    """Load metrics logged during a supervised training experiment

    Args:
        experiment_dir: Directory containing the experiment artifacts

    Returns:
        DataFrame of metrics
    """

    experiment_dir = Path(experiment_dir)
    if not experiment_dir.exists():
        raise ValueError(f"`experiment_dir` does not exist: {experiment_dir}")

    results = []
    for path in sorted(experiment_dir.rglob("metrics.csv")):
        # Load metrics and aggregate per epoch
        metrics = pd.read_csv(path)
        metrics["epoch"] = metrics["epoch"].bfill()
        if "train_loss" in metrics.columns:
            # Old logs: drop train scores b/c per step not epochal mean
            lr_col = metrics.filter(like="lr-").columns.to_list()
            metrics = metrics.drop(
                columns=["mae/train", "mse/train", "train_loss"] + lr_col
            )
        else:
            metrics = metrics.drop(columns="train_loss_step")
        metrics = (
            metrics.rename(columns={"train_loss_epoch": "train_loss"})
            .rename(columns=lambda x: re.sub(r"(.+)\/(train|val|test)$", r"\2_\1", x))
            .groupby("epoch", as_index=False)
            .agg("last")
        )
        if "train_mse" in metrics.columns:
            metrics[["train_rmse", "val_rmse"]] = metrics[["train_mse", "val_mse"]] ** 0.5
        else:
            metrics[["train_rmse", "val_rmse"]] = (
                metrics[["train_loss", "val_mse"]] ** 0.5
            )

        # Load metadata
        metrics["target"] = path.parts[-6].replace("-", "_")
        metrics["dataset"] = path.parts[-5]
        metrics["backbone"] = path.parts[-4]
        metrics["experiment"] = path.parts[-3]
        metrics["version"] = path.parts[-2]
        metrics["hparams"] = list(path.parent.glob("*/events*"))[0].parent.name

        # Load meteorological variables, if any
        config = yaml.safe_load(path.with_name("config.yaml").read_text())
        extra_features = config["data"].get("extra_features")
        extra_features = "None" if extra_features is None else " ".join(extra_features)
        metrics["extra_features"] = extra_features

        results.append(metrics)
        del metrics

    return pd.concat(results, axis="rows")


def load_predictions(experiment_dir: str | Path) -> pd.DataFrame:
    """Load predictions of the best model of a supervised training experiment

    Args:
        experiment_dir: Directory containing one or more experiments

    Returns:
        DataFrame of predictions
    """

    experiment_dir = Path(experiment_dir)
    if not experiment_dir.exists():
        raise ValueError(f"`experiment_dir` does not exist: {experiment_dir}")

    predictions = []
    for path in sorted(experiment_dir.rglob("predictions/pred_best_*.csv")):
        # Load predictions
        preds = pd.read_csv(path, parse_dates=["date"])
        preds["set"] = pd.Categorical(preds["set"], categories=["train", "val", "test"])

        # Load metadata
        preds["target"] = path.parts[-7].replace("-", "_")
        preds["dataset"] = path.parts[-6]
        preds["backbone"] = path.parts[-5]
        preds["experiment"] = path.parts[-4]
        preds["version"] = path.parts[-3]
        preds["hparams"] = list(path.parents[1].glob("*/events*"))[0].parent.name

        # Load meteorological variables, if any
        config = yaml.safe_load(Path(path.parents[1], "config.yaml").read_text())
        extra_features = config["data"].get("extra_features")
        extra_features = "None" if extra_features is None else " ".join(extra_features)
        preds["extra_features"] = extra_features

        predictions.append(preds)
        del preds

    return pd.concat(predictions, axis="rows")


def plot_contrastive_batch(
    data_module: ContrastiveDataModule,
    batch_size: int = 6,
    station_dates: Optional[list[tuple[str, str]]] = None,
    label_images: bool = True,
    img_size: int = 150,
    value_range: Optional[tuple[float, float]] = None,
    seed: Optional[int] = None,
) -> Figure:
    "Plot images and augmented views"

    # Ensure img_size is even so that views can be padded to same size
    if img_size % 2 != 0:
        img_size += 1

    # Identify images to plot
    labels = data_module.image_dataset().labels
    if station_dates is None:
        idx = labels.sample(batch_size, random_state=seed).index.to_list()
    else:
        idx = (
            labels.reset_index()
            .set_index(["station", "date"])
            .loc[station_dates, "index"]
            .to_list()
        )

    # Make ContrastiveDataset of images to plot
    data_module.setup("train")
    dataset = data_module.train_dataset
    dataset.image_dataset = data_module.image_dataset().subset(idx)

    # Get images and augmented views
    images = resize(dataset.image_dataset.data, img_size, antialias=True)
    view1, view2, _ = data_module.train_dataset[range(len(idx))]
    views = torch.concat([view1, view2])
    views = pad(
        views, padding=(images.shape[-1] - views.shape[-1]) // 2, fill=views.max().item()
    )

    # Concatenate images and views
    batch = torch.concat([images, views])
    del view1, view2, views
    dataset.image_dataset.unload()  # clear image dataset from memory

    # Normalize images
    if value_range is None:
        if data_module.data_type == "RGB":
            value_range = (0, 255)
        elif data_module.data_type == "TOAR":
            value_range = (0, 0.3)
        elif data_module.data_type == "ATM":
            batch[:, 0] = normalize_min_max(batch[:, 0], value_range=(0.028, 0.072))
            batch[:, 1] = normalize_min_max(batch[:, 1], value_range=(0.004, 0.045))
            batch[:, 2] = normalize_min_max(batch[:, 2], value_range=(-0.017, 0.021))
            value_range = (0, 1)

    # Make labels
    labels = None
    if label_images:
        labels = dataset.image_dataset.labels
        labels = (
            labels["station"]
            + " "
            + labels["date"].astype(str)
            + "\nPM10: "
            + labels["pm10"].round(1).astype(str)
            + "\nOP AA: "
            + labels["op_aa25_m3"].round(2).astype(str)
            + "\nOP DTT: "
            + labels["op_dtt25_m3"].round(2).astype(str)
        )
        labels = labels.to_list() + [""] * 2 * len(labels)

    # Plot
    return planet_plot(
        batch,
        data_type=data_module.data_type,
        channels="RGB",
        labels=labels,
        nrow=3,
        normalize=True,
        value_range=value_range,
    )


def plot_loss_curves(
    data: pd.DataFrame,
    train_metric: str = "train_loss",
    val_metric: str = "val_loss",
    y_label: str = "Loss",
    ylims: Optional[tuple[float, float]] = None,
    title: Optional[str] = None,
    show_best_epoch: bool = True,
    tight_layout: bool = True,
    **kwargs,
) -> sns.FacetGrid:
    "Plot train and val loss curves"

    # Default plot args
    kwargs = (
        dict(
            hue="set",
            palette="Paired",
            kind="line",
            facet_kws={"legend_out": False},
            aspect=1.62,
            height=4,
        )
        | kwargs
    )

    # Loss curves
    p = sns.relplot(
        data.melt(
            id_vars="epoch",
            value_vars=[train_metric, val_metric],
            var_name="set",
            value_name=y_label,
        ),
        x="epoch",
        y=y_label,
        **kwargs,
    ).set_axis_labels("Epoch")

    # Dashed lines to identify epoch and value of best val loss
    if show_best_epoch:
        for ax in p.axes.flatten():
            best_epoch = data["epoch"].iloc[data[val_metric].argmin()]
            best_val = data[val_metric].min()
            ax.plot(
                [-2, best_epoch],
                [best_val, best_val],
                dashes=(1, 1),
                color="black",
                alpha=0.5,
            )
            ax.plot(
                [best_epoch, best_epoch],
                [0, best_val],
                dashes=(1, 1),
                color="black",
                alpha=0.5,
            )

    # Y-axis lims, title
    p.set(ylim=ylims)
    p.legend.set_title(None)
    p.figure.suptitle(title)
    if tight_layout:
        p.figure.tight_layout()

    return p


def plot_representation_sd(
    data: pd.DataFrame,
    out_dim: int = 2048,
    title: Optional[str] = None,
    ylim: Optional[tuple[float, float]] = None,
) -> Figure:
    """Plot contrastive sanity check metric z1_sd"""

    # Plot
    p = sns.relplot(
        data, x="epoch", y="z1_sd", kind="line", aspect=1.62, height=4
    ).set_axis_labels("Epoch", "Output SD")

    # Horizontal dashed line at value of z1_sd if outputs are normally distributed
    ref_value = 1 / out_dim**0.5
    p.refline(y=ref_value, color="black", alpha=0.5, linestyle="dashed")
    if ylim is None:
        ylim = (0, round(ref_value * 1.15, 3))
    p.set(ylim=ylim)

    p.figure.suptitle(title)

    return p


def plot_preds_scatter(
    data: pd.DataFrame,
    true_col: str,
    pred_col: str = "pred",
    true_desc: Optional[str] = None,
    facet_kws: Optional[dict] = None,
    **kwargs,
) -> sns.FacetGrid:
    "Plot observations vs. predictions"

    if true_desc is None:
        true_desc = true_col

    # Default plot args
    kwargs = (
        dict(col="set", hue="station", style="station", legend=False, alpha=0.5, height=3)
        | kwargs
    )

    p = (
        sns.relplot(
            data, x=true_col, y=pred_col, kind="scatter", facet_kws=facet_kws, **kwargs
        )
        .set_titles("{row_name} {col_name}" if "row" in kwargs else "{col_name}")
        .set_axis_labels(x_var=f"Observed {true_desc}", y_var=f"Estimated {true_desc}")
    )
    for ax in p.axes.flatten():
        ax.axline((0, 0), slope=1, linestyle="dashed", color="black", alpha=0.5)
        minval = min(ax.get_xlim()[0], ax.get_ylim()[0])
        maxval = max(ax.get_xlim()[1], ax.get_ylim()[1])
        ax.set_xlim(min(0, minval), maxval)
        ax.set_ylim(min(0, minval), maxval)

    return p


def plot_scores_bootstrap(
    data: pd.DataFrame,
    true_col: str,
    pred_col: str = "pred",
    by_model: bool = False,
    by_station: bool = False,
    scores: Optional[str | list[str]] = None,
    set_lims: bool = False,
    n_boot: int = 100,
    seed: Optional[int] = None,
    kind: Literal["point", "violin"] = "point",
    **kwargs,
) -> list[sns.FacetGrid]:
    """Plot bootstrap-estimated 95% CI of scores"""

    if isinstance(scores, str):
        scores = [scores]

    defaults = {
        "point": {
            "y": "model" if by_model else "set",
            "hue": "station" if by_station else "model" if by_model else "set",
            "dodge": 0.5 if by_station else None,
            "col": "set" if by_model else "metric",
            "col_order": ["train", "val", "test"] if by_model else None,
            "row": "metric" if by_model else None,
            "sharex": True if by_model else False,
            "errorbar": ("pi", 95),
            "linestyle": "none",
            "linewidth": 2,
            "aspect": 0.618,
            "height": 4 if by_model else 3,
        },
        "violin": {
            "y": "model" if by_model else "set",
            "hue": "station" if by_station else "model" if by_model else "set",
            "inner": "quart",
            "cut": 0,
            "col": "set" if by_model else "metric",
            "col_order": ["train", "val", "test"] if by_model else None,
            "row": "metric" if by_model else None,
            "sharex": True if by_model else False,
            "aspect": 0.618,
            "height": 5 if by_model else 4,
        },
    }
    try:
        kwargs[kind] = defaults[kind] | kwargs
    except KeyError as err:
        raise ValueError(f"kind must be 'point' or 'violin'; got '{kind}'") from err

    # Generate bootstraps
    groupby = ["set"]
    if by_station:
        groupby = ["station"] + groupby
    if by_model:
        groupby = ["model"] + groupby
    boots = data.groupby(groupby, observed=True, sort=False if by_model else True).apply(
        bootstrap_scores,
        true_col=true_col,
        pred_col=pred_col,
        n_boot=n_boot,
        seed=seed,
    )

    # Compute scores for a naive predictor (always predicts mean of train set target)
    # Add a small error to naive predictions to avoid error when calculating correlation
    np.random.seed(SEED)
    naive = data.loc[data["set"].eq("train")][true_col].mean()
    naive = naive + np.random.normal(scale=naive / 1e9, size=data.shape[0])
    naive_scores = compute_scores(
        data.set_index("station").assign(naive=naive).reset_index(),
        true_col=true_col,
        pred_col="naive",
    ).dropna()
    plots = []
    if scores is None:
        scores = boots.columns
    metric_groups = scores if by_model else [scores]
    for plot_metrics in metric_groups:
        # Plot
        p = (
            sns.catplot(
                boots.melt(
                    value_vars=plot_metrics, var_name="metric", ignore_index=False
                ).reset_index(),
                x="value",
                kind=kind,
                **kwargs[kind],
            )
            .set_axis_labels(x_var="", y_var="")
            .set_titles("{row_name} {col_name}" if by_model else "{col_name}")
        )

        for title, ax in p.axes_dict.items():
            metric = plot_metrics if by_model else title

            # Add vertical dashed line to indicate performance of naive mean predictor
            naive_score = naive_scores[metric]
            ax.axvline(x=naive_score, color="black", alpha=0.5, linestyle="dashed")

            # Possibly set x-axis limits
            if set_lims:
                xlims = ax.get_xlim()
                xmax = 1 if metric in ["R2", "R Pearson", "d"] else naive_score
                ax.set_xlim(min(0, xlims[0]), max(xmax, xlims[1]))

            if metric == "NMAE":
                ax.xaxis.set_major_formatter(PercentFormatter(1, decimals=0))

        if p.legend is not None:
            p.legend.set_title(None)

        plots.append(p)

    return plots


def make_scores_table(
    data: pd.DataFrame,
    true_col: str,
    pred_col: str = "pred",
    groupby: Optional[str | list[str]] = None,
    scores: Optional[str | list[str]] = None,
    precision: int = 2,
    formats: Optional[dict[str, str]] = None,
):
    """Compute scores and return a formatted table"""

    if groupby is None:
        groupby = "set"
    elif isinstance(groupby, str):
        groupby = ["set", groupby]
    else:
        groupby = ["set"] + groupby
    data = (
        data.groupby(groupby, observed=True, sort=False)
        .apply(compute_scores, true_col=true_col, pred_col=pred_col)
        .sort_index(level="set", sort_remaining=False)
    )

    if scores is not None:
        if isinstance(scores, str):
            scores = [scores]
        data = data[scores]

    if formats is None:
        formats = {}
    formats = {"NMAE": "{:.0%}"} | formats

    return data.style.format(formats, precision=precision)


def report_preds_timeplot(
    data: pd.DataFrame,
    target: str,
    target_alias: Optional[str] = None,
    legend_out: bool = True,
    **kwargs,
):
    """Return a plot showing timeseries of observed and predicted for each version"""

    if target_alias is None:
        target_alias = target

    data = data.rename(columns={target: "observed"}).melt(
        id_vars=["version", "period", "date", "station"],
        value_vars=["observed", "pred"],
        value_name=target_alias,
    )
    mask = (data["version"] == data["version"].iloc[0]) & (data["variable"] == "observed")
    data.loc[mask, "version"] = "observed"
    data = data.query("version == 'observed' or variable == 'pred'").reset_index(
        drop=True
    )

    p = sns.relplot(
        data,
        x="date",
        y=target_alias,
        hue="version",
        palette=["black"] + sns.color_palette(n_colors=data["version"].nunique() - 1),
        style="version",
        row="station",
        col="period",
        facet_kws=dict(sharex=False, legend_out=legend_out),
        kind="line",
        markers=True,
        aspect=2,
        height=3,
        **kwargs,
    )
    p.legend.set_title(None)
    p.set_titles("{row_name}")
    p.set_xlabels("")

    return p
