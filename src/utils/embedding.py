"""Helpers for representing meteorology as a high-dimensional embedding"""

from pathlib import Path
import pickle

from pandas import DataFrame
from sklearn.ensemble import RandomTreesEmbedding


def embed_met(met_data: DataFrame, embedder: RandomTreesEmbedding) -> DataFrame:
    """Return a high-dimensional representation of meteorology"""

    index = met_data.index
    met_data = (
        DataFrame(embedder.transform(met_data[embedder.feature_names_in_]))
        .astype(int)
        .rename(columns=lambda col: f"met{col:04}")
    )
    met_data.index = index
    return met_data


def load_met_embedder(path: Path) -> RandomTreesEmbedding:
    """Load a fitted unsupervised high-dimensional met data embedder"""

    return pickle.loads(path.read_bytes())
