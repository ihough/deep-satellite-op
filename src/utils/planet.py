"""Utility functions for Planet satellite data"""

from typing import Any, Optional
from warnings import warn

import numpy as np
import PIL.Image
import matplotlib.figure
import matplotlib.pyplot
import torch
from torchvision.utils import make_grid
from torchvision.transforms.functional import to_pil_image, resize

from utils import validate_arg


# Type aliases
PlanetChannelsType = str | int | list[int]
PlanetDataType = np.ndarray | torch.Tensor
PlanetShapeType = str | int | list[int]

# Constants
PLANET_RGB_CHANNELS = "RGB"
PLANET_REFLECTANCE_CHANNELS = "BGRI"
PLANET_SHAPE = "CHW"


# Based on torchvision.utils.make_grid
def normalize_min_max(
    tensor: torch.Tensor, value_range: Optional[tuple[float, float]] = None
) -> torch.Tensor:
    """Clamp values to [min, max] and shift to range [0.0, 1.0]

    Args:
        tensor: torch.Tensor
        value_range: Optional (min, max) values. If None (default) min and max
            are computed from `tensor`.
    """

    if not isinstance(tensor, torch.Tensor):
        raise TypeError(f"`tensor` must be a torch.Tensor; got {type(tensor)}")

    if value_range is not None and not isinstance(value_range, tuple):
        raise TypeError(f"`value_range` must be a tuple (min, max); got {value_range}")

    if value_range is None:
        value_range = tensor.min(), tensor.max()

    low, high = value_range
    return tensor.clamp(low, high).sub(low).div(max(high - low, 1e-5))


def planet_check_dimension(data: PlanetDataType) -> None:
    """Confirm data is 3D or 4D

    Raises:
        ValueError: If data has fewer than 3 or more than 4 dimensions
    """

    if data.ndim < 3 or data.ndim > 4:
        raise ValueError(f"`data` must have 3 or 4 dimensions; got {data.ndim}")


def planet_check_shape(data: PlanetDataType) -> None:
    """Confirm data shape is [Images] x Channels x Height x Width

    Raises:
        ValueError: If data has fewer than 3 or more than 4 dimensions
        ValueError: If number of channels > 4, suggesting data has been reshaped
    """

    planet_check_dimension(data)
    n_channels = data.shape[-3]
    if n_channels > len(PLANET_REFLECTANCE_CHANNELS):
        raise ValueError(
            f"`data` has {n_channels} channels (> {len(PLANET_REFLECTANCE_CHANNELS)});"
            " has it been reshaped?"
        )


def planet_get_channels(
    data: PlanetDataType, channels: PlanetChannelsType, data_type: str
) -> PlanetDataType:
    """Get specified channels (bands) of Planet data

    Args:
        data: The data (torch.Tensor or np.ndarray)
        channels: Desired channels e.g. "RGB" or [2, 1, 0] or "R" or 2
        data_type: The type of Planet data (RGB / TOAR / SR / ATM)

    Returns:
        np.ndarray or torch.Tensor (3D or 4D)

    Raises:
        ValueError: If `channels` is invalid
    """

    # Identify the data's channel order
    match data_type:
        case "RGB":
            data_channels = PLANET_RGB_CHANNELS
        case "TOAR":
            data_channels = PLANET_REFLECTANCE_CHANNELS
        case "SR":
            data_channels = PLANET_REFLECTANCE_CHANNELS
        case "ATM":
            data_channels = PLANET_REFLECTANCE_CHANNELS
        case _:
            raise ValueError(f"Unrecognized `data_type`: {data_type}")
    channel_index = list(range(len(data_channels)))

    # Parse requested channels
    match channels:
        case str():
            # e.g. "RGB" -> [2, 1, 0]
            channel_dict = dict(zip(data_channels, channel_index))
            channels = list(channels.upper())  # ignore case
            channels = validate_arg(channels, allowed=data_channels, arg_name="channels")
            channels = [channel_dict[b] for b in channels]
        case int() | list():
            # Ensure all are valid channel indexes
            # Ensure channels is list -> don't drop a dimension if 1 channel requested
            # e.g. if data is batch of shape 2 x 4 x 100 x 100 then channels = [0] returns
            # shape 2 x 1 x 100 x 100 while channels = 0 would return shape 2 x 100 x 100
            channels = validate_arg(
                channels, allowed=channel_index, arg_name="channels", as_list=True
            )
        case _:
            raise ValueError(f"Unrecognized `channels`: {channels}")

    # Confirm the data has not been reshaped (i.e. channels is -3rd dimension)
    planet_check_shape(data)

    # Rearrange the channels
    try:
        if isinstance(data, torch.Tensor):
            channels = torch.as_tensor(channels, device=data.device)
            data = torch.index_select(data, dim=-3, index=channels)
        else:
            data = np.take(data, channels, axis=-3)
    except IndexError as exc:
        raise IndexError(
            f"`data` has {data.shape[0]} channels; cannot get `channels` {channels}"
        ) from exc

    return data


def planet_reshape(data: PlanetDataType, shape: PlanetShapeType) -> PlanetDataType:
    """Reshapes Planet data

    Original shape of Planet data is Channels (Bands) x Height x Width

    Args:
        data: The data to reshape
        shape: Output shape e.g. "HWC" or [1, 2, 0]. Use "C" ("channels") rather
            than "B" ("bands") since in torch "B" refers to the batch dimension
            of 4D data e.g. Batches (Images) x Channels x Height x Width

    Returns:
        np.ndarray or torch.Tensor (3D or 4D) with the specified shape. First
        dimension of 4D data is assumed to be batch dimension and is ignored
        e.g. reshape(<B x C x H x W>, "HWC") -> <B x H x W x C>

    Raises:
        ValueError: If `shape` is invalid
    """

    shape_index = list(range(len(PLANET_SHAPE)))

    # Parse shape spec
    match shape:
        case str():
            # e.g. "HWC" -> [1, 2, 0]
            shape_dict = dict(zip(PLANET_SHAPE, shape_index))
            shape = list(shape.upper())  # ignore case
            shape = validate_arg(shape, allowed=PLANET_SHAPE, arg_name="shape")
            shape = [shape_dict[d] for d in shape]
        case list():
            # Ensure all are valid dimension indexes
            shape = validate_arg(shape, shape_index, "shape")
        case _:
            raise ValueError(f"Unrecognized `shape`: {shape}")

    # Validate shape spec
    if len(shape) != len(PLANET_SHAPE):
        raise ValueError(
            f"`shape` must have {len(PLANET_SHAPE)} dimensions; got {len(shape)}"
        )
    if len(shape) != len(set(shape)):
        raise ValueError(f"Duplicate dimension in `shape` {shape}")

    # Raise if data is not 3D or 4D
    planet_check_dimension(data)

    # If data is 4D batch of images then ignore the first dimension (assumed to be batch)
    if data.ndim == 4:
        shape = [0] + [x + 1 for x in shape]

    # Reshape
    if isinstance(data, torch.Tensor):
        data = data.permute(shape)
    else:
        data = data.transpose(shape)

    return data


# For plotting functions
def planet_to_tensor(data: PlanetDataType, data_type: str) -> torch.Tensor:
    """Convert Planet data to torch.Tensor

    Args:
        data: Planet data (np.ndarray, torch.Tensor, or list of either)
        data_type: Planet data type (RGB / TOAR / SR / ATM)

    Returns:
        torch.Tensor uint8 or float32
    """

    # If data is already a tensor, ensure it is contiguous
    if isinstance(data, torch.Tensor):
        return data.contiguous()

    # If data is np.ndarray, coerce to contiguous tensor
    if isinstance(data, np.ndarray):
        # Cast to appropriate dtype
        match data_type:
            case "RGB":
                pass
            case "TOAR":
                data = data.astype(np.float32)  # use float32 for mps compatibility
            case "SR":
                data = data.astype(np.float32)  # use float32 for mps compatibility
            case "ATM":
                data = data.astype(np.float32)  # use float32 for mps compatibility
            case _:
                raise ValueError(f"Unrecognized `data_type`: {data_type}")
        return torch.from_numpy(data).contiguous()

    # If data is a list of 3D images, stack it into a mini-batch tensor
    if isinstance(data, list):
        if data[0].ndim != 3:
            raise TypeError(f"List `data` elements must be 3D; got {data[0].ndim}D")
        try:
            data = torch.stack(data)
        except TypeError:
            data = np.stack(data)
        return planet_to_tensor(data, data_type=data_type)

    raise TypeError(f"`data` must be np.ndarray, torch.Tensor, or list; got {type(data)}")


def _compute_nrow_ncol(
    n_images: int, nrow: Optional[int] = None, ncol: Optional[int] = None
) -> tuple[int, int]:
    """Return the number of rows and columns needed to grid n_images images"""

    if ncol is None:
        if nrow is not None:
            ncol = int(np.ceil(n_images / nrow))
        else:
            ncol = int(np.ceil(n_images**0.5))
    if nrow is None:
        nrow = int(np.ceil(n_images / ncol))

    # Warn if too many images
    if n_images > nrow * ncol:
        warn(
            f"Too many images ({n_images}) for {nrow}x{ncol} grid; showing first"
            f" {nrow*ncol}"
        )

    return nrow, ncol


def planet_make_grid(
    images: PlanetDataType | list[PlanetDataType],
    data_type: str,
    *,
    channels: PlanetChannelsType,
    nrow: Optional[int],
    ncol: Optional[int],
    size: Optional[int],
    normalize: Optional[bool] = None,
    value_range: Optional[tuple[int, int]] = None,
    scale_each: bool = False,
    padding: int = 2,
    pad_value: float = 0.0,
) -> torch.Tensor:
    """Mosaic multiple Planet images into a grid"""

    if normalize is None:
        normalize = data_type != "RGB"

    # Coerce images to torch.Tensor
    # Must do this before getting channels in case images is list of images
    images = planet_to_tensor(images, data_type=data_type)

    # Coerce to float if data is int and normalize is True
    if normalize and not torch.is_floating_point(images):
        images = images.float()

    # Get channels
    images = planet_get_channels(images, channels=channels, data_type=data_type)

    # Possibly resize the images
    if size is not None:
        images = resize(images, size=size, antialias=True)

    # Calculate nrow and ncol if missing
    nrow, ncol = _compute_nrow_ncol(images.size(0), nrow, ncol)

    # Make the grid
    images = make_grid(
        images,
        nrow=ncol,  # for make_grid nrow is the number of images per row
        padding=padding,
        normalize=normalize,
        value_range=value_range,
        scale_each=scale_each,
        pad_value=pad_value,
    )

    return images


def planet_show(
    images: list[PlanetDataType] | PlanetDataType,
    data_type: str,
    *,
    channels: PlanetChannelsType = "RGB",
    nrow: Optional[int] = None,
    ncol: Optional[int] = None,
    size: Optional[int] = None,
    normalize: bool = True,
    value_range: Optional[tuple[int, int]] = None,
    scale_each: bool = False,
    padding: int = 2,
    pad_value: float = 0.0,
) -> PIL.Image.Image:
    """Display and returns a PIL image showing a grid of Planet images

    Args:
        images: Planet images as np.ndarray, torch.Tensor, or list of either
        data_type: The type of data [RGB / TOAR / ATM]
        channels: The channels to display (default: "RGB")
        nrow, ncol: Output rows and columns (default: square layout)
        size: Output size of each image in pixels (default: original size)
        normalize: Whether to shift image to range (0, 1) (default: True)
        value_range: (min, max) to clamp to before shifting (default: compute from data)
        scale_each: Whether to normalize each image separately (default: False)
        padding: Pixels of padding around images (default: 2)
        pad_value: Value for the padded pixels (default: 0)
    """

    # Combine the images in a grid
    images = planet_make_grid(
        images=images,
        nrow=nrow,
        ncol=ncol,
        size=size,
        channels=channels,
        data_type=data_type,
        normalize=normalize,
        value_range=value_range,
        scale_each=scale_each,
        pad_value=pad_value,
        padding=padding,
    )

    # Display and return the PIL image
    images = to_pil_image(images)
    images.show()
    return images


def planet_plot(
    images: list[PlanetDataType] | PlanetDataType,
    data_type: str,
    *,
    channels: PlanetChannelsType = "RGB",
    labels: Optional[list[str] | str] = None,
    nrow: Optional[int] = None,
    ncol: Optional[int] = None,
    size: int = 3,
    normalize: bool = True,
    value_range: Optional[tuple[int, int]] = None,
    label_kw: Optional[dict[str, Any]] = None,
) -> matplotlib.figure.Figure:
    """Returns a plot showing a grid of (possibly labeled) Planet images

    Args:
        images: Planet images as np.ndarray, torch.Tensor, or list of either
        data_type: The type of data [RGB / TOAR / ATM]
        channels: The channels to display (default: "RGB")
        labels: Optional labels to draw over the images
        nrow, ncol: Output rows and columns (default: square layout)
        size: Output size of each image in inches (default: 3 inches)
        normalize: Whether to shift image to range (0, 1) (default: True)
        value_range: (min, max) to clamp to before shifting (default: compute from data)
        label_kw: Optional dict of arguments for drawing labels; passed to
            matplotlib.axes.Axes.text
    """

    # Coerce images to torch.Tensor
    # Must do this before getting channels in case images is list of images
    images = planet_to_tensor(images, data_type=data_type)

    # Get channels
    images = planet_get_channels(images, channels=channels, data_type=data_type)

    # Ensure tensor shape is Images x Channels x Height x Width
    if images.dim() == 3:
        images = images.unsqueeze(0)

    # Possibly normalize the images
    if normalize:
        images = normalize_min_max(images, value_range=value_range)

    # Check labels
    if labels is None:
        labels = [None] * images.shape[0]
    if isinstance(labels, str):
        labels = [labels]
    if len(labels) != images.shape[0]:
        raise ValueError(
            f"Number of `labels` ({len(labels)}) != number of `images` ({images.shape[0]})"
        )

    # Calculate nrow and ncol if missing
    nrow, ncol = _compute_nrow_ncol(images.size(0), nrow, ncol)

    # Define how to draw the labels
    label_args = dict(
        x=0.05,
        y=0.95,
        bbox=dict(alpha=0.7, facecolor="white", linewidth=0),
        fontsize=12,
        va="top",
        wrap=True,
    )
    if label_kw is not None:
        label_args |= label_kw

    # Plot
    fig, axs = matplotlib.pyplot.subplots(
        nrow, ncol, squeeze=False, figsize=(size * ncol, size * nrow)
    )
    for ax, img, label in zip(axs.flat, images, labels):
        ax.imshow(planet_reshape(img, shape="HWC"))  # imshow requires shape HWC
        ax.text(s=label, transform=ax.transAxes, **label_args)
    for ax in axs.flat:
        ax.axis("off")  # turn off axes for all subplots (including those with no image)
    matplotlib.pyplot.subplots_adjust(
        bottom=0.01, left=0.01, top=0.99, right=0.99, hspace=0.01, wspace=0.01
    )

    return fig
