"""Utility functions for meteorological data"""

import numpy as np


def wind_components(ws: float, wd: float) -> tuple[float, float]:
    """Convert wind speed and meteorological direction to u and v components

    Args:
        ws: Wind speed (scalar)
        wd: Meteorological wind direction (degrees) = direction of wind origin
            e.g. 0 = wind from north to south

    Returns:
        Wind u and v components
    """

    wd = (270 - wd) % 360  # mathematical direction (degrees)
    wd = wd * np.pi / 180  # mathematical direction (radians)
    u = ws * np.cos(wd)  # zonal (eastwards) component
    v = ws * np.sin(wd)  # meridional (northwards) component
    return u, v


def wind_direction(u: float, v: float) -> float:
    """Convert u and v to meteorological wind direction (degrees)

    Args:
        u: Wind u component (zonal = eastwards)
        v: Wind v component (meridional = northwards)

    Returns:
        Meteorological wind direction (degrees) = direction of wind origin
        *   0 = north wind (from north to south)
        *  90 = west wind (from west to east)
        * 180 = south wind (from south to north)
        * 270 = east wind (from east to west)
    """

    wd = np.arctan2(v, u)  # mathematical direction (radians)
    wd = wd * 180 / np.pi  # mathematical direction (degrees)
    wd = (270 - wd) % 360  # meteorological direction (degrees)
    return wd


def wind_speed(u: float, v: float) -> float:
    """Convert u and v to wind speed

    Args:
        u: Wind u component (zonal = eastwards)
        v: Wind v component (meridional = northwards)

    Returns:
        Wind speed (scalar)
    """

    return np.sqrt(u**2 + v**2)
