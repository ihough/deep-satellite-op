"""Assign data samples to train, validate, and test sets

This function allows all combinations of:
    * Specifying the fraction of samples to use for testing
    * Specifying groups of samples that must be kept together
    * Specifying whether or not to shuffle the data before splitting
      - If False, test samples are taken from the end of the dataset

None of the tools in sklearn provide this combination of functionality:
    * train_test_split does not handle grouped data
    * GroupShuffleSplit always shuffles the data
    * GroupKFold does not take test samples from the end of the dataset and it
      is tricky to get a specific fraction of the dataset

This function does *not* allow stratification i.e. ensuring train and test sets
have a similar distribution of classes. This is quite difficult to achieve with
grouped data; see sklearn's StratifiedGroupKFold for a possible alternative.
"""

from typing import Optional
from pandas import DataFrame, Series

from utils import split_train_test


def split_train_val_test(
    data: DataFrame | Series,
    *,
    test_frac: float,
    val_frac: float,
    groups: Optional[str | Series] = None,
    shuffle: bool = False,
    rand_seed: Optional[int] = None,
) -> tuple[list[int], list[int], list[int]]:
    """Assign data samples to train, validate, and test sets

    Arguments:
        data: A pandas DataFrame or Series to be split
        test_frac: The fraction of samples to assign to the test set
        val_frac: The fraction of samples to assign to the validation set
        groups: Optional, the name of a column or a Series identifying groups.
            If specified all samples in each group are assigned to the same set.
        shuffle: Whether to shuffle samples (or groups) before splitting
        rand_seed: A random seed for shuffling if shuffle = True

    Returns:
        Three lists identifying position (iloc) of train, val, and test samples

    Raises:
        ValueError if data is not DataFrame or Series
        ValueError unless 0 <= test_frac + val_frac < 1
        KeyError if groups is a string that is not the name of a column
        ValueError if no data assigned to train set

    Warns:
        If test set is substantially (> 5% of n_samples) larger than requested
    """

    # Sanity check val_frac; other arguments are sanity-checked in split_train_test
    if val_frac < 0 or val_frac >= 1:
        raise ValueError(f"`val_frac` must be in range [0, 1); got {val_frac}")
    if test_frac + val_frac >= 1:
        raise ValueError(
            f"`test_frac` + `val_frac` must be < 1; got {test_frac + val_frac}"
        )

    # Assign test set; remaining data will be split between train and val
    train_val, test_idx = split_train_test(
        data=data,
        test_frac=test_frac,
        groups=groups,
        shuffle=shuffle,
        rand_seed=rand_seed,
    )

    # Make groups relative to train_val
    groups = groups.iloc[train_val] if isinstance(groups, Series) else groups

    # Assign remaining data to train / val
    train, val = split_train_test(
        data=data.iloc[train_val],
        test_frac=val_frac / (1 - test_frac),  # val_frac as fraction of train + val
        groups=groups,
        shuffle=shuffle,  # reshuffle b/c train_val is not shuffled if test_frac was 0
        rand_seed=rand_seed,
    )

    # Make train + val indexes relative to the original data
    train_idx = [train_val[i] for i in train]
    val_idx = [train_val[i] for i in val]

    return train_idx, val_idx, test_idx
